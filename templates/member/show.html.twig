{% extends 'base.html.twig' %}

{% block page_title %}
{{ member.name ? member.name : 'member.level.disabled'|trans|capitalize }}
{% endblock %}

{% block body %}
<h1 class="mb-3 text-center">
    {{ member.name ? member.name : 'member.level.disabled'|trans|capitalize }}
</h1>
{% if is_granted('ROLE_ALLOWED_TO_SWITCH') %}
    <p class="text-center">
        <a
            class="btn btn-warning"
            href="{{ path('member_me', { _switch_user: member.email }) }}"
        >{{ 'member.action.impersonate'|trans }}</a>
    </p>
{% endif %}
<div class="row">
    <div class="col-md-4 offset-{{ member.name ? 2 : 4 }}">
        {% if is_granted('ROLE_ADMIN') %}
        <h2>{{ 'member.show.info'|trans }}</h2>
        <p>
            {{ 'member.field.email'|trans }} :
            <a href="mailto:{{ member.email }}">{{ member.email }}</a>
        </p>
        {% endif %}

        {% if member.name %}
        {% if member.isCooperator %}
        <hr>
        <p>
            <strong>{{ 'member.show.cooperator.yes'|trans }}</strong>
            {{ 'member.show.period'|trans({
                '%start%': member.passStart|date('d/m/Y'),
                '%end%': member.passEnd|date('d/m/Y')
            }) }}
        </p>
        {% if is_granted('ROLE_ADMIN') %}
        {% if member.passEnd|date('U') < '+30 days'|date('U') %}
        <p class="alert alert-warning">
            {{ 'cooperator_pass.flash.warning.end_admin'|trans }}
        </p>
        {% if member.passIsWaiting %}
        <p class="alert alert-info">
            {{ 'cooperator_pass.flash.info.renewal'|trans }}
        </p>
        {% include 'cooperator_pass/_renew_form.html.twig' with {
            'pass': {'id': member.passId}
        } %}
        {% else %}
        <p class="alert alert-info">
            {{ 'cooperator_pass.flash.info.no_renewal'|trans }}
        </p>
        {% endif %}
        {% endif %}
        <p>
            {% include 'cooperator_pass/_stop_form.html.twig' %}
        </p>
        {% endif %}
        {% else %}
        <p>{{ 'member.show.cooperator.no'|trans }}</p>
        {% if member.passIsWaiting and is_granted('ROLE_ADMIN') %}
        <p class="alert alert-info">
            {{ 'cooperator_pass.flash.info.activation'|trans }}
        </p>
        {% include 'cooperator_pass/_activate_form.html.twig' with {
            'pass': {'id': member.passId}
        } %}
        {% endif %}
        {% endif %}
        
        <hr>
        <p>
            {% if member.isNeighbor %}
            {{ 'member.show.neighbor.yes'|trans }}
            {% else %}
            {{ 'member.show.neighbor.no'|trans }}
            {% endif %}
        </p>
        {% endif %}
        <hr>
        <a href="{{ path('booking_index_applicant', {id: member.id}) }}" class="btn btn-primary">
            {{ 'booking.action.see_bookings'|trans }}
        </a>
    </div>
    {% if is_granted('ROLE_ADMIN') and member.name %}
    <div class="col-md-4">
        <h2>{{ 'member.edit.operations'|trans }}</h2>
        {{ include('member/_update_password_form.html.twig', {
            'label': 'member.action.update_password_unverified'|trans
        }) }}
        <br />
        {{ include('member/_disable_form.html.twig', {
            'label': 'member.action.disable'|trans
        }) }}
    </div>
    {% endif %}
</div>
{% endblock %}
