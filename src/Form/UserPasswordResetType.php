<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPasswordResetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('password', PasswordType::class, [
                'label' => 'user.field.password',
                'help' => 'user.edit.new_password_help',
                'constraints' => [
                    new NotBlank(['message' => 'user.password.blank']),
                    new Length(['min' => 8, 'minMessage' => 'user.password.length']),
                ],
            ])
            ->add('password_confirm', PasswordType::class, [
                'label' => 'user.edit.new_password_confirm',
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
