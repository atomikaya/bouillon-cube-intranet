<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('current_password', PasswordType::class, [
                'label' => 'user.edit.current_password',
                'attr' => ['autocomplete' => 'off'],
                'constraints' => [
                    new UserPassword(['message' => 'user.password.current']),
                ],
            ])
            ->add('new_password', PasswordType::class, [
                'label' => 'user.edit.new_password',
                'help' => 'user.edit.new_password_help',
                'constraints' => [
                    new NotBlank(['message' => 'user.password.blank']),
                    new Length(['min' => 8, 'minMessage' => 'user.password.length']),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
