<?php

namespace App\Form;

use App\Entity\Member;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MemberReactivationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', null, [
                'label' => 'member.field.email',
                'disabled' => true,
            ])
            ->add('name', null, ['label' => 'member.field.name'])
            ->add('address', null, ['label' => 'member.field.address'])
            ->add('isNeighbor', null, ['label' => 'member.field.isNeighbor'])
            ->add('isCooperator', CheckboxType::class, [
                'label' => 'member.field.isCooperator',
                'required' => false,
                'mapped' => false,
            ])
            ->add('passStart', DateType::class, [
                'widget' => 'single_text',
                'input' => 'datetime_immutable',
                'label' => 'cooperator_pass.field.start',
                'required' => false,
                'mapped' => false,
            ])
            ->add('passEnd', DateType::class, [
                'widget' => 'single_text',
                'input' => 'datetime_immutable',
                'label' => 'cooperator_pass.field.end',
                'required' => false,
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Member::class,
        ]);
    }
}
