<?php

namespace App\Form;

use App\Entity\Booking;
use App\Entity\Room;
use App\Repository\RoomRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalendarBookingType extends AbstractType
{
    private RoomRepository $repository;

    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('start', null, ['widget' => 'single_text'])
            ->add('end', null, ['widget' => 'single_text'])
            ->add('place', EntityType::class, [
                'class' => Room::class,
                'choice_label' => 'name',
                'label' => 'booking.field.place',
                'choices' => $this->repository->findAllForAccessLevel(
                    $options['access_level']
                ),
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'booking.field.comment',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'access_level' => 'outsider',
        ]);
    }
}
