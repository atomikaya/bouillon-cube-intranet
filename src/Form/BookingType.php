<?php

namespace App\Form;

use App\Entity\Booking;
use App\Entity\Room;
use App\Repository\RoomRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingType extends AbstractType
{
    private RoomRepository $repository;

    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('place', EntityType::class, [
                'class' => Room::class,
                'choice_label' => 'name',
                'label' => 'booking.field.place',
                'choices' => $this->repository->findAllForAccessLevel(
                    $options['access_level']
                ),
                'choice_attr' => ChoiceList::attr($this, function(?Room $room) use ($options) {
                    $rate = $room->getRateForAccessLevel($options['access_level']);
                    $attr = [ 'data-slug' => $room->getSlug() ];

                    if ($rate->getHourlyRate()) {
                        $attr['data-hourly-rate'] = $rate->getHourlyRate() / 100;
                    } elseif (
                        $rate->hasHourlyRate() || $rate->getMaxHours() > 0
                    ) {
                        $attr['data-hourly-rate'] = 0;
                    }

                    if ($rate->getDailyRate()) {
                        $attr['data-daily-rate'] = $rate->getDailyRate() / 100;
                    } elseif (
                        $rate->hasDailyRate() || $rate->getMaxDays() > 0
                    ) {
                        $attr['data-daily-rate'] = 0;
                    }
                    
                    if ($rate->getMonthlyRate()) {
                        $attr['data-monthly-rate'] = $rate->getMonthlyRate() / 100;
                    }

                    if ($rate->getOvertimeRate()) {
                        $attr['data-overtime-rate'] = $rate->getOvertimeRate() / 100;
                    }

                    if ($rate->hasMaxHours()) {
                        $attr['data-max-hours'] = $rate->getMaxHours();
                    }

                    if ($rate->hasMaxDays()) {
                        $attr['data-max-days'] = $rate->getMaxDays();
                    }

                    $attr['data-hour-seeking'] = $rate->getHourSeeking();

                    return $attr;
                }),
            ])
            ->add('unit', ChoiceType::class, [
                'label' => 'booking.field.unit.label',
                'choices' => [
                    'booking.field.unit.hour' => Booking::UNIT_HOUR,
                    'booking.field.unit.day' => Booking::UNIT_DAY,
                    'booking.field.unit.month' => Booking::UNIT_MONTH,
                ],
                'choice_attr' => function () {
                    return ['autocomplete' => 'off'];
                },
                'expanded' => true,
            ])
            ->add('start', null, [
                'label' => 'booking.field.start',
                'date_widget' => 'single_text',
                'minutes' => [0, 30],
            ])
            ->add('end', null, [
                'label' => 'booking.field.end',
                'date_widget' => 'single_text',
                'minutes' => [0, 30],
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'booking.field.comment',
                'required' => false,
            ])
            ->add('repeat_weekly', CheckboxType::class, [
                'label' => 'booking.field.repeat_weekly',
                'required' => false,
                'mapped' => false,
            ])
            ->add('repeat_weekly_until', DateType::class, [
                'label' => 'booking.field.repeat_weekly_until',
                'widget' => 'single_text',
                'required' => false,
                'mapped' => false,
            ])
            ->add('repeat', HiddenType::class, [
                'data' => false,
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
            'access_level' => 'outsider',
        ]);
    }
}
