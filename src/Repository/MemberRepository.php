<?php

namespace App\Repository;

use App\Entity\CooperatorPass;
use App\Entity\Member;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Member|null find($id, $lockMode = null, $lockVersion = null)
 * @method Member|null findOneBy(array $criteria, array $orderBy = null)
 * @method Member[]    findAll()
 * @method Member[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberRepository extends ServiceEntityRepository
{
    const PAGE_SIZE = 10;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Member::class);
    }

    /** @return Member[] */
    public function findAllRegistrations(): array
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.isActive = false')
            ->andWhere('m.isWaiting = true')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOneWithPass(Member $member): ?array
    {
        return $this->createWithPassBuilder()
            ->andWhere('m.id = :id')
            ->setParameter('id', $member->getId())
            ->select([
                'm.id',
                'm.email',
                'm.name',
                'm.isNeighbor',
                'c.isActive as isCooperator',
                'c.id as passId',
                'c.start as passStart',
                'c.end as passEnd',
                'c.isActive as passIsActive',
                'c.isWaiting as passIsWaiting',
            ])
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findOneWithFuturePass(Member $member): ?array
    {
        return $this->createWithFuturePassBuilder()
            ->andWhere('m.id = :id')
            ->setParameter('id', $member->getId())
            ->select([
                'm.id',
                'm.email',
                'm.name',
                'm.isNeighbor',
                'c.isActive as isCooperator',
                'c.id as passId',
                'c.start as passStart',
                'c.end as passEnd',
                'c.isActive as passIsActive',
                'c.isWaiting as passIsWaiting',
            ])
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findAllPagedWithPass(int $page): array
    {
        return $this->createWithPassBuilder()
            ->leftJoin(User::class, 'u', Join::WITH, 'm = u.owner')
            ->andWhere('m.isActive = true')
            ->select([
                'm.id',
                'm.email',
                'm.name',
                'm.isNeighbor',
                'c.isActive as isCooperator',
                'c.start as passStart',
                'c.end as passEnd',
            ])
            ->orderBy('m.name', 'ASC')
            ->setMaxResults(self::PAGE_SIZE)
            ->setFirstResult(($page - 1) * self::PAGE_SIZE)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAllPagedWithFuturePass(int $page): array
    {
        return $this->createWithFuturePassBuilder()
            ->leftJoin(User::class, 'u', Join::WITH, 'm = u.owner')
            ->andWhere('m.isActive = true')
            ->select([
                'm.id',
                'm.email',
                'm.name',
                'm.isNeighbor',
                'c.isActive as isCooperator',
                'c.start as passStart',
                'c.end as passEnd',
            ])
            ->orderBy('m.name', 'ASC')
            ->setMaxResults(self::PAGE_SIZE)
            ->setFirstResult(($page - 1) * self::PAGE_SIZE)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findDisabledPaged(int $page): array
    {
        return $this->createWithPassBuilder()
            ->leftJoin(User::class, 'u', Join::WITH, 'm = u.owner')
            ->andWhere('m.isActive = false')
            ->andWhere('m.isWaiting = false')
            ->andWhere('u is null')
            ->select([
                'm.id',
                'm.email',
            ])
            ->orderBy('m.email', 'ASC')
            ->setMaxResults(self::PAGE_SIZE)
            ->setFirstResult(($page - 1) * self::PAGE_SIZE)
            ->getQuery()
            ->getResult()
        ;
    }

    private function createWithPassBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('m')
            ->leftJoin(
                CooperatorPass::class,
                'c',
                Join::WITH,
                'm = c.owner AND'
                    . ' (c.isActive = true OR c.isWaiting = true) AND'
                    . ' c.start <= :today AND c.end >= :today'
            )
            ->setParameter('today', new \DateTime())
        ;
    }

    private function createWithFuturePassBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('m')
            ->leftJoin(
                CooperatorPass::class,
                'c',
                Join::WITH,
                'm = c.owner AND'
                    . ' (c.isActive = true OR c.isWaiting = true) AND'
                    . ' c.end >= :today'
            )
            ->setParameter('today', new \DateTime())
        ;
    }
}
