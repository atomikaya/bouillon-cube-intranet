<?php

namespace App\Repository;

use App\Entity\BookingBill;
use App\Entity\Member;
use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BookingBill|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingBill|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingBill[]    findAll()
 * @method BookingBill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingBillRepository extends ServiceEntityRepository
{
    const PAGE_SIZE = 10;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BookingBill::class);
    }

    public function findMonthsDue(): array
    {
        return $this->createQueryBuilder('bb')
            ->join('bb.receiver', 'r')
            ->andWhere('bb.due <= :now')
            ->andWhere('bb.isSettled = false')
            ->andWhere('bb.price > 0')
            ->setParameter('now', new \DateTimeImmutable())
            ->select(
                'CONCAT(YEAR(bb.due), \'-\', MONTH(bb.due)) as uniqueMonth',
                'COUNT(DISTINCT r.id) as count',
            )
            ->groupBy('uniqueMonth')
            ->orderBy('uniqueMonth', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findMonthlyGroupedByReceiver(string $uniqueMonth): array
    {
        $date = \DateTimeImmutable::createFromFormat('Y-n-d', "$uniqueMonth-01");

        return $this->createQueryBuilder('bb')
            ->join('bb.receiver', 'r')
            ->andWhere('bb.due >= :current AND bb.due < :next')
            ->andWhere('bb.isSettled = false')
            // ->andWhere('bb.price > 0')
            ->setParameter('current', $date->modify('first day of this month midnight'))
            ->setParameter('next', $date->modify('first day of next month midnight'))
            ->select([
                'GROUP_CONCAT(bb.id) as ids',
                'bb.due',
                'r.id as receiverId',
                'r.name as receiverName',
                'COUNT(DISTINCT bb.source) as count',
                'COUNT(DISTINCT bb.id) - COUNT(DISTINCT bb.source) as pass',
                'SUM(bb.price) as price',
            ])
            ->orderBy('r.name', 'ASC')
            ->groupBy('bb.due')
            ->addGroupBy('r.id')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findPagedDue(int $page): array
    {
        return $this->createQueryBuilder('bb')
            ->join('bb.receiver', 'r')
            ->andWhere('bb.due <= :now')
            ->andWhere('bb.isSettled = false')
            ->andWhere('bb.price > 0')
            ->setParameter('now', new \DateTimeImmutable())
            ->select([
                'GROUP_CONCAT(bb.id) as ids',
                'bb.due',
                'r.id as receiverId',
                'r.name as receiverName',
                'COUNT(bb.source) as count',
                'COUNT(bb.id) - COUNT(bb.source) as pass',
                'SUM(bb.price) as price',
            ])
            ->orderBy('bb.due', 'DESC')
            ->groupBy('bb.due')
            ->addGroupBy('r.id')
            ->setMaxResults(self::PAGE_SIZE)
            ->setFirstResult(($page - 1) * self::PAGE_SIZE)
            ->getQuery()
            ->getResult()
        ;
    }

    // due : past && not settled
    public function countDue(): int
    {
        $result = $this->createQueryBuilder('bb')
            ->andWhere('bb.due <= :now')
            ->andWhere('bb.isSettled = false')
            ->andWhere('bb.price > 0')
            ->setParameter('now', new \DateTimeImmutable())
            ->select('COUNT(distinct bb.receiver) as count')
            ->groupBy('bb.due')
            ->getQuery()
            ->getResult()
        ;
        $total = 0;

        foreach ($result as ['count' => $count]) {
            $total += $count;
        }

        return $total;
    }

    public function findFutureForPassByReceiver(Member $receiver): array
    {
        return $this->createQueryBuilder('bb')
            ->andWhere('bb.due > :now')
            ->andWhere('bb.receiver = :receiver')
            ->andWhere('bb.source IS NULL')
            ->setParameter('now', new \DateTimeImmutable())
            ->setParameter('receiver', $receiver)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findMonthlyDue(\DateTimeImmutable $month): array
    {
        return $this->createQueryBuilder('bb')
            ->join('bb.receiver', 'r')
            ->andWhere('bb.due >= :current AND bb.due < :next')
            ->andWhere('bb.isSettled = false')
            ->setParameter('current', $month->modify('first day of this month midnight'))
            ->setParameter('next', $month->modify('first day of next month midnight'))
            ->getQuery()
            ->getResult()
        ;
    }
    
    public function findFutureByPlaceAndApplicant(
        \DateTimeImmutable $start,
        Room $place,
        Member $applicant
    ): array {
        return $this->createQueryBuilder('bb')
            ->join('bb.source', 'b')
            ->andWhere('b.start > :start')
            ->andWhere('b.place = :place')
            ->andWhere('b.applicant = :applicant')
            ->setParameter('place', $place->getId())
            ->setParameter('applicant', $applicant->getId())
            ->setParameter('start', $start)
            ->orderBy('b.start', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
