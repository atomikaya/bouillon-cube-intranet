<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\BookingBill;
use App\Entity\Member;
use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    const PAGE_SIZE = 10;

    const WHERE_DATES_OVERLAP = 'b.start < :end AND b.end > :start';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    public function findAllPrefetched(): array
    {
        return $this->createQueryBuilder('b')
            ->join('b.applicant', 'a')
            ->join('b.place', 'p')
            ->join(BookingBill::class, 'bb', Join::WITH, 'b = bb.source')
            ->select([
                'b.id',
                'b.start',
                'b.end',
                'b.isApproved',
                'b.unit',
                'SUM(bb.price) as price',
                'a.id as applicantId',
                'a.name as applicantName',
                'p.name as placeName',
            ])
            ->orderBy('b.start', 'ASC')
            ->groupBy('b.id')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findPagedByApplicant(Member $member, int $page): array
    {
        return $this->createQueryBuilder('b')
            ->join('b.applicant', 'a')
            ->join('b.place', 'p')
            ->join(BookingBill::class, 'bb', Join::WITH, 'b = bb.source')
            ->select([
                'b.id',
                'b.start',
                'b.end',
                'b.isApproved',
                'b.unit',
                'SUM(bb.price) as price',
                'a.id as applicantId',
                'a.name as applicantName',
                'p.name as placeName',
            ])
            ->andWhere('b.applicant = :id')
            ->setParameter('id', $member->getId())
            ->orderBy('b.start', 'DESC')
            ->groupBy('b.id')
            ->setMaxResults(self::PAGE_SIZE)
            ->setFirstResult(($page - 1) * self::PAGE_SIZE)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findPagedWaiting(int $page): array
    {
        return $this->createQueryBuilder('b')
            ->join('b.applicant', 'a')
            ->join('b.place', 'p')
            ->join(BookingBill::class, 'bb', Join::WITH, 'b = bb.source')
            ->select([
                'b.id',
                'b.start',
                'b.end',
                'b.unit',
                'SUM(bb.price) as price',
                'a.id as applicantId',
                'a.name as applicantName',
                'p.name as placeName',
            ])
            ->andWhere('b.isApproved = false')
            ->orderBy('b.start', 'ASC')
            ->groupBy('b.id')
            ->setMaxResults(self::PAGE_SIZE)
            ->setFirstResult(($page - 1) * self::PAGE_SIZE)
            ->getQuery()
            ->getResult()
        ;
    }

    public function countWaitingByApplicant(Member $applicant): ?array
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.applicant = :applicant')
            ->andWhere('b.isApproved = false')
            ->setParameter('applicant', $applicant->getId())
            ->select(['COUNT(b.id) as count'])
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findConcurrents(
        Room $room,
        \DateTime $start,
        \DateTime $end
    ): array {
        $qb = $this->createQueryBuilder('b');
        $eb = $qb->expr();
        
        return $qb
            ->join('b.applicant', 'a')
            ->andWhere('b.place = :room')
            // inbounds calculation
            ->andWhere(self::WHERE_DATES_OVERLAP)
            ->setParameter('room', $room->getId())
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->select([
                'b.start',
                'b.end',
                'b.isApproved',
                'a.id as applicantId',
                'a.name as applicantName',
            ])
            ->orderBy('b.start', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findYearlyByPlaceAndApplicant(
        \DateTimeImmutable $start,
        \DateTimeImmutable $end,
        Room $place,
        Member $applicant
    ): array {
        $qb = $this->createQueryBuilder('b');
        $eb = $qb->expr();

        return $qb
            ->andWhere('b.applicant = :applicant')
            ->andWhere('b.place = :place')
            ->andWhere(self::WHERE_DATES_OVERLAP)
            // ->andWhere('b.isApproved = true')
            ->setParameter('applicant', $applicant->getId())
            ->setParameter('place', $place->getId())
            ->setParameter('start', $start->modify('1st january'))
            ->setParameter('end', $end->modify('1st january next year'))
            ->select(['b.start', 'b.end'])
            ->getQuery()
            ->getResult()
        ;
    }

    public function findFutureByApplicant(
        Member $applicant,
        \DateTimeImmutable $start
    ): array {
        return $this->createQueryBuilder('b')
            ->andWhere('b.start >= :now')
            ->andWhere('b.applicant = :applicant')
            ->setParameter('applicant', $applicant->getId())
            ->setParameter('now', $start)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findMonthsByApplicant(Member $applicant): array
    {
        $bookings = $this->createQueryBuilder('b')
            ->andWhere('b.applicant = :applicant')
            ->setParameter('applicant', $applicant->getId())
            ->select(
                'b.start',
                'b.end',
            )
            ->getQuery()
            ->getResult()
        ;
        $min = array_reduce($bookings, function($acc, $b) {
            return $b['start'] < $acc ? $b['start'] : $acc;
        }, new \DateTimeImmutable());
        $max = array_reduce($bookings, function($acc, $b) {
            return $b['end'] > $acc ? $b['end'] : $acc;
        }, new \DateTimeImmutable('2020-01-01'));
        $result = [];

        for (
            $now = $min;
            $now < $max;
            $now = $now->modify('first day of next month midnight')
        ) {
            $next = $now->modify('first day of next month midnight');
            $uniqueMonth = $now->format('Y-n');

            foreach ($bookings as $booking) {
                if ($booking['start'] < $next && $booking['end'] > $now) {
                    if (!isset($result[$uniqueMonth])) {
                        $result[$uniqueMonth] = [
                            'uniqueMonth' => $uniqueMonth,
                            'count' => 0,
                        ];
                    }

                    $result[$uniqueMonth]['count']++;
                }
            }
        }

        krsort($result);

        return array_values($result);
    }

    public function findMonthlyByApplicant(
        Member $member,
        string $uniqueMonth
    ): array {
        $date = \DateTimeImmutable::createFromFormat('Y-n-d', "$uniqueMonth-01");

        return $this->createQueryBuilder('b')
            ->join('b.applicant', 'a')
            ->join('b.place', 'p')
            ->join(BookingBill::class, 'bb', Join::WITH, 'b = bb.source')
            ->select([
                'b.id',
                'b.start',
                'b.end',
                'b.isApproved',
                'b.unit',
                'SUM(bb.price) as price',
                'a.id as applicantId',
                'a.name as applicantName',
                'p.name as placeName',
                'MONTH(b.start) as startMonth'
            ])
            ->andWhere('b.applicant = :id')
            ->andWhere(self::WHERE_DATES_OVERLAP)
            ->andWhere('bb.due = :end')
            ->setParameter('id', $member->getId())
            ->setParameter('start', $date->modify('first day of this month midnight'))
            ->setParameter('end', $date->modify('first day of next month midnight'))
            ->orderBy('b.start', 'ASC')
            ->groupBy('b.id')
            ->getQuery()
            ->getResult()
        ;
    }
    
    public function findFutureByPlaceAndApplicant(
        \DateTimeImmutable $start,
        Room $place,
        Member $applicant
    ): array {
        return $this->createQueryBuilder('b')
            ->andWhere('b.start > :start')
            ->andWhere('b.place = :place')
            ->andWhere('b.applicant = :applicant')
            ->setParameter('place', $place->getId())
            ->setParameter('applicant', $applicant->getId())
            ->setParameter('start', $start)
            ->orderBy('b.start', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findForCalendar(
        \DateTimeInterface $start,
        \DateTimeInterface $end,
        ?int $roomId
    ): array {
        $qb = $this->createQueryBuilder('b')
            ->where(self::WHERE_DATES_OVERLAP)
            ->setParameter('start', $start->format('Y-m-d H:i'))
            ->setParameter('end', $end->format('Y-m-d H:i'))
        ;

        if (!is_null($roomId)) {
            $qb->andWhere('b.place = :place')->setParameter('place', $roomId);
        }

        return $qb->getQuery()->getResult();
    }

    // same room + same member + intersecting period
    public function findDuplicates(Booking $booking): array
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.place = :place')
            ->andWhere('b.applicant = :applicant')
            ->andWhere(self::WHERE_DATES_OVERLAP)
            ->setParameter('place', $booking->getPlace())
            ->setParameter('applicant', $booking->getApplicant())
            ->setParameter('start', $booking->getStart())
            ->setParameter('end', $booking->getEnd())
            ->getQuery()
            ->getResult()
        ;
    }
}
