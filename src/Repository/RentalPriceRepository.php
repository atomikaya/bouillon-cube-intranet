<?php

namespace App\Repository;

use App\Entity\RentalPrice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RentalPrice|null find($id, $lockMode = null, $lockVersion = null)
 * @method RentalPrice|null findOneBy(array $criteria, array $orderBy = null)
 * @method RentalPrice[]    findAll()
 * @method RentalPrice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RentalPriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RentalPrice::class);
    }

    // /**
    //  * @return RentalPrice[] Returns an array of RentalPrice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RentalPrice
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
