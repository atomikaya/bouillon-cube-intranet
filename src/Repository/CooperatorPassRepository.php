<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\CooperatorPass;
use App\Entity\Member;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CooperatorPass|null find($id, $lockMode = null, $lockVersion = null)
 * @method CooperatorPass|null findOneBy(array $criteria, array $orderBy = null)
 * @method CooperatorPass[]    findAll()
 * @method CooperatorPass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CooperatorPassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CooperatorPass::class);
    }

    public function findActiveByOwner(Member $member): ?CooperatorPass
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.owner = :id')
            ->andWhere('c.isActive = true')
            ->andWhere('c.start <= :today')
            ->andWhere('c.end >= :today')
            ->setParameter('id', $member->getId())
            ->setParameter('today', new \DateTime())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findAllWaitingWithOwner(): array
    {
        return $this->createQueryBuilder('c')
            ->join('c.owner', 'u')
            ->select([
                'c.id',
                'c.end',
                'c.isActive',
                'c.isWaiting',
                'u.id as ownerId',
                'u.name as ownerName',
            ])
            ->andWhere('c.isWaiting = true')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAllExpired(): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.isActive = true')
            ->andWhere('c.end < :today')
            ->setParameter('today', new \DateTime())
            ->getQuery()
            ->getResult()
        ;
    }

    public function findExpiringInOneMonth(): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.isActive = true')
            ->andWhere('c.end = :in1Month')
            ->setParameter('in1Month', new \DateTimeImmutable('+1 month'))
            ->getQuery()
            ->getResult()
        ;
    }

    public function findExpiredWithFutureBookings(): array
    {
        $result = $this->createQueryBuilder('c')
            ->leftJoin(
                Booking::class,
                'b',
                Join::WITH,
                'c.owner = b.applicant AND b.start >= :now'
            )
            ->andWhere('c.isActive = true')
            ->andWhere('c.end < :now')
            ->setParameter('now', new \DateTime())
            ->select(['c','b'])
            ->getQuery()
            ->getResult()
        ;

        $passes = array_filter($result, function($entity) {
            return $entity instanceof CooperatorPass;
        });
        $byOwner = [];

        foreach ($passes as $pass) {
            $byOwner[] = [
                'pass' => $pass,
                'bookings' => array_filter($result, function($entity) use ($pass) {
                    return $entity instanceof Booking
                        && $entity->getApplicant() == $pass->getOwner();
                }),
            ];
        }

        return $byOwner;
    }

    /** @return CooperatorPass[] */
    public function findEndingAfter(\DateTimeImmutable $date): array
    {
        return $this
            ->createQueryBuilder('cc')
            ->where('cc.end >= :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult()
        ;
    }
}
