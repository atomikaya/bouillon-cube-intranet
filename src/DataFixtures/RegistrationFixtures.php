<?php

namespace App\DataFixtures;

use App\Entity\Member;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class RegistrationFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        $kaya = (new Member())
            ->setName('Kaya Thommy')
            ->setEmail('coucou@kayathommy.fr')
            ->setAddress("18 rue Flaugergues\n34000 Montpellier")
            ->setIsWaiting(true)
        ;

        $manager->persist($kaya);
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['test', 'prod'];
    }
}
