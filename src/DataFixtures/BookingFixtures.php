<?php

namespace App\DataFixtures;

use App\Entity\Booking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class BookingFixtures extends Fixture
implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        $zenythBooking1 = (new Booking())
            ->setApplicant($this->getReference('member_zenyth'))
            ->setPlace($this->getReference('room_office_shared_2'))
            ->setUnit('hour')
            ->setStart($this->createDate('10/11/2021 14:00'))
            ->setEnd($this->createDate('10/11/2021 20:00'))
            ->setIsApproved(true)
        ;

        $manager->persist($zenythBooking1);
        $this->setReference('booking_zenyth_1', $zenythBooking1);

        $zenythBooking2 = (new Booking())
            ->setApplicant($this->getReference('member_zenyth'))
            ->setPlace($this->getReference('room_office_shared_2'))
            ->setUnit('hour')
            ->setStart($this->createDate('12/11/2021 14:00'))
            ->setEnd($this->createDate('12/11/2021 20:00'))
            ->setIsApproved(true)
        ;

        $manager->persist($zenythBooking2);
        $this->setReference('booking_zenyth_2', $zenythBooking2);

        $zenythBooking3 = (new Booking())
            ->setApplicant($this->getReference('member_zenyth'))
            ->setPlace($this->getReference('room_office_shared_2'))
            ->setUnit('hour')
            ->setStart($this->createDate('25/11/2021 14:00'))
            ->setEnd($this->createDate('25/11/2021 20:00'))
            ->setIsApproved(true)
        ;

        $manager->persist($zenythBooking3);
        $this->setReference('booking_zenyth_3', $zenythBooking3);

        $vincaBooking1 = (new Booking())
            ->setApplicant($this->getReference('member_vinca'))
            ->setPlace($this->getReference('room_office_shared_1'))
            ->setUnit('hour')
            ->setStart($this->createDate('16/12/2021 08:00'))
            ->setEnd($this->createDate('16/12/2021 16:00'))
            ->setIsApproved(true)
        ;

        $manager->persist($vincaBooking1);
        $this->setReference('booking_vinca_1', $vincaBooking1);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            RoomFixtures::class,
            MemberFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['test'];
    }

    private function createDate(string $datetime): \DateTimeImmutable
    {
        return \DateTimeImmutable::createFromFormat('d/m/Y H:i', $datetime);
    }
}
