<?php

namespace App\DataFixtures;

use App\Entity\Room;
use App\Service\ColorBalancer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RoomFixtures extends Fixture
implements DependentFixtureInterface, FixtureGroupInterface
{
    private ColorBalancer $balancer;

    public function __construct(ColorBalancer $balancer)
    {
        $this->balancer = $balancer;
    }
    public function load(ObjectManager $manager): void
    {
        $rooms = [];

        // individual offices
        for ($i=1; $i < 4; $i++) {
            $room = (new Room())
                ->setId(180 + $i)
                ->setName("Bureau $i")
                ->setSlug('office')
                ->setCooperatorRate(
                    $this->getReference("rate_cooperator_office_$i")
                )
            ;
            $rooms[] = $room;

            $this->addReference("room_office_$i", $room);
        }

        // shared office spaces
        for ($i=1; $i < 9; $i++) {
            $room = (new Room())
                ->setId(183 + $i)
                ->setName("Bureau partagé $i" . ($i == 8 ? " (équipé)" : ""))
                ->setSlug('office_shared')
                ->setCooperatorRate(
                    $this->getReference('rate_cooperator_office_shared')
                )
                ->setNeighborRate(
                    $this->getReference('rate_neighbor_office_shared')
                )
                ->setOutsiderRate(
                    $this->getReference('rate_outsider_office_shared')
                )
            ;
            $rooms[] = $room;

            $this->addReference("room_office_shared_$i", $room);
        }

        // big teleconf -> residence with kiosk rates
        $residence = (new Room())
            ->setId(191)
            ->setName('Pièce de résidence')
            ->setSlug('residence')
            ->setCooperatorRate(
                $this->getReference('rate_cooperator_kiosk')
            )
            ->setNeighborRate(
                $this->getReference('rate_neighbor_other')
            )
            ->setOutsiderRate(
                $this->getReference('rate_outsider_other')
            )
        ;
        $rooms[] = $residence;

        $this->addReference('room_residence', $residence);

        // teleconf
        $teleconf = (new Room())
            ->setId(192)
            ->setName('Petite visio')
            ->setSlug('teleconf')
            ->setCooperatorRate(
                $this->getReference('rate_free')
            )
            ->setNeighborRate(
                $this->getReference('rate_free')
            )
            ->setOutsiderRate(
                $this->getReference('rate_free')
            )
        ;
        $rooms[] = $teleconf;

        $this->addReference('room_teleconf', $teleconf);

        // open-air theater
        $theater = (new Room())
            ->setId(193)
            ->setName('Théâtre de verdure')
            ->setSlug('theater')
            ->setCooperatorRate(
                $this->getReference('rate_cooperator_open_air_theater')
            )
            ->setNeighborRate(
                $this->getReference('rate_neighbor_other')
            )
            ->setOutsiderRate(
                $this->getReference('rate_outsider_other')
            )
        ;
        $rooms[] = $theater;

        $this->addReference('room_theater', $theater);

        // kiosk
        $kiosk = (new Room())
            ->setId(194)
            ->setName('Kiosque')
            ->setSlug('kiosk')
            ->setCooperatorRate(
                $this->getReference('rate_cooperator_kiosk')
            )
            ->setNeighborRate(
                $this->getReference('rate_neighbor_other')
            )
            ->setOutsiderRate(
                $this->getReference('rate_outsider_other')
            )
        ;
        $rooms[] = $kiosk;

        $this->addReference('room_kiosk', $kiosk);

        // kitchen
        $kitchen = (new Room())
            ->setId(195)
            ->setName('Cuisine')
            ->setSlug('kitchen')
            ->setCooperatorRate(
                $this->getReference('rate_cooperator_kitchen')
            )
            ->setNeighborRate(
                $this->getReference('rate_neighbor_other')
            )
            ->setOutsiderRate(
                $this->getReference('rate_outsider_other')
            )
        ;
        $rooms[] = $kitchen;

        $this->addReference('room_kitchen', $kitchen);

        // add colors and persist
        $wheel = $this->balancer->generateHSLWheel(count($rooms), true);

        foreach ($rooms as $i => $room) {
            $room
                ->setPosition($i)
                ->setColor($wheel[$i])
            ;
            $manager->persist($room);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [RentalPriceFixtures::class];
    }

    public static function getGroups(): array
    {
        return ['test', 'prod', 'room'];
    }
}
