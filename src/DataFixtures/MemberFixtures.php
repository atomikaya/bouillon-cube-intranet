<?php

namespace App\DataFixtures;

use App\Entity\Member;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class MemberFixtures extends Fixture implements FixtureGroupInterface
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        // cooperator with 2 month left
        $elias = (new Member())
            ->setName("Elias Mune")
            ->setEmail("elias.mune@exemple.fr")
            ->setIsActive(true);

        $manager->persist($elias);
        $this->addReference("member_elias", $elias);

        // neighbor waiting for pass activation
        $zenyth = (new Member())
            ->setName("Zenyth Lapuis")
            ->setEmail("zlap@exemple.fr")
            ->setIsNeighbor(true)
            ->setIsActive(true);

        $manager->persist($zenyth);
        $this->addReference("member_zenyth", $zenyth);

        // cooperator & neighbor with 13 days left, waiting for renewal
        $ambre = (new Member())
            ->setName("Ambre Djennek")
            ->setEmail("adj@exemple.fr")
            ->setIsNeighbor(true)
            ->setIsActive(true);

        $manager->persist($ambre);
        $this->addReference("member_ambre", $ambre);

        // new member with no pass
        $kae = (new Member())
            ->setName("Kae Loiseau")
            ->setEmail("kael@exemple.fr")
            ->setIsActive(true);

        $manager->persist($kae);
        $this->addReference("member_kae", $kae);

        // member with expired pass
        $vinca = (new Member())
            ->setName("Vinca Sellier")
            ->setEmail("vs@exemple.fr")
            ->setIsActive(true);

        $manager->persist($vinca);
        $this->addReference("member_vinca", $vinca);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ["test"];
    }
}
