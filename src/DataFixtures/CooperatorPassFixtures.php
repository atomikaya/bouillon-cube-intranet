<?php

namespace App\DataFixtures;

use App\Entity\CooperatorPass;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CooperatorPassFixtures extends Fixture
implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        $eliasPass = (new CooperatorPass())
            ->setPeriod(new \DateTimeImmutable('+2 month'))
            ->setIsActive(true)
            ->setOwner($this->getReference('member_elias'))
        ;

        $manager->persist($eliasPass);
        $this->addReference('pass_elias', $eliasPass);

        $zenythPass = (new CooperatorPass())
            ->startNow()
            ->setIsWaiting(true)
            ->setOwner($this->getReference('member_zenyth'))
        ;

        $manager->persist($zenythPass);
        $this->addReference('pass_zenyth', $zenythPass);

        $ambrePass = (new CooperatorPass())
            ->setPeriod(new \DateTimeImmutable('+13 days'))
            ->setIsActive(true)
            ->setIsWaiting(true)
            ->setOwner($this->getReference('member_ambre'))
        ;

        $manager->persist($ambrePass);
        $this->addReference('pass_ambre', $ambrePass);

        $vincaPass = (new CooperatorPass())
            ->setPeriod(new \DateTimeImmutable('-1 day'))
            ->setIsActive(true)
            ->setIsWaiting(true)
            ->setOwner($this->getReference('member_vinca'))
        ;

        $manager->persist($vincaPass);
        $this->addReference('pass_vinca', $vincaPass);

        $manager->flush();
    }
    
    public function getDependencies(): array
    {
       return [MemberFixtures::class]; 
    }

    public static function getGroups(): array
    {
        return ['test'];
    }
}
