<?php

namespace App\DataFixtures;

use App\Entity\Member;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AdminFixtures extends Fixture implements FixtureGroupInterface
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $member = (new Member())
            ->setName("Équipe La Grange")
            ->setEmail("grange@bouilloncube.fr")
            ->setIsActive(true);
        $user = (new User())
            ->setEmail($member->getEmail())
            ->setOwner($member)
            ->setRoles(["ROLE_ADMIN"]);

        $user->setPassword($this->hasher->hashPassword($user, "password"));

        $manager->persist($user);
        $manager->persist($member);
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ["test", "prod"];
    }
}
