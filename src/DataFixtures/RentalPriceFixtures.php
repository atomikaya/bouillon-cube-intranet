<?php

namespace App\DataFixtures;

use App\Entity\RentalPrice;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class RentalPriceFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        // individual offices
        $cooperatorOffice1 = (new RentalPrice())
            ->setMonthlyRate(23800)
        ;

        $manager->persist($cooperatorOffice1);
        $this->addReference('rate_cooperator_office_1', $cooperatorOffice1);

        $cooperatorOffice2 = (new RentalPrice())
            ->setMonthlyRate(14000)
        ;

        $manager->persist($cooperatorOffice2);
        $this->addReference('rate_cooperator_office_2', $cooperatorOffice2);

        $cooperatorOffice3 = (new RentalPrice())
            ->setMonthlyRate(49000)
        ;

        $manager->persist($cooperatorOffice3);
        $this->addReference('rate_cooperator_office_3', $cooperatorOffice3);

        // shared office
        $cooperatorOfficeShared = (new RentalPrice())
            ->setHourlyRate(100)
            ->setDailyRate(500)
            ->setMonthlyRate(6300)
        ;

        $manager->persist($cooperatorOfficeShared);
        $this->addReference('rate_cooperator_office_shared', $cooperatorOfficeShared);

        $neighborOfficeShared = (new RentalPrice())
            ->setHourlyRate(200)
            ->setDailyRate(800)
            ->setMonthlyRate(10000)
        ;

        $manager->persist($neighborOfficeShared);
        $this->addReference('rate_neighbor_office_shared', $neighborOfficeShared);

        $outsiderOfficeShared = (new RentalPrice())
            ->setHourlyRate(300)
            ->setDailyRate(1000)
            ->setMonthlyRate(12000)
        ;

        $manager->persist($outsiderOfficeShared);
        $this->addReference('rate_outsider_office_shared', $outsiderOfficeShared);

        // open-air theater
        $cooperatorOpenAirTheater = (new RentalPrice())
            ->setHourlyRate(0)
            ->setDailyRate(0)
            ->setMaxDays(10)
            ->setOvertimeRate(500)
        ;

        $manager->persist($cooperatorOpenAirTheater);
        $this->addReference('rate_cooperator_open_air_theater', $cooperatorOpenAirTheater);
        
        // kiosk
        $cooperatorKiosk = (new RentalPrice())
            ->setHourlyRate(0)
            ->setDailyRate(0)
            ->setMaxHours(60)
            ->setFollowsPass(true)
            ->setOvertimeRate(100)
        ;

        $manager->persist($cooperatorKiosk);
        $this->addReference('rate_cooperator_kiosk', $cooperatorKiosk);

        // kitchen
        $cooperatorKitchen = (new RentalPrice())
            ->setHourlyRate(100)
            ->setDailyRate(1000)
        ;

        $manager->persist($cooperatorKitchen);
        $this->addReference('rate_cooperator_kitchen', $cooperatorKitchen);

        // other
        $cooperatorOther = (new RentalPrice())
            ->setHourlyRate(0)
            ->setDailyRate(0)
        ;

        $manager->persist($cooperatorOther);
        $this->addReference('rate_cooperator_other', $cooperatorOther);

        $neighborOther = (new RentalPrice())
            ->setHourlyRate(500)
            ->setDailyRate(3000)
        ;
        $manager->persist($neighborOther);
        $this->addReference('rate_neighbor_other', $neighborOther);

        $outsiderOther = (new RentalPrice())
            ->setHourlyRate(1000)
            ->setDailyRate(5000)
        ;

        $manager->persist($outsiderOther);
        $this->addReference('rate_outsider_other', $outsiderOther);

        // free
        $free = (new RentalPrice())
            ->setHourlyRate(0)
            ->setDailyRate(0)
        ;

        $manager->persist($free);
        $this->addReference('rate_free', $free);

        $manager->flush();

    }

    public static function getGroups(): array
    {
        return ['test', 'prod', 'room'];
    }
}
