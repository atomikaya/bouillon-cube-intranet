<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements
    DependentFixtureInterface,
    FixtureGroupInterface
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        // cooperator with 2 month left
        $elias = (new User())
            ->setEmail($this->getReference("member_elias")->getEmail())
            ->setOwner($this->getReference("member_elias"));

        $elias->setPassword($this->hasher->hashPassword($elias, "password"));
        $manager->persist($elias);

        // neighbor waiting for pass activation
        $zenyth = (new User())
            ->setEmail($this->getReference("member_zenyth")->getEmail())
            ->setOwner($this->getReference("member_zenyth"));

        $zenyth->setPassword($this->hasher->hashPassword($zenyth, "password"));
        $manager->persist($zenyth);

        // cooperator & neighbor with 13 days left, waiting for renewal
        $ambre = (new User())
            ->setEmail($this->getReference("member_ambre")->getEmail())
            ->setOwner($this->getReference("member_ambre"));

        $ambre->setPassword($this->hasher->hashPassword($ambre, "password"));
        $manager->persist($ambre);

        // new user with no pass
        $kae = (new User())
            ->setEmail($this->getReference("member_kae")->getEmail())
            ->setOwner($this->getReference("member_kae"));

        $kae->setPassword($this->hasher->hashPassword($kae, "password"));
        $manager->persist($kae);

        // user with expired pass
        $vinca = (new User())
            ->setEmail($this->getReference("member_vinca")->getEmail())
            ->setOwner($this->getReference("member_vinca"));

        $vinca->setPassword($this->hasher->hashPassword($vinca, "password"));
        $manager->persist($vinca);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [MemberFixtures::class];
    }

    public static function getGroups(): array
    {
        return ["test"];
    }
}
