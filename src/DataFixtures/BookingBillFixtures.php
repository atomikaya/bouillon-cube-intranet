<?php

namespace App\DataFixtures;

use App\Service\BillingCalculator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class BookingBillFixtures extends Fixture
implements DependentFixtureInterface, FixtureGroupInterface
{
    private BillingCalculator $calculator;

    public function __construct(BillingCalculator $calculator)
    {
        $this->calculator = $calculator;
    }

    public function load(ObjectManager $manager): void
    {
        // pass bills
        $passBills = [
            $this->calculator->estimatePass($this->getReference('pass_elias')),
            $this->calculator->estimatePass($this->getReference('pass_ambre')),
            $this->calculator->estimatePass($this->getReference('pass_vinca')),
        ];

        foreach ($passBills as $bill) {
            $manager->persist($bill);
        }

        // booking bills
        $bookingBills = array_merge(
            $this->calculator->estimate($this->getReference('booking_zenyth_1')),
            $this->calculator->estimate($this->getReference('booking_zenyth_2')),
            $this->calculator->estimate($this->getReference('booking_zenyth_3')),
            $this->calculator->estimate($this->getReference('booking_vinca_1')),
        );

        foreach ($bookingBills as $bill) {
            $manager->persist($bill);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            BookingFixtures::class,
            CooperatorPassFixtures::class,
        ];
    }

    public static function getGroups(): array
    {
        return ['test'];
    }
}
