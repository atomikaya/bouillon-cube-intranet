<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueBooking extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'booking.duplicate';

    public function getTargets(): string|array
    {
        return self::CLASS_CONSTRAINT;
    }
}
