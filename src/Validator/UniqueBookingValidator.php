<?php

namespace App\Validator;

use App\Entity\Booking;
use App\Repository\BookingRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use UnexpectedValueException;

class UniqueBookingValidator extends ConstraintValidator
{
    private BookingRepository $repository;

    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;
    }

    /** 
     * @var Booking $booking
     * @var UniqueBooking $constraint
     */
    public function validate($booking, Constraint $constraint)
    {

        if (null === $booking) {
            return;
        }

        if (!$booking instanceof Booking) {
            throw new UnexpectedValueException($booking, Booking::class);
        }

        $duplicates = $this->repository->findDuplicates($booking);

        if (count($duplicates) > 0) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
