<?php

namespace App\Service;

use App\Entity\Room;
use App\Repository\BookingRepository;
use Eluceo\iCal\Domain\Entity\Calendar;
use Eluceo\iCal\Domain\Entity\Event;
use Eluceo\iCal\Domain\ValueObject\Date;
use Eluceo\iCal\Domain\ValueObject\DateTime;
use Eluceo\iCal\Domain\ValueObject\EmailAddress;
use Eluceo\iCal\Domain\ValueObject\Location;
use Eluceo\iCal\Domain\ValueObject\MultiDay;
use Eluceo\iCal\Domain\ValueObject\Organizer;
use Eluceo\iCal\Domain\ValueObject\SingleDay;
use Eluceo\iCal\Domain\ValueObject\TimeSpan;
use Eluceo\iCal\Presentation\Factory\CalendarFactory;
use Symfony\Contracts\Translation\TranslatorInterface;

class IcalGenerator
{
    private BookingRepository $repository;

    private TranslatorInterface $translator;

    public function __construct(
        BookingRepository $repository,
        TranslatorInterface $translator
    ) {
        $this->repository = $repository;
        $this->translator = $translator;
    }

    public function generateIcs(Room $room): string
    {
        $factory = new CalendarFactory();
        $events = [];

        $bookings = $this->repository->findBy(["place" => $room]);

        foreach ($bookings as $booking) {
            $applicant = $booking->getApplicant();
            $description = "";
            $tag = "";

            if ($booking->getIsApproved()) {
                $description = $this->translator->trans(
                    "booking.event.approved",
                    [
                        "%name%" => $applicant->getName(),
                        "%place%" => $room->getName(),
                    ]
                );
                $tag = $this->translator->trans("booking.event.approvedTag");
            } else {
                $description = $this->translator->trans(
                    "booking.event.request",
                    [
                        "%name%" => $applicant->getName(),
                        "%place%" => $room->getName(),
                    ]
                );
                $tag = $this->translator->trans("booking.event.requestTag");
            }

            $event = (new Event())
                ->setSummary($tag . " " . $applicant->getName())
                ->setDescription($description)
                ->setOrganizer(new Organizer(
                    new EmailAddress($applicant->getEmail()),
                    $applicant->getName()
                ))
                ->setLocation(new Location($room->getName()));

            if ($booking->isFullDays()) {
                if ($booking->getStart()->format('Y-m-d') == $booking->getEnd()->format('Y-m-d')) {
                    $event->setOccurrence(new SingleDay(new Date($booking->getStart())));
                } else {
                    $event->setOccurrence(new MultiDay(
                        new Date($booking->getStart()),
                        new Date($booking->getEnd())
                    ));
                }
            } else {
                $event->setOccurrence(new TimeSpan(
                    new DateTime($booking->getStart(), true),
                    new DateTime($booking->getEnd(), true)
                ));
            }

            $events[] = $event;
        }

        return $factory->createCalendar(new Calendar($events));
    }
}
