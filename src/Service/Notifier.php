<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

class Notifier
{
    private RequestStack $request;
    
    private TranslatorInterface $translator;

    public function __construct(
        RequestStack $request,
        TranslatorInterface $translator
    ) {
        $this->request = $request;
        $this->translator = $translator;
    }

    public function addDanger(string $actionId, array $params = []): self
    {
        return $this->add('danger', $actionId, $params);
    }

    public function addInfo(string $actionId, array $params = []): self
    {
        return $this->add('info', $actionId, $params);
    }

    public function addSuccess(string $actionId, array $params = []): self
    {
        return $this->add('success', $actionId, $params);
    }

    public function addWarning(string $actionId, array $params = []): self
    {
        return $this->add('warning', $actionId, $params);
    }

    public function peekAll(): array
    {
        return $this->request->getSession()->getFlashbag()->peekAll();
    }

    public function getAll():array
    {
        return $this->request->getSession()->getFlashbag()->all();
    }

    private function add(
        string $result,
        string $actionId,
        array $params
    ): self {
        $this->request->getSession()->getFlashbag()->add(
            $result,
            $this->translator->trans(
                $actionId,
                $params,
                'flashes'
            )
        );

        return $this;
    }
}
