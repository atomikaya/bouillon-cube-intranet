<?php

namespace App\Service;

class ColorBalancer
{
    const HUE_MAX = 360;

    // wheel as HSL CSS color strings from size
    public function generateHSLWheel(
        int $size,
        bool $contrasted = false
    ): array {
        $wheel = $contrasted
            ? $this->generateContrastedWheel($size)
            : $this->generateWheel($size);
        $hsl = [];

        foreach ($wheel as [$h, $s, $l]) {
            $hsl[] = "hsl($h, $s%, $l%)";
        }

        return $hsl;
    }

    // hue wheel where $size colors are  spread as far apart chromatically as
    // possible
    private function generateWheel(
        int $size,
        int $saturation = 50,
        int $luminosity = 50
    ): array {
        if ($size < 1) {
            return [];
        }

        $wheel = [];
        $step = intdiv(self::HUE_MAX, $size);

        for ($hue = 0; $hue < self::HUE_MAX; $hue += $step) { 
            $wheel[] = [$hue, $saturation, $luminosity];
        }

        return $wheel;
    }

    // hue wheel where saturation and luminosity alternate for better contrast
    private function generateContrastedWheel(int $size): array {
        if ($size < 1) {
            return [];
        }

        $wheel = [];
        $step = intdiv(self::HUE_MAX, $size);
        $strong = true;
        $bright = true;

        for ($hue = 0; $hue < self::HUE_MAX; $hue += $step) {
            $wheel[] = [$hue, $strong ? 70 : 50, $bright ? 50 : 35];
            $strong = !$strong;
            $bright = !$bright;
        }

        return $wheel;
    }
}
