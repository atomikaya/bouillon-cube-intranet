<?php

namespace App\Service;

use App\Entity\Booking;
use App\Entity\BookingBill;
use App\Entity\CooperatorPass;
use App\Entity\RentalPrice;
use App\Entity\Room;
use App\Entity\Member;
use App\Repository\BookingBillRepository;
use App\Repository\BookingRepository;
use App\Repository\CooperatorPassRepository;
use App\Util\AccurateDiff;
use App\Util\BookingHistory;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;

class BillingCalculator
{
    private BillingAccess $access;

    private BookingBillRepository $billingRepository;

    private BookingRepository $bookingRepository;

    private CooperatorPassRepository $passRepository;

    private EntityManagerInterface $manager;

    public function __construct(
        BillingAccess $access,
        BookingBillRepository $billingRepository,
        BookingRepository $bookingRepository,
        CooperatorPassRepository $passRepository,
        EntityManagerInterface $manager
    ) {
        $this->access = $access;
        $this->billingRepository = $billingRepository;
        $this->bookingRepository = $bookingRepository;
        $this->passRepository = $passRepository;
        $this->manager = $manager;
    }

    public function hasRate(Booking $booking): bool
    {
        $rate = $booking
            ->getPlace()
            ->getRateForAccessLevel(
                $this->access->grantLevel($booking->getApplicant())
            );

        return $rate instanceof RentalPrice;
    }

    public function sumPrices(array $bills): int
    {
        $price = 0;

        /** @var BookingBill $bill */
        foreach ($bills as $bill) {
            $price += $bill->getPrice();
        }

        return $price;
    }

    public function estimatePass(
        CooperatorPass $pass,
        bool $renew = false
    ): BookingBill {
        $nextBillingDay = 'first day of next month midnight';
        $due = $renew
            ? new \DateTimeImmutable($nextBillingDay)
            : $pass->getStart()->modify($nextBillingDay);
        
        return (new BookingBill())
            ->setReceiver($pass->getOwner())
            ->setDue($due)
            ->setPrice(CooperatorPass::PRICE)
        ;
    }

    public function updateAfter(Booking $booking)
    {
        // list future bookings for same place
        $futureBills = $this->billingRepository->findFutureByPlaceAndApplicant(
            $booking->getStart(),
            $booking->getPlace(),
            $booking->getApplicant()
        );
        $future = [];

        /** @var BookingBill $bill */
        foreach ($futureBills as $bill) {
            if (!in_array($bill->getSource(), $future, true)) {
                $future[] = $bill->getSource();
            }

            $this->manager->remove($bill);
        }

        // re-estimate
        /** @var Booking $futureBooking */
        foreach ($future as $futureBooking) {
            $newBills = $this->estimate($futureBooking);

            foreach ($newBills as $bill) {
                $this->manager->persist($bill);
            }
        }

        $this->manager->flush();
    }

    public function resetAll()
    {
        foreach ($this->billingRepository->findAll() as $bill) {
            $this->manager->remove($bill);
        }

        $this->manager->flush();

        foreach ($this->bookingRepository->findAll() as $booking) {
            foreach ($this->estimate($booking) as $bill) {
                $this->manager->persist($bill);
            }
        }

        $this->manager->flush();
    }

    public function estimate(Booking $booking): array
    {
        $applicant = $booking->getApplicant();

        if (in_array('ROLE_ADMIN', $applicant->getAccess()->getRoles())) {
            return [];
        }

        $place = $booking->getPlace();
        $rate = $place->getRateForAccessLevel(
            $this->access->grantLevel($applicant)
        );
        $pass = $rate->getFollowsPass()
            ? $this->passRepository->findActiveByOwner($applicant)
            : null
        ;

        switch ($booking->getUnit()) {
            case Booking::UNIT_HOUR:
                return $this->estimateHours(
                    $booking,
                    $place,
                    $applicant,
                    $rate,
                    $pass
                );
            
            case Booking::UNIT_DAY:
                return $this->estimateDays(
                    $booking,
                    $place,
                    $applicant,
                    $rate,
                    $pass
                );
            
            case Booking::UNIT_MONTH:
                return $this->estimateMonths(
                    $booking,
                    $place,
                    $applicant,
                    $rate
                );
            
            default:
                break;
        }

        return [];
    }

    /* max of 23.5 hours restricted by the ui */
    private function estimateHours(
        Booking $booking,
        Room $place,
        Member $applicant,
        RentalPrice $rate,
        ?CooperatorPass $pass
    ): array {
        // get bookings for year
        $bookingHistory = new BookingHistory(
            $this->bookingRepository->findYearlyByPlaceAndApplicant(
                $booking->getStart(),
                $booking->getEnd(),
                $place,
                $applicant
            )
        );
        
        $allowance = $this->sumAllowanceHours($booking, $rate, $pass, $bookingHistory);

        $price = $this->applyHourlyRate(
            $booking->getDuration(),
            $rate,
            $allowance
        );

        return [
            (new BookingBill())
                ->setSource($booking)
                ->setReceiver($applicant)
                ->setDue($booking->getStart()->modify('first day of next month midnight'))
                ->setPrice($price)
            ,
        ];
    }

    /* can span multiple years */
    private function estimateDays(
        Booking $booking,
        Room $place,
        Member $applicant,
        RentalPrice $rate,
        ?CooperatorPass $pass
    ): array {
        // get bookings for years
        $bookingHistory = new BookingHistory(
            $this->bookingRepository->findYearlyByPlaceAndApplicant(
                $booking->getStart(),
                $booking->getEnd(),
                $place,
                $applicant
            )
        );

        // restrict bookings to pass duration if follows pass
        if ($rate->getFollowsPass() && $pass) {
            $bookingHistory = new BookingHistory(
                $bookingHistory->filter(
                    $booking->getStart(),
                    $pass->getLastRenewal(),
                    $pass->getEnd()
                )
            );
        }

        // if there is a monthly allowance
        if ($rate->hasMaxHours()) {
            $bills = [];
            // by month
            for (
                $thisMonth = $booking->getStart();
                $thisMonth < $booking->getEnd();
                $thisMonth = $thisMonth->modify('first day of next month midnight')
            ) {
                $allowance = null;
                $nextMonth = $thisMonth->modify('first day of next month midnight');
                // filter bookings for the month
                $monthlyBookings = $bookingHistory->filterMonth($thisMonth);
                // get allowance for the month
                $maxForMonth = $this->getAllowance(
                    $rate->getMaxHours(),
                    $monthlyBookings
                );
                // if allowance is zero retrieve price for the month
                if ($maxForMonth <= 0) {
                    $allowance = $maxForMonth;
                } elseif ($rate->hasMaxDays()) {
                    // else get yearly allowance
                    // filter bookings for the year
                    $thisYearBookings = $bookingHistory->filterYear($thisMonth);
                    // get allowance for the year
                    $maxForYear = $this->getAllowance(
                        $rate->getMaxDays() *24,
                        $thisYearBookings
                    );
                    // go with the smallest one and retrieve price
                    $allowance = min($maxForMonth, $maxForYear);
                } else {
                    $allowance = $rate->getMaxHours();
                }
                // add the monthly booking chunk to the yearly bookings
                $bookingChunk = [
                    'start' => max($booking->getStart(), $thisMonth),
                    'end' => min($booking->getEnd(), $nextMonth),
                ];
                $bookingHistory->push($bookingChunk);
                // add to price the applied dailyrate
                $price = $this->applyDailyRate(
                    AccurateDiff::diff($bookingChunk['start'], $bookingChunk['end']),
                    $rate,
                    $allowance
                );
                $bills[] = (new BookingBill())
                    ->setSource($booking)
                    ->setReceiver($applicant)
                    ->setDue($nextMonth)
                    ->setPrice($price)
                ;
                // go to next month
            }
            // return cumulated price
            return $bills;
        }

        // else if max days
        if ($rate->hasMaxDays()) {
            $bills = [];
            // per year
            for (
                $thisYear = $booking->getStart();
                $thisYear < $booking->getEnd();
                $thisYear = $thisYear->modify('1st january next year midnight')
            ) {
                $nextYear = $thisYear->modify('1st january next year midnight');
                // filter bookings for the year
                $thisYearBookings = $bookingHistory->filterYear($thisYear);

                for (
                    $thisMonth = $thisYear;
                    $thisMonth < min($booking->getEnd(), $nextYear);
                    $thisMonth = $thisMonth->modify('first day of next month midnight')
                ) {
                    $allowance = null;
                    $nextMonth = $thisMonth->modify('first day of next month midnight');
                    // get yearly allowance
                    $allowance = $this->getAllowance(
                        $rate->getMaxDays() *24,
                        $thisYearBookings
                    );
                    // calculate this month chunk end
                    $chunkEnd = min($booking->getEnd(), $nextMonth);
                    $thisYearBookings[] = [
                        'start' => $thisMonth,
                        'end' => $chunkEnd,
                    ];
                    // add to price
                    $price = $this->applyDailyRate(
                        AccurateDiff::diff($thisMonth, $chunkEnd),
                        $rate,
                        $allowance
                    );
                    // fwrite(
                    //     STDERR,
                    //     PHP_EOL.'start('.print_r($booking->getStart()->format('d/m/Y'), true).')'.
                    //     PHP_EOL.'end('.print_r($booking->getEnd()->format('d/m/Y'), true).')'.
                    //     PHP_EOL.'this year('.print_r($thisYear->format('d/m/Y'), true).')'.
                    //     PHP_EOL.'this month('.print_r($thisMonth->format('d/m/Y'), true).')'.
                    //     PHP_EOL.'next month('.print_r($nextMonth->format('d/m/Y'), true).')'.
                    //     PHP_EOL.'price('.print_r($price, true).')'.
                    //     PHP_EOL
                    // );
                    $bills[] = (new BookingBill())
                        ->setSource($booking)
                        ->setReceiver($applicant)
                        ->setDue($nextMonth)
                        ->setPrice($price)
                    ;
                }
            }

            return $bills;
        }
        // return price
        $bills = [];

        for (
            $thisMonth = $booking->getStart();
            $thisMonth < $booking->getEnd();
            $thisMonth = $thisMonth->modify('first day of next month midnight')
        ) {
            $nextMonth = $thisMonth->modify('first day of next month midnight');
            
            // calculate this month chunk end
            $chunkEnd = min($booking->getEnd(), $nextMonth);
            // add to price
            $price = $this->applyDailyRate(
                AccurateDiff::diff($thisMonth, $chunkEnd),
                $rate,
            );
            $bills[] = (new BookingBill())
                ->setSource($booking)
                ->setReceiver($applicant)
                ->setDue($nextMonth)
                ->setPrice($price)
            ;
        }

        return $bills;
    }

    private function estimateMonths(
        Booking $booking,
        Room $place,
        Member $applicant,
        RentalPrice $rate
    ): array {
        $duration = $booking->getDuration();
        $months = $duration->y * 12 + $duration->m;
        $bills = [];

        for ($i = 0; $i < $months; $i++) {
            $dueIn = $i + 1;
            $bills[] = (new BookingBill())
                ->setSource($booking)
                ->setReceiver($applicant)
                ->setDue($booking->getStart()->modify("+$dueIn months"))
                ->setPrice($rate->getMonthlyRate())
            ;
        }

        return $bills;
    }

    private function getAllowance(
        int $maxHours,
        array $bookings
    ): float {
        $hoursSpent = 0;

        /**
         * @var \DateTimeImmutable $start
         * @var \DateTimeImmutable $end
         */
        foreach ($bookings as ['start' => $start, 'end' => $end]) {
            $interval = AccurateDiff::diff($start, $end);

            $hoursSpent += $this->toHours($interval);
        }

        return $hoursSpent >= $maxHours ? 0 : $maxHours - $hoursSpent;
    }

    private function sumAllowanceHours(
        Booking $booking,
        RentalPrice $rate,
        ?CooperatorPass $pass,
        BookingHistory $bookingHistory
    ): ?int {
        if ($rate->hasMaxHours()) {
            // filter bookings for this month or further back if the overtime
            // follows pass renewal
            if ($rate->getFollowsPass() && $pass) {
                $monthlyBookings = $bookingHistory->filter(
                    $booking->getStart(),
                    $pass->getLastRenewal(),
                    $pass->getEnd()
                );
            } else {
                $monthlyBookings = $bookingHistory->filterDuration(
                    $booking->getStart(),
                    $rate->getHourSeeking()
                );
            }

            // get allowance
            $maxForMonth = $this->getAllowance(
                $rate->getMaxHours(),
                $monthlyBookings
            );

            // if maxHours is 0 go with that
            if ($maxForMonth <= 0) {
                return $maxForMonth;

                // else if there is yearly allowance
            } elseif ($rate->hasMaxDays()) {
                $thisYearBookings = $bookingHistory->filterYear(
                    $booking->getStart()
                );

                // else get allowance for year
                $maxForYear = $this->getAllowance(
                    $rate->getMaxDays() * 24,
                    $thisYearBookings
                );

                // go with the smallest one
                return min($maxForMonth, $maxForYear);
            }
            
            return $maxForMonth;
        }

        return null;
    }

    private function toHours(\DateInterval $di): float
    {
        return $di->days * 24 + $di->h + ($di->i > 0 ? $di->i / 60 : 0);
    }

    private function applyHourlyRate(
        DateInterval $interval,
        RentalPrice $rate,
        float $allowance = null
    ): int {
        $hours = $this->toHours($interval);

        if (is_null($allowance)) {
            return $hours * $rate->getHourlyRate();
        } elseif ($allowance <= 0) {
            return $hours * $rate->getOvertimeRate();
        }

        $overtime = 0;

        if ($hours > $allowance) {
            $overtime = $hours - $allowance;
            $hours = $allowance;
        }

        return $hours * $rate->getHourlyRate()
            + $overtime * $rate->getOvertimeRate(); 
    }

    private function applyDailyRate(
        DateInterval $interval,
        RentalPrice $rate,
        float $allowance = null
    ): int {
        $days = $interval->days;
        
        if (is_null($allowance)) {
            return $days * $rate->getDailyRate();
        } elseif ($allowance <= 0) {
            return $days * 24 * $rate->getOvertimeRate();
        }
        
        $hours = $this->toHours($interval);
        $overtime = 0;

        if ($hours > $allowance) {
            $overtime = $hours - $allowance;
            $days -= ceil($overtime / 24);
            $hours = $allowance;
        }

        return $hours * $rate->getHourlyRate()
            + $days * $rate->getDailyRate()
            + $overtime * $rate->getOvertimeRate(); 
    }
}
