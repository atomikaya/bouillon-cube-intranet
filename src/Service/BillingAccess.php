<?php

namespace App\Service;

use App\Entity\Member;
use App\Repository\MemberRepository;

class BillingAccess
{
    private MemberRepository $repository;

    public function __construct(MemberRepository $repository)
    {
        $this->repository = $repository;
    }

    public function grantLevel(Member $member): string
    {
        $info = $this->repository->findOneWithPass($member);

        return $info['isCooperator']
            ? Member::LEVEL_COOPERATOR
            : ($info['isNeighbor']
            ? Member::LEVEL_NEIGHBOR
            : Member::LEVEL_OUTSIDER)
        ;
    }
}
