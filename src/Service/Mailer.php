<?php

namespace App\Service;

use App\Entity\Booking;
use App\Entity\CooperatorPass;
use App\Entity\MailContactInterface;
use App\Entity\Member;
use App\Entity\ResetLink;
use App\Repository\UserRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Mailer
{
    const REGISTRATION_CREATED = 'registration_created';

    const REGISTRATION_APPROVED = 'registration_approved';

    const REGISTRATION_DENIED = 'registration_denied';

    const MEMBER_DISABLED = 'member_disabled';

    const PASS_REQUESTED = 'pass_requested';

    const PASS_DENIED = 'pass_denied';

    const PASS_APPROVED = 'pass_approved';

    const PASS_INTERRUPTED = 'pass_interrupted';

    const PASS_EXPIRED = 'pass_expired';

    const PASS_EXPIRED_REPORT = 'pass_expired_report';

    const PASS_EXPIRING = 'pass_expiring';

    const RENEWAL_REQUESTED = 'renewal_requested';

    const RENEWAL_APPROVED = 'renewal_approved';

    const RENEWAL_DENIED = 'renewal_denied';

    const BOOKING_REQUESTED = 'booking_requested';

    const BOOKING_APPROVED = 'booking_approved';

    const BOOKING_DENIED = 'booking_denied';

    const BOOKING_CANCELLED = 'booking_cancelled';

    const BOOKING_CANCELLED_REPORT = 'booking_cancelled_report';

    const BILLING_DUE = 'billing_due';

    const BILLING_DUE_REPORT = 'billing_due_report';

    const PASSWORD_RESET_LINK = 'password_reset_link';

    private MailerInterface $mailer;
    
    private UrlGeneratorInterface $router;

    private TranslatorInterface $translator;

    private BillingCalculator $calculator;

    private \NumberFormatter $number;

    private \IntlDateFormatter $month;

    private Member $admin;

    private static string $helpDesk = 'contact-project+atomikaya-bouillon-cube-intranet-30069484-issue-@incoming.gitlab.com';

    private static string $dnsTester = 'Nick Searle <nickelastic@orange.fr>';

    public function __construct(
        MailerInterface $mailer,
        UrlGeneratorInterface $router,
        TranslatorInterface $translator,
        BillingCalculator $calculator,
        UserRepository $repository
    ) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->translator = $translator;
        $this->calculator = $calculator;
        $this->number = new \NumberFormatter(
            'fr-FR',
            \NumberFormatter::DECIMAL
        );
        $this->month = new \IntlDateFormatter(
            'fr-FR',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            'Europe/Paris',
            null,
            'MMMM'
        );
        $this->admin = $repository->findAdmin()->getOwner();
    }

    public function sendRegistrationCreated(MailContactInterface $recipient): void
    {
        $this->send(self::REGISTRATION_CREATED, $recipient);
    }

    public function sendRegistrationApproved(
        MailContactInterface $recipient,
        string $plainPassword
    ): void {
        $email = $this->send(self::REGISTRATION_APPROVED, $recipient, [
            '%password%' => $plainPassword,
            '%url%' => $this->router->generate(
                'login',
                [],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
        ]);
    }

    public function sendRegistrationDenied(MailContactInterface $recipient): void
    {
        $this->send(self::REGISTRATION_DENIED, $recipient);
    }

    public function sendMemberDisabled(MailContactInterface $recipient): void
    {
        $this->send(self::MEMBER_DISABLED, $recipient);
    }

    public function sendPassRequested(MailContactInterface $recipient): void
    {
        $this->send(self::PASS_REQUESTED, $recipient);
    }

    public function sendPassDenied(MailContactInterface $recipient): void
    {
        $this->send(self::PASS_DENIED, $recipient);
    }

    public function sendPassApproved(
        MailContactInterface $recipient,
        CooperatorPass $pass
    ): void {
        $email = $this->send(self::PASS_APPROVED, $recipient, [
            '%end%' => $pass->getEnd()->format('d/m/Y'),
        ]);
    }

    public function sendPassInterrupted(
        MailContactInterface $recipient,
        string $level
    ): void {
        $email = $this->send(self::PASS_INTERRUPTED, $recipient, [
            '%level%' => $this->translator->trans("member.level.$level"),
        ]);
    }

    public function sendPassExpired(
        MailContactInterface $recipient,
        string $level
    ): void {
        $email = $this->send(self::PASS_EXPIRED, $recipient, [
            '%level%' => $this->translator->trans("member.level.$level"),
        ]);
    }

    public function sendPassExpiredReport(array $report): void
    {
        $passes = '';

        foreach ($report['passes'] as $row) {
            $passes .= $this->translator->trans(
                self::PASS_EXPIRED_REPORT . '.passes_item',
                [
                    '%name%' => $row['name'],
                    '%level%' => $this->translator->trans(
                        'member.level.' . $row['level']
                    ),
                ],
                'emails'
            );
        }

        $bookings = '';

        if (key_exists('bookings', $report)) {
            foreach ($report['bookings'] as $row) {
                if ($row['isDeleted']) {
                    $status = $this->translator->trans(
                        self::PASS_EXPIRED_REPORT . '.status_deleted',
                        [],
                        'emails'
                    );
                } else {
                    $status = $this->translator->trans(
                        self::PASS_EXPIRED_REPORT . '.status_raised',
                        ['%price%' => $this->number->format($row['price'] / 100)],
                        'emails'
                    );
                }
    
                $bookings .= $this->translator->trans(
                    self::PASS_EXPIRED_REPORT . '.bookings_item',
                    [
                        '%place%' => $row['booking']->getPlace()->getName(),
                        '%start%' => $row['booking']->getStart()->format('d/m/Y H:i'),
                        '%end%' => $row['booking']->getEnd()->format('d/m/Y H:i'),
                        '%status%' => $status,
                    ],
                    'emails'
                );
            }
        }

        $email = $this->send(self::PASS_EXPIRED_REPORT, $this->admin, [
            '%count%' => count($report['passes']),
            '%passes%' => $passes,
            '%bookings%' => $bookings,
        ]);
    }

    public function sendPassExpiring(
        MailContactInterface $recipient,
        CooperatorPass $pass
    ): void {
        $email = $this->send(self::PASS_EXPIRING, $recipient, [
            '%end%' => $pass->getEnd()->format('d/m/Y'),
            '%url%' => $this->router->generate(
                'member_me',
                [],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
        ]);
    }

    public function sendRenewalRequested(MailContactInterface $recipient): void
    {
        $this->send(self::RENEWAL_REQUESTED, $recipient);
    }

    public function sendRenewalApproved(
        MailContactInterface $recipient,
        CooperatorPass $pass
    ): void {
        $email = $this->send(self::RENEWAL_APPROVED, $recipient, [
            '%end%' => $pass->getEnd()->format('d/m/Y'),
        ]);
    }

    public function sendRenewalDenied(MailContactInterface $recipient): void
    {
        $this->send(self::RENEWAL_DENIED, $recipient);
    }

    public function sendBookingRequested(
        MailContactInterface $recipient,
        Booking $booking
    ): void {
        $email = $this->send(self::BOOKING_REQUESTED, $recipient, [
            '%place%' => $booking->getPlace()->getName(),
            '%start%' => $booking->getStart()->format('d/m/Y'),
            '%end%' => $booking->getDisplayEnd()->format('d/m/Y'),
        ]);
    }

    public function sendBookingApproved(
        MailContactInterface $recipient,
        Booking $booking
    ): void {
        $price = $this->calculator->sumPrices($booking->getCosts()->toArray());
        $email = $this->send(self::BOOKING_APPROVED, $recipient, [
            '%place%' => $booking->getPlace()->getName(),
            '%start%' => $booking->getStart()->format('d/m/Y'),
            '%end%' => $booking->getDisplayEnd()->format('d/m/Y'),
            '%price%' => $this->number->format($price / 100),
        ]);
    }

    public function sendBookingDenied(
        MailContactInterface $recipient,
        Booking $booking
    ): void {
        $email = $this->send(self::BOOKING_DENIED, $recipient, [
            '%place%' => $booking->getPlace()->getName(),
            '%start%' => $booking->getStart()->format('d/m/Y'),
            '%end%' => $booking->getDisplayEnd()->format('d/m/Y'),
            '%url%' => $this->router->generate(
                'booking_new',
                [],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
        ]);
    }

    public function sendBookingCancelled(
        MailContactInterface $recipient,
        Booking $booking
    ): void {
        $email = $this->send(self::BOOKING_CANCELLED, $recipient, [
            '%place%' => $booking->getPlace()->getName(),
            '%start%' => $booking->getStart()->format('d/m/Y'),
            '%end%' => $booking->getDisplayEnd()->format('d/m/Y'),
        ]);

        $report = $this->send(self::BOOKING_CANCELLED_REPORT, $this->admin, [
            '%name%' => $recipient->getName(),
            '%place%' => $booking->getPlace()->getName(),
            '%start%' => $booking->getStart()->format('d/m/Y'),
            '%end%' => $booking->getDisplayEnd()->format('d/m/Y'),
        ]);
    }

    public function sendBillingDue(
        MailContactInterface $recipient,
        array $bills
    ): void {
        $month = $bills[0]->getDue()->modify('previous month');
        $price = $this->calculator->sumPrices($bills);
        $email = $this->send(self::BILLING_DUE, $recipient, [
            '%month%' => $this->month->format($month),
            '%price%' => $this->number->format($price / 100),
        ]);
    }

    public function sendBillingDueReport(array $report): void
    {
        $month = (new \DateTimeImmutable())->modify('previous month');
        $list = '';

        foreach ($report as $row) {
            $price = $this->calculator->sumPrices($row['bills']);
            $list .= $this->translator->trans(
                self::BILLING_DUE_REPORT . '.item',
                [
                    '%name%' => $row['name'],
                    '%price%' => $this->number->format($price / 100),
                ],
                'emails'
            );
        }

        $email = $this->send(self::BILLING_DUE_REPORT, $this->admin, [
            '%month%' => $this->month->format($month),
            '%count%' => count($report),
            '%list%' => $list,
            '%url%' => $this->router->generate(
                'booking_bill',
                [],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
        ]);
    }

    public function sendPasswordResetLink(
        MailContactInterface $recipient,
        ResetLink $link
    ): void {
        $email = $this->send(self::PASSWORD_RESET_LINK, $recipient, [
            '%url%' => $this->router->generate('password_reset', [
                'token' => $link->getToken(),
            ], UrlGeneratorInterface::ABSOLUTE_URL),
            '%time%' => $link->getExpiration()->format('H:i:s'),
        ]);
    }

    public function sendBugReport(
        Member $sender,
        array $report
    ): void {
        $email = (new Email())
            ->from($this->formatContact($sender))
            ->to(self::$helpDesk)
            ->subject($report['subject'])
            ->text($report['text'])
        ;

        $this->sendSimple($email);
    }

    // test

    public function sendDnsTest(): void
    {
        $email = (new Email())
            ->from($this->formatContact($this->admin))
            ->to(self::$dnsTester)
            ->subject('Test email: is it working?')
            ->text('It certainly seems to work, good job!')
        ;

        $this->sendSimple($email);
    }

    // helpers

    private function send(
        string $message,
        MailContactInterface $recipient,
        array $params = []
    ): void {
        try {
            $this->mailer->send($this->create($message, $recipient, $params));
        } catch (\Throwable $e) {}
    }

    private function sendSimple(Email $email): void {
        try {
            $this->mailer->send($email);
        } catch (\Throwable $e) {}
    }

    private function create(
        string $message,
        MailContactInterface $recipient,
        array $params = []
    ): Email {
        $params = array_merge([
            '%name%' => $recipient->getName(),
            '%email%' => $recipient->getEmail(),
        ], $params);

        return (new Email())
            ->from($this->formatContact($this->admin))
            ->to($this->formatContact($recipient))
            ->subject($this->translator->trans(
                "$message.subject",
                $params,
                'emails'
            ))
            ->text($this->translator->trans(
                "$message.text",
                $params,
                'emails'
            ))
        ;
    }

    private function formatContact(MailContactInterface $contact): string
    {
        return $contact->getName() . '<' . $contact->getEmail() . '>';
    }
}
