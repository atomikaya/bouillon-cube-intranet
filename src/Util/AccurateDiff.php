<?php

namespace App\Util;

class AccurateDiff
{
    public static function diff(\DateTimeInterface $a, \DateTimeInterface $b): \DateInterval
    {
        // hack: at the time of writing, diff fails to compute months correctly
        // with Europe/Paris timezone
        date_default_timezone_set('UTC');
        
        $a = new \DateTimeImmutable($a->format('Y-m-d\TH:i:s'));
        $b = new \DateTimeImmutable($b->format('Y-m-d\TH:i:s'));
        $interval = $a->diff($b);

        date_default_timezone_set('Europe/Paris');

        return $interval;
    }

    public static function getMonths(\DateInterval $interval): int
    {
        return $interval->y * 12 + $interval->m;
    }
}