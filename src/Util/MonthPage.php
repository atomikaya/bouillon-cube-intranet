<?php

namespace App\Util;

class MonthPage
{
    private int $number;

    private string $uniqueMonth;

    private int $count;

    public function __construct(int $page, array $data)
    {
        $this->number = $page;
        $this->uniqueMonth = $data['uniqueMonth'];
        $this->count = $data['count'];
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getUniqueMonth(): string
    {
        return $this->uniqueMonth;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}
