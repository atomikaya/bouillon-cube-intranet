<?php

namespace App\Util;

class MonthPaginator
{
    private MonthPage $previous;

    private MonthPage $current;

    private MonthPage $next;

    public function __construct(int $page, array $months)
    {
        if (count($months) < 1) {
            return;
        }

        $this->current = (new MonthPage($page, $months[$page - 1]));
        
        if ($page > 1) {
            $this->previous = (new MonthPage($page - 1, $months[$page - 2]));
        }

        if ($page < count($months)) {
            $this->next = (new MonthPage($page + 1, $months[$page]));
        }
    }

    public function getPrevious(): ?MonthPage
    {
        return $this->previous ?? null;
    }

    public function getCurrent(): ?MonthPage
    {
        return $this->current ?? null;
    }

    public function getNext(): ?MonthPage
    {
        return $this->next ?? null;
    }
}
