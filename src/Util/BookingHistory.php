<?php

namespace App\Util;

class BookingHistory
{
    private array $history;

    public function __construct(array $history)
    {
        $this->history = $history;
    }

    public function filterDuration(
        \DateTimeImmutable $date,
        string $duration
    ): array {
        // align start and end on full months
        $start = $date
            ->modify("-$duration")
            ->modify('first day of next month midnight')
        ;
        $end = $date->modify('first day of next month midnight');

        return $this->filter($date, $start, $end);
    }

    public function filterMonth(\DateTimeImmutable $date): array
    {
        $start = $date->modify('first day of this month midnight');
        $end = $date->modify('first day of next month midnight');

        return $this->filter($date, $start, $end);
    }

    public function filterYear(\DateTimeImmutable $date): array
    {
        $start = $date->modify('1st january midnight');
        $end = $date->modify('1st january next year midnight');

        return $this->filter($date, $start, $end);
    }

    public function push(array $booking)
    {
        $this->history[] = $booking;
    }

    /**
     * Filter booking history, returning past bookings intersecting with the
     * given start and end markers.
     *
     * @param \DateTimeImmutable $date
     * @param \DateTimeImmutable $start
     * @param \DateTimeImmutable $end
     * @return array
     */
    public function filter(
        \DateTimeImmutable $date,
        \DateTimeImmutable $start,
        \DateTimeImmutable $end
    ): array {
        return array_filter(
            $this->history,
            function($booking) use($date, $start, $end) {
                return $date > $booking['start']
                    && (($booking['start'] >= $start && $booking['start'] < $end)
                    || ($booking['end'] > $start && $booking['end'] < $end));
            }
        );
    }
}
