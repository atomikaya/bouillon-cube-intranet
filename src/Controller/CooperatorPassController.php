<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\CooperatorPass;
use App\Entity\Member;
use App\Repository\BookingBillRepository;
use App\Repository\BookingRepository;
use App\Repository\CooperatorPassRepository;
use App\Service\BillingCalculator;
use App\Service\BillingAccess;
use App\Service\Mailer;
use App\Service\Notifier;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/abonnement")
 */
class CooperatorPassController extends AbstractController
{
    /**
     * @Route("/", name="cooperator_pass_index", methods={"GET"})
     */
    public function index(CooperatorPassRepository $repo): Response
    {
        return $this->render('cooperator_pass/index.html.twig', [
            'passes' => $repo->findAllWaitingWithOwner(),
        ]);
    }

    /**
     * @Route("/{id}", name="cooperator_pass_ask", methods={"POST"})
     */
    public function ask(
        Member $member,
        EntityManagerInterface $manager,
        Mailer $mailer,
        Notifier $notifier
    ): Response {
        $pass = (new CooperatorPass())
            ->startNow()
            ->setIsWaiting(true)
            ->setOwner($member)
        ;

        $manager->persist($pass);
        $manager->flush();
        $mailer->sendPassRequested($member);
        $notifier->addSuccess('pass.requested');

        return $this->redirectToRoute('member_me');
    }

    /**
     * @Route("/{id}", name="cooperator_pass_reject", methods={"DELETE"})
     */
    public function reject(
        CooperatorPass $pass,
        EntityManagerInterface $manager,
        Mailer $mailer,
        Notifier $notifier
    ): JsonResponse {
        $pass->setIsWaiting(false);
        
        if ($pass->getIsActive()) {
            $mailer->sendRenewalDenied($pass->getOwner());
            $notifier->addDanger('pass.renewal_denied');
        } else {
            $mailer->sendPassDenied($pass->getOwner());
            $notifier->addWarning('pass.denied');
            $manager->remove($pass);
        }

        $manager->flush();
        
        // return $this->redirectToRoute('cooperator_pass_index');

        return $this->json(['messages' => $notifier->getAll()]);
    }

    /**
     * @Route("/{id}", name="cooperator_pass_activate", methods={"PUT"})
     */
    public function activate(
        CooperatorPass $pass,
        BookingRepository $repo,
        BillingCalculator $calculator,
        EntityManagerInterface $manager,
        Mailer $mailer,
        Notifier $notifier
    ): JsonResponse {
        $pass
            ->startNow()
            ->setIsActive(true)
            ->setIsWaiting(false)
        ;

        $bill = $calculator->estimatePass($pass);
        
        $manager->persist($bill);
        $manager->flush();

        $bookings = $repo->findFutureByApplicant(
            $pass->getOwner(),
            $pass->getStart()
        );

        /** @var Booking $booking */
        foreach ($bookings as $booking) {
            if ($calculator->hasRate($booking)) {
                $oldBills = $booking->getCosts();

                foreach ($oldBills as $bill) {
                    $manager->remove($bill);
                }

                $bills = $calculator->estimate($booking);

                foreach ($bills as $bill) {
                    $manager->persist($bill);
                }
            } else {
                $manager->remove($booking);
            }
        }

        $manager->flush();
        $mailer->sendPassApproved($pass->getOwner(), $pass);
        $notifier->addSuccess('pass.approved');

        // return $this->redirectToRoute('cooperator_pass_index');

        return $this->json(['messages' => $notifier->getAll()]);
    }

    /**
     * @Route("/{id}/renouvellement", name="cooperator_pass_ask_renewal", methods={"POST"})
     */
    public function askRenewal(
        CooperatorPass $pass,
        EntityManagerInterface $manager,
        Mailer $mailer,
        Notifier $notifier
    ): Response {
        $pass->setIsWaiting(true);
        $manager->flush();
        $mailer->sendRenewalRequested($pass->getOwner());
        $notifier->addSuccess('pass.renewal_requested');

        return $this->redirectToRoute('member_me');
    }

    /**
     * @Route("/{id}/renouvellement", name="cooperator_pass_renew", methods={"PUT"})
     */
    public function renew(
        CooperatorPass $pass,
        BillingCalculator $calculator,
        EntityManagerInterface $manager,
        Mailer $mailer,
        Notifier $notifier
    ): JsonResponse {
        $pass
            ->renew()
            ->setIsWaiting(false)
        ;

        $bill = $calculator->estimatePass($pass, true);

        $manager->persist($bill);
        $manager->flush();
        $mailer->sendRenewalApproved($pass->getOwner(), $pass);
        $notifier->addSuccess('pass.renewal_approved');

        // return $this->redirectToRoute('cooperator_pass_index');

        return $this->json(['messages' => $notifier->getAll()]);
    }

    /**
     * For now stop bypasses full month laws, it stops when you tell it to stop.
     * 
     * @Route("/{id}/{pass_id}", name="cooperator_pass_stop", methods={"DELETE"})
     * @ParamConverter("pass", options={"id" = "pass_id"})
     */
    public function stop(
        Member $member,
        CooperatorPass $pass,
        BookingRepository $bookingRepo,
        BookingBillRepository $billRepo,
        BillingCalculator $calculator,
        EntityManagerInterface $manager,
        BillingAccess $access,
        Mailer $mailer,
        Notifier $notifier
    ): Response {
        $pass->archive();

        $bills = $billRepo->findFutureForPassByReceiver($member);

        foreach ($bills as $bill) {
            $manager->remove($bill);
        }

        $manager->flush();

        // adjust booking prices or remove them when they're not allowed anymore
        $bookings = $bookingRepo->findFutureByApplicant(
            $pass->getOwner(),
            $pass->getEnd()
        );

        foreach ($bookings as $booking) {
            if ($calculator->hasRate($booking)) {
                $bills = $calculator->estimate($booking);

                foreach ($bills as $bill) {
                    $manager->persist($bill);
                }
            } else {
                $manager->remove($booking);
            }
        }

        $manager->flush();
        $mailer->sendPassInterrupted($member, $access->grantLevel($member));
        $notifier->addDanger('pass.interrupted');

        return $this->redirectToRoute('member_show', ['id' => $member->getId()]);
    }
}
