<?php

namespace App\Controller;

use App\Entity\BookingBill;
use App\Repository\BookingBillRepository;
use App\Service\Notifier;
use App\Util\MonthPaginator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reservation/facturation")
 */
class BookingBillController extends AbstractController
{
    /**
     * @Route("/", name="booking_bill", methods={"GET"})
     */
    public function index(Request $request, BookingBillRepository $repo): Response
    {
        // monthly pagination
        $paginator = new MonthPaginator(
            (int) $request->query->get('page', 1),
            $repo->findMonthsDue()
        );

        $page = $paginator->getCurrent();

        $bills = $page
            ? $repo->findMonthlyGroupedByReceiver($page->getUniqueMonth())
            : []
        ;

        return $this->render('booking/bill/index.html.twig', [
            'bills' => $bills,
            'page' => $paginator,
        ]);
    }

    /**
     * @Route("/{ids}", name="booking_bill_settle", methods={"PUT"})
     */
    public function settle(
        string $ids,
        BookingBillRepository $repo,
        EntityManagerInterface $manager,
        Notifier $notifier
    ): Response {
        $bills = $repo->findBy(['id' => explode(',', $ids)]);

        if (count($bills) > 0) {
            foreach ($bills as $bill) {
                $bill->setIsSettled(true);
            }
    
            $manager->flush();
            $notifier->addSuccess('booking_bill.settled');
        } else {
            $notifier->addDanger('booking_bill.nothing_to_settle');
        }

        return $this->redirectToRoute('booking_bill');
    }
}
