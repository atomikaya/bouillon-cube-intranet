<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Room;
use App\Entity\Member;
use App\Form\BookingType;
use App\Form\CalendarBookingType;
use App\Repository\BookingRepository;
use App\Repository\RoomRepository;
use App\Service\BillingCalculator;
use App\Service\IcalGenerator;
use App\Service\BillingAccess;
use App\Service\Mailer;
use App\Service\Notifier;
use App\Util\MonthPaginator;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/reservation")
 */
class BookingController extends AbstractController
{
    /**
     * @Route("/", name="booking")
     */
    public function index(
        BillingAccess $access,
        RoomRepository $roomRepo
    ): Response {
        /** @var Member $me */
        $me = $this->getUser()->getOwner();
        $booking = (new Booking())
            ->setApplicant($me)
            ->setStart(new \DateTimeImmutable('today +1 weekday +9 hours'))
            ->setEnd(new \DateTimeImmutable('today +1 weekday +18 hours'))
        ;
        $rooms = [];
        $level = Member::LEVEL_COOPERATOR;
        $action = '';

        if ($this->isGranted('ROLE_ADMIN')) {
            $rooms = $roomRepo->findAllActive();
            $action = $this->generateUrl('booking_block');
        } else {
            $level = $access->grantLevel($me);
            $rooms = $roomRepo->findAllForAccessLevel($level);
            $action = $this->generateUrl('booking_new', [
                'du_calendrier' => true,
            ]);
        }

        $form = $this->createForm(CalendarBookingType::class, $booking, [
            'access_level' => $level,
            'action' => $action,
            'method' => 'POST',
        ]);

        return $this->renderForm('booking/index.html.twig', [
            'rooms' => $rooms,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/nouvelle", name="booking_new", methods={"GET", "POST"})
     */
    public function new(
        Request $request,
        BillingAccess $access,
        ValidatorInterface $validator,
        EntityManagerInterface $manager,
        BillingCalculator $calculator,
        Mailer $mailer,
        Notifier $notifier,
        RoomRepository $roomRepo
    ): Response {
        /** @var Member $me */
        $me = $this->getUser()->getOwner();
        $booking = (new Booking())
            ->setApplicant($me)
            ->setStart(new \DateTimeImmutable('today +1 weekday +9 hours'))
            ->setEnd(new \DateTimeImmutable('today +1 weekday +18 hours'))
        ;
        $accessLevel = $access->grantLevel($me);
        $errors = [];

        if ($request->query->get('du_calendrier')) {
            $calForm = $this->createForm(CalendarBookingType::class, $booking, [
                'access_level' => $accessLevel,
            ]);

            $calForm->handleRequest($request);

            if ($booking->isFullDays()) {
                $booking->setUnit($booking->isFullMonths() ? 'month' : 'day');
            }
            
            $form = $this->createForm(BookingType::class, $booking, [
                'access_level' => $accessLevel,
                'action' => $this->generateUrl('booking_new'),
            ]);
        } else {
            $form = $this->createForm(BookingType::class, $booking, [
                'access_level' => $accessLevel,
            ]);
            
            $form->handleRequest($request);
        }
        
        // validate by hand to use only unit group
        $errors = $validator->validate($booking, null, [
            'Default',
            $booking->getUnit(),
        ]);

        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            if ($error->getPropertyPath() == '') {
                $form->addError(new FormError($error->getMessage()));
            } else {
                $form
                    ->get($error->getPropertyPath())
                    ->addError(new FormError($error->getMessage()))
                ;
            }
        }

        if ($form->isSubmitted() && count($errors) == 0) {
            $manager->persist($booking);

            $bills = $calculator->estimate($booking);

            foreach ($bills as $bill) {
                $manager->persist($bill);
            }

            // deal with weekly repetition

            if (
                $booking->getUnit() == Booking::UNIT_HOUR &&
                $form->get('repeat_weekly')->getData() &&
                $form->get('repeat_weekly_until')->getData()
            ) {
                $until = $form->get('repeat_weekly_until')->getData();

                for (
                    $i = 1;
                    $booking->getStart()->modify("+$i week") < $until;
                    $i++
                ) { 
                    $b = (clone $booking)
                        ->setStart($booking->getStart()->modify("+$i week"))
                        ->setEnd($booking->getEnd()->modify("+$i week"))
                    ;

                    $manager->persist($b);

                    // possibly unnecessary, recalculated with updateAfter

                    foreach ($calculator->estimate($b) as $bill) {
                        $manager->persist($bill);
                    }
                }
            }

            //

            $manager->flush();
            $mailer->sendBookingRequested($me, $booking);
            $calculator->updateAfter($booking);

            if (!$form->get('repeat')->getData()) {
                $notifier->addSuccess('booking.requested');

                return $this->redirectToRoute('booking_index_applicant', [
                    'id' => $me->getId(),
                ]);
            } else {
                $notifier->addSuccess('booking.requested_repeat');
                $booking->setForward();

                $form = $this->createForm(BookingType::class, $booking, [
                    'access_level' => $accessLevel,
                ]);
            }
        }

        return $this->renderForm('booking/new.html.twig', [
            'form' => $form,
            'access_level' => $accessLevel,
            'rooms' => $roomRepo->findAllForAccessLevel($accessLevel),
        ]);
    }

    /**
     * @Route("/membre/{id}", name="booking_index_applicant", methods={"GET"})
     */
    public function indexApplicant(
        Member $member,
        Request $request,
        BookingRepository $repo
    ): Response {
        $months = $repo->findMonthsByApplicant($member);

        if (!$request->query->has('page')) {
            $month = $request->query->get(
                'month',
                (new \DateTimeImmutable())->format('Y-n')
            );
            $results = array_filter($months, function($v) use ($month) {
                return $month === $v['uniqueMonth'];
            });
            $page = count($results) > 0 ? array_keys($results)[0] + 1 : 1;
        } else {
            $page = (int) $request->query->get('page', 1);
        }

        // pagination
        $paginator = new MonthPaginator($page, $months);
        
        if ($paginator->getCurrent()) {
            // retrieve bookings for nth month sorted by date asc
            $bookings = $repo->findMonthlyByApplicant(
                $member,
                $paginator->getCurrent()->getUniqueMonth()
            );
        } else {
            $bookings = [];
        }

        // display bookings and prev and next month names and counts
        return $this->render('booking/applicant.html.twig', [
            'bookings' => $bookings,
            'member' => $member,
            'page' => $paginator,
        ]);
    }

    /**
     * @Route("/attente", name="booking_index_waiting", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexWaiting(
        Request $request,
        BookingRepository $repo
    ): Response {
        // pagination
        $page = (int) $request->query->get('page', 1);
        $total = $repo->count(['isApproved' => false]);
        $paginator = [];

        if ($page > 1) {
            $paginator['previous'] = $page - 1;
        }

        if ($page * $repo::PAGE_SIZE < $total) {
            $paginator['next'] = $page + 1;
        }

        return $this->render('booking/waiting.html.twig', [
            'bookings' => $repo->findPagedWaiting($page),
            'page' => $paginator,
        ]);
    }

    /**
     * @Route("/attente/{id}", name="booking_allow", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function allow(
        Booking $booking,
        Request $request,
        EntityManagerInterface $manager,
        Mailer $mailer,
        Notifier $notifier
    ): JsonResponse {
        $booking->setIsApproved(true);
        $manager->flush();
        $mailer->sendBookingApproved($booking->getApplicant(), $booking);
        $notifier->addSuccess('booking.approved');

        // return $this->redirect(
        //     $request->headers->get('referer', $this->generateUrl('booking'))
        // );

        return $this->json(['messages' => $notifier->getAll()]);
    }

    /**
     * @Route("/attente/{id}", name="booking_deny", methods={"DELETE"})
     */
    public function deny(
        Booking $booking,
        Request $request,
        BillingCalculator $calculator,
        EntityManagerInterface $manager,
        Mailer $mailer,
        Notifier $notifier
    ): Response {

        if ($booking->getIsApproved()) {
            $mailer->sendBookingCancelled($booking->getApplicant(), $booking);
            $notifier->addDanger('booking.cancelled');
        } else {
            $mailer->sendBookingDenied($booking->getApplicant(), $booking);
            $notifier->addWarning('booking.denied');
        }
        
        // clean up denied booking
        $manager->remove($booking);
        $manager->flush();
        $calculator->updateAfter($booking);

        // return $this->redirect(
        //     $request->headers->get('referer', $this->generateUrl('booking'))
        // );
        
        return $this->json(['messages' => $notifier->getAll()]);
    }

    /**
     * @Route("/info/{id}/{start}/{end}/{unit}", name="booking_info", methods={"GET"})
     * @ParamConverter("start", options={"format": "!Y-m-d-H-i"})
     * @ParamConverter("end", options={"format": "!Y-m-d-H-i"})
     */
    public function sendInfo(
        Room $room,
        \DateTime $start,
        \DateTime $end,
        string $unit,
        ValidatorInterface $validator,
        BookingRepository $repo,
        BillingCalculator $calculator
    ): JsonResponse {
        $booking = (new Booking())
            ->setPlace($room)
            ->setApplicant($this->getUser()->getOwner())
            ->setStart(\DateTimeImmutable::createFromMutable($start))
            ->setEnd(\DateTimeImmutable::createFromMutable($end))
            ->setUnit($unit)
        ;

        $errors = $validator->validate($booking, null, ['Default', $unit]);

        if (count($errors) > 0) {
            return $this->json(['errors' => $errors]);
        }

        $bills = $calculator->estimate($booking);

        return $this->json([
            'concurrents' => $repo->findConcurrents($room, $start, $end),
            'price' => $calculator->sumPrices($bills),
        ]);
    }

    /**
     * @Route("/export/{id}", name="booking_export", methods={"GET"})
     */
    public function export(
        Room $room,
        Request $request,
        IcalGenerator $generator
    ): Response {
        $filename = 'calendrier_' . $room->getId() . '.ics';

        $response =  new Response($generator->generateIcs($room), 200, [
            'Content-Type' => 'text/calendar; charset=utf-8',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
        ]);

        $response->headers->set(
            AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER,
            true
        );
        $response->setCache([
            'must_revalidate' => true,
            'public' => true,
            'no_cache' => true,
        ]);

        return $response;
    }

    /**
     * @Route("/blocage", name="booking_block", methods={"POST"})
     */
    public function block(
        Request $request,
        EntityManagerInterface $manager
    ): Response {
        /** @var Member $me */
        $me = $this->getUser()->getOwner();
        $booking = (new Booking())
            ->setApplicant($me)
            ->setIsApproved(true)
        ;
        $calForm = $this->createForm(CalendarBookingType::class, $booking, [
            'access_level' => Member::LEVEL_COOPERATOR,
        ]);

        $calForm->handleRequest($request);

        if ($booking->isFullDays()) {
            $booking->setUnit($booking->isFullMonths() ? 'month' : 'day');
        }

        $manager->persist($booking);
        $manager->flush();

        return $this->redirect(
            $request->headers->get('referer', $this->generateUrl('booking'))
        );
    }
}
