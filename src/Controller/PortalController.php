<?php

namespace App\Controller;

use App\Entity\Member;
use App\Service\Mailer;
use App\Service\Notifier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PortalController extends AbstractController
{
    /**
     * @Route("/", name="portal")
     */
    public function index(): Response
    {
        return $this->render('portal/index.html.twig');
    }
    
    /**
     * @Route("/rapport-de-bug/{id}", name="portal_report", methods={"POST"})
     */
    public function report(
        Member $member,
        Request $request,
        Mailer $mailer,
        Notifier $notifier
    ): Response
    {
        /** @var array $report */
        $report = $request->get('bug_report', []);
        $route = $request->query->get('fromRoute');

        $mailer->sendBugReport($member, $report);
        $notifier->addSuccess('bug_report.sent');

        return $this->redirectToRoute($route);
    }

    /**
     * @Route("/mailer-test", name="portal_mailer_test", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function testMailer(Mailer $mailer): Response
    {
        $mailer->sendDnsTest();

        return $this->redirectToRoute('portal');
    }
}
