<?php

namespace App\Controller;

use App\Entity\Member;
use App\Entity\ResetLink;
use App\Entity\User;
use App\Form\UserPasswordResetType;
use App\Repository\ResetLinkRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use App\Service\Notifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('security/login.html.twig', [
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'lastUsername' => $authenticationUtils->getLastUsername(),
        ]);
    }

    /**
     * @Route("/deconnexion", name="logout")
     */
    public function logout(): void
    {}

    /**
     * @Route("/mot-de-passe", name="password_ask", methods={"GET", "POST"})
     */
    public function askPassword(
        Request $request,
        UserRepository $userRepo,
        ResetLinkRepository $linkRepo,
        EntityManagerInterface $manager,
        Mailer $mailer,
        Notifier $notifier
    ): Response {
        $email = $request->get('_username');
        
        if($email) {
            $user = $userRepo->findOneBy(['email' => $email]);

            if ($user) {
                $oldLink = $linkRepo->findOneBy(['target' => $user]);

                if ($oldLink) {
                    $manager->remove($oldLink);
                    $manager->flush();
                }

                $token = md5(random_bytes(10));
                $link = (new ResetLink())
                    ->setToken($token)
                    ->setTarget($user)
                    ->setExpiration(new \DateTimeImmutable('+30 minutes'))
                ;

                $manager->persist($link);
                $manager->flush();
                $mailer->sendPasswordResetLink($user->getOwner(), $link);
                $notifier->addSuccess('reset_link.sent');

                return $this->redirectToRoute('login');
            }

            $notifier->addDanger('reset_link.email_not_found');
        }

        return $this->render('security/ask_password.html.twig', []);
    }

    /**
     * @Route("/mot-de-passe/{token}", name="password_reset", methods={"GET", "POST"})
     */
    public function resetPassword(
        string $token,
        Request $request,
        ResetLinkRepository $repo,
        EntityManagerInterface $manager,
        UserPasswordHasherInterface $hasher,
        Notifier $notifier
    ): Response {
        $link = $repo->findOneBy(['token' => $token]);
        $now = new \DateTimeImmutable();

        if (is_null($link)) {
            $notifier->addDanger('reset_link.not_found');

            return $this->redirectToRoute('login');
        } elseif ($link->getExpiration() < $now) {
            $manager->remove($link);
            $manager->flush();
            $notifier->addDanger('reset_link.expired');

            return $this->redirectToRoute('login');
        }

        $form = $this->createForm(UserPasswordResetType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('password')->getData();
            $confirmation = $form->get('password_confirm')->getData();

            if ($password == $confirmation) {
                $user = $link->getTarget();

                $user->setPassword($hasher->hashPassword($user, $password));
                $manager->remove($link);
                $manager->flush();
                $notifier->addSuccess('user.password_updated');
    
                return $this->redirectToRoute('login');
            }

            $notifier->addDanger('user.password_confirm_wrong');
        }

        return $this->renderForm('security/reset_password.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/admin/mot-de-passe/{id}', name: 'password_update_unverified', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function updatePasswordUnverified(
        Member $member,
        Request $request,
        EntityManagerInterface $manager,
        UserPasswordHasherInterface $hasher,
        Notifier $notifier
    ): Response {

        $form = $this->createForm(UserPasswordResetType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('password')->getData();
            $confirmation = $form->get('password_confirm')->getData();

            if ($password == $confirmation) {

                $user = $member->getAccess();
                $user->setPassword($hasher->hashPassword($user, $password));
                $manager->flush();
                $notifier->addSuccess('user.password_updated');
    
                return $this->redirectToRoute('member_show', [
                    'id' => $member->getId()
                ]);
            }

            $notifier->addDanger('user.password_confirm_wrong');
        }

        return $this->renderForm('security/reset_password.html.twig', [
            'form' => $form,
        ]);

    }
}
