<?php

namespace App\Controller;

use App\Entity\CooperatorPass;
use App\Entity\Member;
use App\Entity\User;
use App\Form\MemberInfoType;
use App\Form\MemberReactivationType;
use App\Form\UserPasswordType;
use App\Repository\BookingRepository;
use App\Repository\CooperatorPassRepository;
use App\Repository\MemberRepository;
use App\Repository\ResetLinkRepository;
use App\Service\Mailer;
use App\Service\Notifier;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/membre")
 */
class MemberController extends AbstractController
{
    /**
     * @Route("/", name="member_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(Request $request, MemberRepository $repo): Response
    {
        // pagination
        $page = (int) $request->query->get('page', 1);
        $total = $repo->count(['isWaiting' => false]);
        $paginator = [];

        if ($page > 1) {
            $paginator['previous'] = $page - 1;
        }

        if ($page * $repo::PAGE_SIZE < $total) {
            $paginator['next'] = $page + 1;
        }

        return $this->render('member/index.html.twig', [
            'members' => $repo->findAllPagedWithFuturePass($page),
            'page' => $paginator,
        ]);
    }

    /**
     * @Route("/moi", name="member_me")
     */
    public function edit(
        Request $request,
        MemberRepository $repo,
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $hasher,
        Notifier $notifier
    ): Response {
        /** @var Member $me */
        $me = $this->getUser()->getOwner();
        $infoForm = $this->createForm(MemberInfoType::class, $me);
        $passwordForm = $this->createForm(UserPasswordType::class);

        $infoForm->handleRequest($request);
        $passwordForm->handleRequest($request);

        if ($infoForm->isSubmitted() && $infoForm->isValid()) {
            $entityManager->flush();
            $notifier->addSuccess('member.info_updated');
        }

        if ($passwordForm->isSubmitted() && $passwordForm->isValid()) {
            $user = $me->getAccess();
            $user->setPassword(
                $hasher->hashPassword(
                    $user,
                    $passwordForm->get('new_password')->getData()
                )
            );
            $entityManager->flush();
            $notifier->addSuccess('user.password_updated');
        }

        return $this->render('member/edit.html.twig', [
            'member' => $repo->findOneWithFuturePass($me),
            'info_form' => $infoForm->createView(),
            'password_form' => $passwordForm->createView(),
        ]);
    }
    
    /**
     * @Route("/desactive-es", name="member_index_disabled", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexDisabled(
        Request $request,
        MemberRepository $repo
    ): Response {
        // pagination
        $page = (int) $request->query->get('page', 1);
        $total = $repo->count(['isWaiting' => false, 'isActive' => false]);
        $paginator = [];

        if ($page > 1) {
            $paginator['previous'] = $page - 1;
        }

        if ($page * $repo::PAGE_SIZE < $total) {
            $paginator['next'] = $page + 1;
        }

        return $this->render('member/index_disabled.html.twig', [
            'members' => $repo->findDisabledPaged($page),
            'page' => $paginator,
        ]);
    }

    /**
     * @Route("/{id}", name="member_new", methods={"POST"})
     */
    public function new(
        Member $member,
        Request $request,
        Mailer $mailer,
        UserPasswordHasherInterface $hasher,
        EntityManagerInterface $manager,
        Notifier $notifier
    ): Response {
        $plainPassword = md5(random_bytes(10));
        $user = (new User())
            ->setEmail($member->getEmail())
            ->setOwner($member)
        ;

        $user->setPassword($hasher->hashPassword($user, $plainPassword));
        $member->setIsWaiting(false)->setIsActive(true);
        $manager->persist($user);

        /** @var bool */
        $reactivation = (bool) $request->query->get('reactivation', false);
        /** @var array */
        $data = $request->get($reactivation ? 'member_reactivation' : 'member_rights');

        if (
            key_exists('isCooperator', $data) &&
            key_exists('passStart', $data) &&
            key_exists('passEnd', $data)
        ) {
            $pass = (new CooperatorPass())
                ->setStart(new \DateTimeImmutable($data['passStart']))
                ->setEnd(new \DateTimeImmutable($data['passEnd']))
                ->setIsActive(true)
                ->setOwner($member)
            ;

            $manager->persist($pass);
            $notifier->addSuccess('pass.created');
        }

        $manager->flush();
        $mailer->sendRegistrationApproved($member, $plainPassword);
        $notifier->addSuccess('member.approved');

        return $this->redirectToRoute('member_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{id}", name="member_show", methods={"GET"})
     */
    public function show(Member $member, MemberRepository $repo): Response
    {
        return $this->render('member/show.html.twig', [
            'member' => $repo->findOneWithFuturePass($member),
        ]);
    }

    /**
     * @Route("/{id}", name="member_disable", methods={"DELETE"})
     */
    public function disable(
        Member $member,
        Request $request,
        BookingRepository $bookingRepo,
        CooperatorPassRepository $passRepo,
        ResetLinkRepository $linkRepo,
        Mailer $mailer,
        EntityManagerInterface $manager,
        Notifier $notifier
    ): Response {
        if ($this->isCsrfTokenValid(
            'delete' . $member->getId(),
            $request->get('_token')
        )) {
            $isMe = $member == $this->getUser()->getOwner();

            $mailer->sendMemberDisabled($member);
            $member->scrapInfo()->setIsActive(false);

            $bookings = $bookingRepo->findFutureByApplicant(
                $member,
                new \DateTimeImmutable()
            );

            foreach ($bookings as $booking) {
                $manager->remove($booking);
            }

            $link = $linkRepo->findOneBy(['target' => $member->getAccess()->getId()]);

            if ($link) {
                $manager->remove($link);
            }

            $manager->flush();
            
            $pass = $passRepo->findActiveByOwner($member);

            if ($pass) {
                $this->forward(CooperatorPassController::class.'::stop', [
                    'member' => $member,
                    'pass' => $pass,
                ]);
            }
            
            if ($isMe) {
                // trick to delete own user
                $session = (new Session())->invalidate();
                $manager->remove($member->getAccess());
                $manager->flush();
                $notifier->addDanger('member.disabled_me');

                return $this->redirectToRoute('logout');
            }

            $manager->remove($member->getAccess());
            $manager->flush();
            $notifier->addDanger('member.disabled');
            
            return $this->redirectToRoute('member_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->redirectToRoute('member_me');
    }

    /**
     * @Route(
     *   "/desactive-es/{id}",
     *   name="member_show_disabled",
     *   methods={"GET", "POST"}
     * )
     */
    public function show_disabled(
        Member $member,
        Request $request,
        EntityManagerInterface $manager
    ): Response {
        $form = $this->createForm(MemberReactivationType::class, $member);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();

            return $this->redirectToRoute('member_new', [
                'id' => $member->getId(),
                'reactivation' => true,
            ], 307);
        }

        return $this->renderForm('member/show_disabled.html.twig', [
            'member' => $member,
            'form' => $form,
        ]);
    }
}
