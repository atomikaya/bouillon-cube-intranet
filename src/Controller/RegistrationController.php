<?php

namespace App\Controller;

use App\Entity\Member;
use App\Form\MemberRegistrationType;
use App\Form\MemberRightsType;
use App\Repository\MemberRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use App\Service\Notifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/inscription")
 */
class RegistrationController extends AbstractController
{
    /**
     * @Route("/", name="member_registration", methods={"GET"})
     */
    public function index(
        MemberRepository $repo
    ): Response {
        return $this->render('member/registration/index.html.twig', [
            'members' => $repo->findAllRegistrations(),
        ]);
    }

    /**
     * @Route("/nouvelle", name="member_registration_new", methods={"GET","POST"})
     */
    public function new(
        Request $request,
        Mailer $mailer,
        UserRepository $userRepository,
        MemberRepository $memberRepository,
        EntityManagerInterface $manager,
        Notifier $notifier
    ): Response {
        $member = new Member();
        $form = $this->createForm(MemberRegistrationType::class, $member);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $criteria = ['email' => $member->getEmail()];

            if (
                $userRepository->findOneBy($criteria)
                || $memberRepository->findOneBy($criteria)
            ) {
                $notifier->addDanger('member.email_duplicate');
            } else {
                $member->setIsWaiting(true);
                $manager->persist($member);
                $manager->flush();
                $mailer->sendRegistrationCreated($member);
                $notifier->addSuccess('member.requested');
    
                return $this->redirectToRoute('login');
            }

        }

        return $this->renderForm('member/registration/new.html.twig', [
            'member' => $member,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="member_registration_show", methods={"GET", "POST"})
     */
    public function show(
        Member $member,
        Request $request,
        EntityManagerInterface $manager
    ): Response {
        $form = $this->createForm(MemberRightsType::class, $member);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();

            return $this->redirectToRoute('member_new', [
                'id' => $member->getId(),
            ], 307);
        }

        return $this->renderForm('member/registration/show.html.twig', [
            'member' => $member,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="member_registration_delete", methods={"DELETE"})
     */
    public function delete(
        Request $request,
        Member $member,
        Mailer $mailer,
        EntityManagerInterface $manager,
        Notifier $notifier
    ): Response {
        if ($this->isCsrfTokenValid(
            'delete' . $member->getId(),
            $request->get('_token')
        )) {
            $mailer->sendRegistrationDenied($member);
            $manager->remove($member);
            $manager->flush();
            $notifier->addWarning('member.denied');
        }

        return $this->redirectToRoute(
            'member_registration',
            [],
            Response::HTTP_SEE_OTHER
        );
    }
}
