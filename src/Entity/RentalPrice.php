<?php

namespace App\Entity;

use App\Repository\RentalPriceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RentalPriceRepository::class)
 */
class RentalPrice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hourlyRate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dailyRate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $monthlyRate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $overtimeRate;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxHours = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxDays = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hourSeeking = '1 month';

    /**
     * @ORM\Column(type="boolean")
     */
    private $followsPass = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHourlyRate(): ?int
    {
        return $this->hourlyRate;
    }

    public function setHourlyRate(?int $hourlyRate): self
    {
        $this->hourlyRate = $hourlyRate;

        return $this;
    }

    public function hasHourlyRate(): bool
    {
        return !is_null($this->hourlyRate);
    }

    public function getDailyRate(): ?int
    {
        return $this->dailyRate;
    }

    public function setDailyRate(?int $dailyRate): self
    {
        $this->dailyRate = $dailyRate;

        return $this;
    }

    public function hasDailyRate(): bool
    {
        return !is_null($this->dailyRate);
    }

    public function getMonthlyRate(): ?int
    {
        return $this->monthlyRate;
    }

    public function setMonthlyRate(?int $monthlyRate): self
    {
        $this->monthlyRate = $monthlyRate;

        return $this;
    }

    public function hasMonthlyRate(): bool
    {
        return !is_null($this->monthlyRate);
    }

    public function getOvertimeRate(): ?int
    {
        return $this->overtimeRate;
    }

    public function setOvertimeRate(?int $overtimeRate): self
    {
        $this->overtimeRate = $overtimeRate;

        return $this;
    }

    public function hasOvertimeRate(): bool
    {
        return !is_null($this->overtimeRate);
    }

    public function getMaxHours(): ?int
    {
        return $this->maxHours;
    }

    public function setMaxHours(int $maxHours): self
    {
        $this->maxHours = $maxHours;

        return $this;
    }

    public function hasMaxHours(): bool
    {
        return $this->maxHours > 0;
    }

    public function getMaxDays(): ?int
    {
        return $this->maxDays;
    }

    public function setMaxDays(int $maxDays): self
    {
        $this->maxDays = $maxDays;

        return $this;
    }

    public function hasMaxDays(): bool
    {
        return $this->maxDays > 0;
    }

    public function getHourSeeking(): string
    {
        return $this->hourSeeking;
    }

    public function setHourSeeking(string $hourSeeking): self
    {
        $this->hourSeeking = $hourSeeking;

        return $this;
    }

    public function getFollowsPass(): ?bool
    {
        return $this->followsPass;
    }

    public function setFollowsPass(bool $followsPass): self
    {
        $this->followsPass = $followsPass;

        return $this;
    }
}
