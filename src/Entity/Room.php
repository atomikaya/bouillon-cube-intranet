<?php

namespace App\Entity;

use App\Repository\RoomRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoomRepository::class)
 */
class Room
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=RentalPrice::class)
     */
    private $cooperatorRate;

    /**
     * @ORM\ManyToOne(targetEntity=RentalPrice::class)
     */
    private $neighborRate;

    /**
     * @ORM\ManyToOne(targetEntity=RentalPrice::class)
     */
    private $outsiderRate;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $color = '#ccc';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    public function getId(): ?int
    {
        return $this->id;
    }

    /* so I can update the rooms through fixtures alongside real user data */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCooperatorRate(): ?RentalPrice
    {
        return $this->cooperatorRate;
    }

    public function setCooperatorRate(?RentalPrice $cooperatorRate): self
    {
        $this->cooperatorRate = $cooperatorRate;

        return $this;
    }

    public function getNeighborRate(): ?RentalPrice
    {
        return $this->neighborRate;
    }

    public function setNeighborRate(?RentalPrice $neighborRate): self
    {
        $this->neighborRate = $neighborRate;

        return $this;
    }

    public function getOutsiderRate(): ?RentalPrice
    {
        return $this->outsiderRate;
    }

    public function setOutsiderRate(?RentalPrice $outsiderRate): self
    {
        $this->outsiderRate = $outsiderRate;

        return $this;
    }

    public function getRateForAccessLevel(string $level): ?RentalPrice
    {
        switch ($level) {
            case Member::LEVEL_COOPERATOR:
                return $this->getCooperatorRate();
            case Member::LEVEL_NEIGHBOR:
                return $this->getNeighborRate();
            default:
                return $this->getOutsiderRate();
        }
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
