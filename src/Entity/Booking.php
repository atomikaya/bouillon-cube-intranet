<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use App\Util\AccurateDiff;
use App\Validator as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 * @AppAssert\UniqueBooking
 */
class Booking
{
    const UNIT_HOUR = 'hour';

    const UNIT_DAY = 'day';

    const UNIT_MONTH = 'month';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime_immutable")
     * Assert\GreaterThanOrEqual(
     *   "now",
     *   groups={"hour"},
     *   message="booking.start.later"
     * )
     * @Assert\Expression(
     *   "this.isSameDay()",
     *   groups={"hour"},
     *   message="booking.same_day"
     * )
     * Assert\GreaterThanOrEqual(
     *   "tomorrow midnight",
     *   groups={"day"},
     *   message="booking.start.tomorrow"
     * )
     * @Assert\Expression(
     *   "this.isDayStart(value)",
     *   groups={"day", "month"},
     *   message="booking.start.full_day"
     * )
     * Assert\GreaterThanOrEqual(
     *   "first day of next month midnight",
     *   groups={"month"},
     *   message="booking.start.next_month"
     * )
     * @Assert\Expression(
     *   "this.isMonthStart(value)",
     *   groups={"month"},
     *   message="booking.start.full_month"
     * )
     * @Assert\LessThan(
     *   "+4 months",
     *   groups={"hour", "day", "month"},
     *   message="booking.start.within_4_months"
     * )
     */
    private $start;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Assert\Expression(
     *   "this.isSameDay()",
     *   groups={"hour"},
     *   message="booking.same_day"
     * )
     * @Assert\GreaterThan(
     *   propertyPath="start",
     *   message="booking.end.after_start"
     * )
     * @Assert\Expression(
     *   "this.isDayStart(value)",
     *   groups={"day", "month"},
     *   message="booking.end.full_day"
     * )
     * @Assert\Expression(
     *   "this.isMonthStart(value)",
     *   groups={"month"},
     *   message="booking.end.full_month"
     * )
     */
    private $end;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isApproved = false;

    /**
     * @ORM\ManyToOne(targetEntity=Room::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $place;

    /**
     * @ORM\ManyToOne(targetEntity=Member::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $applicant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $unit = self::UNIT_HOUR;

    /**
     * @ORM\OneToMany(
     *   targetEntity=BookingBill::class,
     *   mappedBy="source",
     *   orphanRemoval=true
     * )
     */
    private $costs;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    public function __construct()
    {
        $this->costs = new ArrayCollection();
        $this->test = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStart(): ?\DateTimeImmutable
    {
        return $this->start;
    }

    public function setStart(\DateTimeImmutable $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeImmutable
    {
        return $this->end;
    }

    public function getDisplayEnd(): ?\DateTimeImmutable
    {
        if ($this->unit == self::UNIT_HOUR) {
            return $this->end;
        }
        
        return $this->end->modify('-1 day');
    }

    public function setEnd(\DateTimeImmutable $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function isSameDay(): bool
    {
        return $this->start->format('Y-m-d') == $this->end->format('Y-m-d');
    }

    public function isDayStart(\DateTimeImmutable $date): bool
    {
        return $date->format('H:i') == '00:00';
    }

    public function isFullDays(): bool
    {
        return $this->isDayStart($this->start) && $this->isDayStart($this->end);
    }

    public function isMonthStart(\DateTimeImmutable $date):bool
    {
        return $date->format('d') == '1';
    }

    public function isFullMonths(): bool
    {
        return $this->isMonthStart($this->start) && $this->isMonthStart($this->end);
    }

    public function getDuration(): \DateInterval
    {
        return AccurateDiff::diff($this->start, $this->end);
    }

    public function getLength(): float
    {
        $duration = $this->getDuration();

        switch ($this->unit) {
            case self::UNIT_HOUR:
                return $duration->days * 24 + $duration->h + $duration->i / 60;
            case self::UNIT_DAY:
                return $duration->days;
            case self::UNIT_MONTH:
                return $duration->m;
            default:
                return 0;
        }
    }

    public function setForward(): self
    {
        switch ($this->unit) {
            case self::UNIT_HOUR:
                $this-> start = $this->start->modify('+1 day');
                $this-> end = $this->end->modify('+1 day');
                break;
            case self::UNIT_DAY:
                $days = AccurateDiff::diff($this->start, $this->end)->days;

                $this->start = clone $this->end;
                $this->end = $this->end->modify("+$days days");
                break;
            case self::UNIT_MONTH:
                $this->start = clone $this->end;
                $this->end = $this->end->modify('+1 month');
                break;
            default:
                break;
        }

        return $this;
    }

    public function getIsApproved(): ?bool
    {
        return $this->isApproved;
    }

    public function setIsApproved(bool $isApproved): self
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    public function getPlace(): ?Room
    {
        return $this->place;
    }

    public function setPlace(?Room $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getApplicant(): ?Member
    {
        return $this->applicant;
    }

    public function setApplicant(?Member $applicant): self
    {
        $this->applicant = $applicant;

        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    public function setCosts(Collection $costs): self
    {
        $this->costs = $costs;

        return $this;
    }

    /**
     * @return Collection|BookingBill[]
     */
    public function getCosts(): Collection
    {
        return $this->costs;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
