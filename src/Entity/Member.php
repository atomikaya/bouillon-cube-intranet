<?php

namespace App\Entity;

use App\Repository\MemberRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MemberRepository::class)
 * @ORM\Table(name="app_member")
 */
class Member implements MailContactInterface
{
    const LEVEL_COOPERATOR = 'cooperator';

    const LEVEL_NEIGHBOR = 'neighbor';

    const LEVEL_OUTSIDER = 'outsider';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="member.email.blank")
     * @Assert\Email(message="member.email.format")
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isNeighbor;

    /**
     * Meaning is registered
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isWaiting = false;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="owner")
     */
    private $access;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if (trim($name) !== '') {
            $this->name = $name;
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getIsNeighbor(): ?bool
    {
        return $this->isNeighbor;
    }

    public function setIsNeighbor(?bool $isNeighbor): self
    {
        $this->isNeighbor = $isNeighbor;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsWaiting(): ?bool
    {
        return $this->isWaiting;
    }

    public function setIsWaiting(bool $isWaiting): self
    {
        $this->isWaiting = $isWaiting;

        return $this;
    }

    public function getAccess(): ?User
    {
        return $this->access;
    }

    public function setAccess(?User $access): self
    {
        $this->access = $access;

        return $this;
    }

    public function scrapInfo(): self
    {
        $this->name = null;
        $this->address = null;
        $this->isNeighbor = null;
        
        return $this;
    }
}
