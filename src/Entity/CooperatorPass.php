<?php

namespace App\Entity;

use App\Repository\CooperatorPassRepository;
use App\Util\AccurateDiff;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CooperatorPassRepository::class)
 */
class CooperatorPass
{
    const PRICE = 3000;
    const DURATION = '6 months';
    const MODIFIER = 'first day of this month midnight';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private $start;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private $end;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isWaiting = false;

    /**
     * @ORM\ManyToOne(targetEntity=Member::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStart(): ?\DateTimeImmutable
    {
        return $this->start;
    }

    public function setStart(\DateTimeImmutable $start): self
    {
        // enforce full month
        $this->start = $start->modify(self::MODIFIER);

        return $this;
    }

    public function getEnd(): ?\DateTimeImmutable
    {
        return $this->end;
    }

    public function setEnd(\DateTimeImmutable $end): self
    {
        // enforce full month
        $this->end = $end->modify(self::MODIFIER);

        return $this;
    }

    public function getLastRenewal(): \DateTimeImmutable
    {
        $renewal = $this->start;

        $diff = AccurateDiff::diff($renewal, $this->end);
        $last = AccurateDiff::getMonths($diff) < 6;

        while (!$last) {
            $renewal = $renewal->modify('+' . self::DURATION);
            $diff = AccurateDiff::diff($renewal, $this->end);
            $last = AccurateDiff::getMonths($diff) < 6;
        }

        return $renewal;
    }

    public function setPeriod(\DateTimeImmutable $end): self
    {
        $this->start = $end->modify('-' . self::DURATION);
        $this->end = $end;

        return $this;
    }

    public function startNow(): self
    {
        $this->start = new \DateTimeImmutable(self::MODIFIER);
        $this->end = $this->start->modify('+' . self::DURATION);

        return $this;
    }

    public function renew(): self
    {
        $this->end = $this->end->modify('+' . self::DURATION);

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsWaiting(): ?bool
    {
        return $this->isWaiting;
    }

    public function setIsWaiting(bool $isWaiting): self
    {
        $this->isWaiting = $isWaiting;

        return $this;
    }

    public function getOwner(): ?Member
    {
        return $this->owner;
    }

    public function setOwner(?Member $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function archive(): self
    {
        $this->isActive = false;
        $this->isWaiting = false;
        $this->end = $this->end->modify('now');

        return $this;
    }
}
