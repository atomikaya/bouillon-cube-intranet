<?php

namespace App\Entity;

use App\Repository\BookingBillRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookingBillRepository::class)
 */
class BookingBill
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Booking::class, inversedBy="costs")
     */
    private $source;

    /**
     * @ORM\ManyToOne(targetEntity=Member::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $receiver;

    /**
     * @ORM\Column(type="integer")
     */
    private $price = 0;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $due;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSettled = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSource(): ?Booking
    {
        return $this->source;
    }

    public function setSource(?Booking $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function hasSource(): bool
    {
        return !is_null($this->source);
    }

    public function getReceiver(): ?Member
    {
        return $this->receiver;
    }

    public function setReceiver(?Member $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDue(): ?\DateTimeImmutable
    {
        return $this->due;
    }

    public function setDue(\DateTimeImmutable $due): self
    {
        $this->due = $due;

        return $this;
    }

    public function getIsSettled(): ?bool
    {
        return $this->isSettled;
    }

    public function setIsSettled(bool $isSettled): self
    {
        $this->isSettled = $isSettled;

        return $this;
    }
}
