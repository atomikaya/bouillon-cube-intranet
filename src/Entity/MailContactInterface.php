<?php

namespace App\Entity;

interface MailContactInterface
{
    public function getName(): ?string;

    public function getEmail(): ?string;
}
