<?php

namespace App\Command;

use App\Service\BillingCalculator;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:bill-reset',
    description: 'Re-estimate all bills based on bookings',
)]
class BillResetCommand extends Command
{
    private BillingCalculator $calculator;

    public function __construct(BillingCalculator $calculator)
    {
        $this->calculator = $calculator;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->calculator->resetAll();

        return Command::SUCCESS;
    }
}
