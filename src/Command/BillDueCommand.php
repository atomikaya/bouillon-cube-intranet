<?php

namespace App\Command;

use App\Entity\BookingBill;
use App\Repository\BookingBillRepository;
use App\Service\Mailer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BillDueCommand extends Command
{
    protected static $defaultName = 'app:bill-due';
    protected static $defaultDescription = 'Sends due bills to their receivers';

    private BookingBillRepository $repository;

    private Mailer $mailer;

    public function __construct(
        BookingBillRepository $repository,
        Mailer $mailer
    ) {
      $this->repository = $repository;
      $this->mailer = $mailer;

      parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $now = new \DateTimeImmutable();
        $bills = $this->repository->findMonthlyDue($now);
        $receivers = [];
        $report = [];

        /** @var BookingBill $bill */
        foreach ($bills as $bill) {
            $receivers[$bill->getReceiver()->getId()][] = $bill;
        }

        /** @var BookingBill[] $bills */
        foreach ($receivers as $bills) {
            $receiver = $bills[0]->getReceiver();
            $report[] = ['name' => $receiver->getName(), 'bills' => $bills];

            $this->mailer->sendBillingDue($receiver, $bills);
            $io->note(sprintf(
                'Receiver %s (%s) has been sent the following bills:',
                $receiver->getName(),
                $receiver->getEmail()
            ));

            $rows = [];

            foreach ($bills as $bill) {
                $rows[] = [$bill->getId(), $bill->getPrice() / 100 . ' €'];
            }

            $io->table(['id', 'price'], $rows);
        }

        $this->mailer->sendBillingDueReport($report);

        $io->success('The receivers have been informed of their due bills.');

        return Command::SUCCESS;
    }
}
