<?php

namespace App\Command;

use App\Entity\CooperatorPass;
use App\Repository\CooperatorPassRepository;
use App\Service\Mailer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PassExpiringCommand extends Command
{
    protected static $defaultName = 'app:pass-expiring';
    protected static $defaultDescription = 'Sends a reminder to owners of an expiring pass';

    private CooperatorPassRepository $repository;
    private Mailer $mailer;

    public function __construct(
        CooperatorPassRepository $repository,
        Mailer $mailer
    ) {
        $this->repository = $repository;
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        /** @var CooperatorPass[] $expiring */
        $expiring = $this->repository->findExpiringInOneMonth();

        foreach ($expiring as $pass) {
            $owner = $pass->getOwner();

            $io->note(sprintf(
                'Cooperator pass %d from owner %s expires in 1 month.',
                $pass->getId(),
                $owner->getName()
            ));
            $this->mailer->sendPassExpiring($owner, $pass);
        }

        $io->success('Pass owners successfully warned.');

        return Command::SUCCESS;
    }
}
