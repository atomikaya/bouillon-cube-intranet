<?php

namespace App\Command;

use App\Entity\RentalPrice;
use App\Entity\Room;
use App\Repository\RentalPriceRepository;
use App\Repository\RoomRepository;
use App\Service\ColorBalancer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:room-update',
    description: 'Updates info on rooms and rates without losing existing '
        . 'relations',
)]
class RoomUpdateCommand extends Command
{
    private ColorBalancer $balancer;

    private EntityManagerInterface $manager;
    
    private RentalPriceRepository $rates;
    
    private RoomRepository $rooms;

    public function __construct(
        ColorBalancer $balancer,
        EntityManagerInterface $manager,
        RentalPriceRepository $rates,
        RoomRepository $rooms
    ) {
        $this->balancer = $balancer;
        $this->manager = $manager;
        $this->rates = $rates;
        $this->rooms = $rooms;

        parent::__construct();
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $io = new SymfonyStyle($input, $output);

        $io->note('Recovering room and rates data…');

        $roomInfo = $this->getRoomInfo();
        $rooms = $this->rooms->findAll();
        $rateInfo = $this->getRateInfo();
        $rates = $this->rates->findAll();

        // remove all rates and rooms absent from info
        $io->note('Unlinking rates and removing deleted rooms…');

        foreach ($rooms as $room) {
            $room
                ->setCooperatorRate(null)
                ->setNeighborRate(null)
                ->setOutsiderRate(null)
                ->setIsActive(array_key_exists($room->getId(), $roomInfo))
                // hack for unique colors
                ->setColor(bin2hex(random_bytes(20)))
            ;
        }

        $this->manager->flush();

        $io->note('Dropping all rates…');
        // drop old rates
        foreach ($rates as $rate) {
            $this->manager->remove($rate);
        }

        $this->manager->flush();

        // persist new rates and ref them
        $io->note('Persisting new rates…');

        $rates = [];

        foreach ($rateInfo as $key => $info) {
            $rates[$key] = (new RentalPrice);

            foreach ($info as $prop => $value) {
                $rates[$key]->{"set$prop"}($value);
            }

            $this->manager->persist($rates[$key]);
        }

        // update rooms, create if necessary and add rates
        $io->note('Updating room info and linking new rates…');

        foreach ($roomInfo as $id => $info) {
            $room = ($this->rooms->find($id) ?? (new Room))
                ->setName($info['name'])
                ->setSlug($info['slug'])
                ->setColor($info['color'])
            ;

            if (array_key_exists('cooperatorRate', $info)) {
                $room->setCooperatorRate($rates[$info['cooperatorRate']]);
            }

            if (array_key_exists('neighborRate', $info)) {
                $room->setNeighborRate($rates[$info['neighborRate']]);
            }

            if (array_key_exists('outsiderRate', $info)) {
                $room->setOutsiderRate($rates[$info['outsiderRate']]);
            }
        }

        $this->manager->flush();
        $io->success('Room update done.');
       
        return Command::SUCCESS;
    }

    /* hash of room info indexed by ids */
    private function getRoomInfo(): array
    {
        $rooms = [
            // big teleconf -> residence with kiosk rates
            26 => [
                'id' => 26,
                'name' => 'Pièce de résidence',
                'slug' => 'residence',
                'cooperatorRate' => 'rate_cooperator_kiosk',
                'neighborRate' => 'rate_neighbor_other',
                'outsiderRate' => 'rate_neighbor_other',
            ],
            // teleconf
            27 => [
                'id' => 27,
                'name' => 'Petite visio',
                'slug' => 'teleconf',
                'cooperatorRate' => 'rate_cooperator_office_shared',
                'neighborRate' => 'rate_neighbor_office_shared',
                'outsiderRate' => 'rate_outsider_office_shared',
            ],
            // open-air theater
            28 => [
                'id' => 28,
                'name' => 'Théâtre de verdure',
                'slug' => 'theater',
                'cooperatorRate' => 'rate_cooperator_open_air_theater',
                'neighborRate' => 'rate_neighbor_other',
                'outsiderRate' => 'rate_outsider_other',
            ],
            // kiosk
            29 => [
                'id' => 29,
                'name' => 'Kiosque',
                'slug' => 'kiosk',
                'cooperatorRate' => 'rate_cooperator_kiosk',
                'neighborRate' => 'rate_neighbor_other',
                'outsiderRate' => 'rate_outsider_other',
            ],
            // kitchen
            30 => [
                'id' => 30,
                'name' => 'Cuisine',
                'slug' => 'kitchen',
                'cooperatorRate' => 'rate_cooperator_kitchen',
                'neighborRate' => 'rate_neighbor_other',
                'outsiderRate' => 'rate_outsider_other',
            ],
        ];

        // individual offices
        for ($i=1; $i < 4; $i++) {
            $rooms[15 + $i] = [
                'id' => 15 + $i,
                'name' => "Bureau $i",
                'slug' => 'office',
                'cooperatorRate' => "rate_cooperator_office_$i",
            ];
        }
        
        // shared office spaces
        for ($i=1; $i < 8; $i++) {
            $rooms[18 + $i] = [
                'id' => 18 + $i,
                'name' => "Bureau partagé $i",
                'slug' => 'office_shared',
                'cooperatorRate' => 'rate_cooperator_office_shared',
                'neighborRate' => 'rate_neighbor_office_shared',
                'outsiderRate' => 'rate_outsider_office_shared',
            ];
        }

        asort($rooms);

        // add colors and persist
        $wheel = $this->balancer->generateHSLWheel(count($rooms), true);

        foreach (array_values($rooms) as $i => $room) {
            $rooms[$room['id']]['color'] = $wheel[$i];
        }

        return $rooms;
    }

    private function getRateInfo(): array
    {
        return [
            // individual offices
            'rate_cooperator_office_1' => [
                'monthlyRate' => 23800,
            ],
            'rate_cooperator_office_2' => [
                'monthlyRate' => 14000,
            ],
            'rate_cooperator_office_3' => [
                'monthlyRate' => 49000,
            ],
            // shared office
            'rate_cooperator_office_shared' => [
                'hourlyRate' => 100,
                'dailyRate' => 500,
                'monthlyRate' => 6300,
            ],
            'rate_neighbor_office_shared' => [
                'hourlyRate' => 200,
                'dailyRate' => 800,
                'monthlyRate' => 10000,
            ],
            'rate_outsider_office_shared' => [
                'hourlyRate' => 300,
                'dailyRate' => 1000,
                'monthlyRate' => 12000,
            ],
            // open-air theater
            'rate_cooperator_open_air_theater' => [
                'hourlyRate' => 0,
                'dailyRate' => 0,
                'maxDays' => 10,
                'overtimeRate' => 500,
            ],
            // kiosk
            'rate_cooperator_kiosk' => [
                'hourlyRate' => 0,
                'dailyRate' => 0,
                'maxHours' => 60,
                'hourSeeking' => '6 months',
                'overtimeRate' => 100,
            ],
            // kitchen
            'rate_cooperator_kitchen' => [
                'hourlyRate' => 100,
                'dailyRate' => 1000,
            ],
            // other
            'rate_cooperator_other' => [
                'hourlyRate' => 0,
                'dailyRate' => 0,
            ],
            'rate_neighbor_other' => [
                'hourlyRate' => 500,
                'dailyRate' => 3000,
            ],
            'rate_outsider_other' => [
                'hourlyRate' => 1000,
                'dailyRate' => 5000,
            ],
        ];
    }
}
