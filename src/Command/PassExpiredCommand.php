<?php

namespace App\Command;

use App\Entity\Booking;
use App\Entity\CooperatorPass;
use App\Repository\CooperatorPassRepository;
use App\Service\BillingCalculator;
use App\Service\BillingAccess;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PassExpiredCommand extends Command
{
    protected static $defaultName = 'app:pass-expired';
    protected static $defaultDescription = 'Archives expired cooperator passes';

    private EntityManagerInterface $manager;
    private CooperatorPassRepository $repository;
    private BillingCalculator $calculator;
    private BillingAccess $access;
    private Mailer $mailer;

    public function __construct(
        EntityManagerInterface $manager,
        CooperatorPassRepository $repository,
        BillingCalculator $calculator,
        BillingAccess $access,
        Mailer $mailer
    ) {
        $this->manager = $manager;
        $this->repository = $repository;
        $this->calculator = $calculator;
        $this->access = $access;
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $expired = $this->repository->findExpiredWithFutureBookings();
        $expiredBookings = [];
        $report = [];

        /** @var CooperatorPass $pass */
        foreach ($expired as ['pass' => $pass, 'bookings' => $bookings]) {
            $owner = $pass->getOwner();
            $level = $this->access->grantLevel($owner->getId());
            $report['passes'][] = [
                'name' => $owner->getName(),
                'level' => $level
            ];

            $pass->archive();
            $io->note(sprintf(
                'Cooperator pass %d from owner %s has expired.',
                $pass->getId(),
                $owner->getName()
            ));

            $expiredBookings = array_merge($expiredBookings, $bookings);

            $this->mailer->sendPassExpired($owner, $level);
        }
        
        $io->success('All cooperator passes have been checked.');

        /** @var Booking $booking */
        foreach ($expiredBookings as $booking) {
            if ($this->calculator->hasRate($booking)) {
                $oldBills = $booking->getCosts()->toArray();
                $oldPrice = $this->calculator->sumPrices($oldBills);
                
                foreach ($oldBills as $bill) {
                    $this->manager->remove($bill);
                }

                $bills = $this->calculator->estimate($booking);
                $newPrice = $this->calculator->sumPrices($bills);

                foreach ($bills as $bill) {
                    $this->manager->persist($bill);
                }

                $io->note(sprintf(
                    'Booking %d from applicant %s has changed price from %g € to %g €.',
                    $booking->getId(),
                    $booking->getApplicant()->getName(),
                    $oldPrice / 100,
                    $newPrice / 100
                ));

                $report['bookings'][] = [
                    'booking' => $booking,
                    'isDeleted' => false,
                    'price' => $newPrice,
                ];
            } else {
                $this->manager->remove($booking);
                $io->note(sprintf(
                    'Booking %d from applicant %s is no longer allowed.',
                    $booking->getId(),
                    $booking->getApplicant()->getName()
                ));

                $report['bookings'][] = [
                    'booking' => $booking,
                    'isDeleted' => true,
                ];
            }
        }
        
        $this->manager->flush();

        if (count($report) > 0) {
            $this->mailer->sendPassExpiredReport($report);
        }
        
        $io->success('All related bookings have been updated.');

        return Command::SUCCESS;
    }
}
