<?php

namespace App\Command;

use App\Repository\CooperatorPassRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:pass-extend',
    description: 'One-time command, extends passes so the end is on the first '
        . 'day of a month. Should quickly become deprecated unless *bugs*.',
)]
class PassExtendCommand extends Command
{
    private CooperatorPassRepository $repository;
    private EntityManagerInterface $manager;

    public function __construct(
        CooperatorPassRepository $repository,
        EntityManagerInterface $manager
    ) {
        $this->repository = $repository;
        $this->manager = $manager;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                'start',
                InputArgument::REQUIRED,
                'Start date with yyyy-mm-dd format'
            )
        ;
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $start = \DateTimeImmutable::createFromFormat(
            'Y-m-d',
            $input->getArgument('start')
        );
        $passes = $this->repository->findEndingAfter($start);

        foreach ($passes as $pass) {
            $pass->setEnd($pass->getEnd());
        }

        $this->manager->flush();

        return Command::SUCCESS;
    }
}
