<?php

namespace App\EventSubscriber;

use App\Entity\Booking;
use App\Repository\BookingRepository;
use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use CalendarBundle\Event\CalendarEvent;
use DateTimeZone;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CalendarSubscriber implements EventSubscriberInterface
{
    private BookingRepository $repository;

    private TranslatorInterface $translator;

    private UrlGeneratorInterface $router;

    public function __construct(
        BookingRepository $repository,
        TranslatorInterface $translator,
        UrlGeneratorInterface $router
    ) {
        $this->repository = $repository;
        $this->translator = $translator;
        $this->router = $router;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CalendarEvents::SET_DATA => "onCalendarSetData",
        ];
    }

    public function onCalendarSetData(CalendarEvent $calendar)
    {
        $filters = $calendar->getFilters();
        $bookings = $this->repository->findForCalendar(
            $calendar->getStart(),
            $calendar->getEnd(),
            key_exists("room", $filters) ? $filters["room"] : null
        );

        /** @var Booking $booking */
        foreach ($bookings as $booking) {
            $place = $booking->getPlace();
            $applicant = $booking->getApplicant();
            $name =
                $applicant->getName() ?:
                ucfirst($this->translator->trans("member.level.disabled"));
            $options = [
                "editable" => false,
                "borderColor" => $place->getColor(),
                "extendedProps" => [
                    "applicantId" => $applicant->getId(),
                    "applicantName" => $name,
                    "applicantUrl" => $this->router->generate("member_show", [
                        "id" => $applicant->getId(),
                    ]),
                    "bookingsUrl" => $this->router->generate(
                        "booking_index_applicant",
                        [
                            "id" => $applicant->getId(),
                        ]
                    ),
                    "placeId" => $place->getId(),
                    "placePosition" => $place->getPosition(),
                    "placeName" => $place->getName(),
                    "length" =>
                        $booking->getLength() .
                        " " .
                        $this->translator->trans(
                            "booking.field.length." . $booking->getUnit()
                        ),
                    "isApproved" => $booking->getIsApproved(),
                    "allowUrl" => $this->router->generate("booking_allow", [
                        "id" => $booking->getId(),
                    ]),
                    "denyUrl" => $this->router->generate("booking_deny", [
                        "id" => $booking->getId(),
                    ]),
                    "comment" => $booking->getComment(),
                ],
            ];
            $title = "";

            if ($booking->getIsApproved()) {
                $options["backgroundColor"] = $place->getColor();
                $options["extendedProps"][
                    "modalTitle"
                ] = $this->translator->trans("booking.field.isApproved");
                $title = $this->translator->trans("booking.event.approved", [
                    "%name%" => $name,
                    "%place%" => $place->getName(),
                ]);
            } else {
                $options["backgroundColor"] = "white";
                $options["textColor"] = $place->getColor();
                $options["extendedProps"][
                    "modalTitle"
                ] = $this->translator->trans("booking.field.isRequest");
                $title = $this->translator->trans("booking.event.request", [
                    "%name%" => $name,
                    "%place%" => $place->getName(),
                ]);
            }

            // calendar bundle strips timezone so I need to convert dates
            $start = $booking->getStart()->setTimezone(new DateTimeZone("Europe/Paris"));
            $end = $booking->getEnd()->setTimezone(new DateTimeZone("Europe/Paris"));

            $event = new Event($title, $start, $end);

            $event->setAllDay($booking->isFullDays());
            $event->setOptions($options);
            $calendar->addEvent($event);
        }
    }
}
