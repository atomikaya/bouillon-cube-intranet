<?php

namespace App\Twig;

use App\Entity\Booking;
use App\Util\AccurateDiff;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('booking_length', [$this, 'getBookingLength']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'count_booking_requests',
                [AppRuntime::class, 'countBookingRequests']
            ),
            new TwigFunction(
                'count_waiting_members',
                [AppRuntime::class, 'countWaitingMembers']
            ),
            new TwigFunction(
                'count_due_bills',
                [AppRuntime::class, 'countDueBills']
            ),
        ];
    }

    public function getBookingLength(array $booking): float
    {
        $duration = AccurateDiff::diff($booking['start'], $booking['end']);

        switch ($booking['unit']) {
            case Booking::UNIT_HOUR:
                return $duration->days * 24 + $duration->h + $duration->i / 60;
            case Booking::UNIT_DAY:
                return $duration->days;
            case Booking::UNIT_MONTH:
                return $duration->m;
            default:
                return 0;
        }
    }
}
