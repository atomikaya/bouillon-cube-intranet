<?php

namespace App\Twig;

use App\Entity\Member;
use App\Repository\BookingBillRepository;
use App\Repository\BookingRepository;
use App\Repository\CooperatorPassRepository;
use App\Repository\MemberRepository;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    private BookingRepository $bookingRepository;

    private MemberRepository $memberRepository;

    private CooperatorPassRepository $passRepository;

    private BookingBillRepository $billRepository;

    // buffers to avoid requesting twice
    private int $bookingRequestCount = -1;

    private int $totalBookingRequestsCount = -1;

    private int $registrationCount = -1;

    private int $waitingPassCount = -1;

    private int $dueBills = -1;

    public function __construct(
        BookingRepository $bookingRepository,
        MemberRepository $memberRepository,
        CooperatorPassRepository $passRepository,
        BookingBillRepository $billRepository
    ) {
        $this->bookingRepository = $bookingRepository;
        $this->memberRepository = $memberRepository;
        $this->passRepository = $passRepository;
        $this->billRepository = $billRepository;
    }

    public function countBookingRequests(Member $applicant = null): int
    {
        if (is_null($applicant)) {
            if ($this->totalBookingRequestsCount < 0) {
                $this->totalBookingRequestsCount = $this->bookingRepository
                    ->count(['isApproved' => false])
                ;
            }

            return $this->totalBookingRequestsCount;
        }

        if ($this->bookingRequestCount < 0) {
            ['count' => $count] = $this->bookingRepository
                ->countWaitingByApplicant($applicant)
            ;
            $this->bookingRequestCount = $count;
        }
        
        return $this->bookingRequestCount;
    }

    public function countWaitingMembers(): int
    {
        if ($this->registrationCount < 0) {
            $this->registrationCount = $this->memberRepository->count([
                'isActive' => false,
                'isWaiting' => true,
            ]);
        }

        if ($this->waitingPassCount < 0) {
            $this->waitingPassCount = $this->passRepository
                ->count(['isWaiting' => true])
            ;
        }
        
        return $this->registrationCount + $this->waitingPassCount;
    }

    public function countDueBills(): int
    {
        if ($this->dueBills < 0) {
            $this->dueBills = $this->billRepository
                ->countDue()
            ;
        }
        
        return $this->dueBills;
    }
}
