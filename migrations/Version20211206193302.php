<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211206193302 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE booking_bill (id INT AUTO_INCREMENT NOT NULL, source_id INT DEFAULT NULL, receiver_id INT NOT NULL, price INT NOT NULL, start DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_62B03BAD953C1C61 (source_id), INDEX IDX_62B03BADCD53EDB6 (receiver_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE booking_bill ADD CONSTRAINT FK_62B03BAD953C1C61 FOREIGN KEY (source_id) REFERENCES booking (id)');
        $this->addSql('ALTER TABLE booking_bill ADD CONSTRAINT FK_62B03BADCD53EDB6 FOREIGN KEY (receiver_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE booking DROP price');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE booking_bill');
        $this->addSql('ALTER TABLE booking ADD price INT NOT NULL');
    }
}
