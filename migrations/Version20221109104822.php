<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * 6 month rate follows pass.
 */
final class Version20221109104822 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '6 month rate follows pass';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'UPDATE rental_price
            SET follows_pass = 1
            WHERE hour_seeking = "6 months"'
        );

    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            'UPDATE rental_price
            SET follows_pass = 0'
        );
    }
}
