<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211124125345 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cooperator_pass (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, start DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', end DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', is_active TINYINT(1) NOT NULL, is_waiting TINYINT(1) NOT NULL, INDEX IDX_8D4E3F8B7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cooperator_pass ADD CONSTRAINT FK_8D4E3F8B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user DROP membership_end');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE cooperator_pass');
        $this->addSql('ALTER TABLE user ADD membership_end DATE DEFAULT NULL COMMENT \'(DC2Type:date_immutable)\'');
    }
}
