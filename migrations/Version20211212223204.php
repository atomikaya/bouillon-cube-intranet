<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211212223204 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE member (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, email VARCHAR(180) NOT NULL, address LONGTEXT DEFAULT NULL, is_neighbor TINYINT(1) DEFAULT NULL, is_active TINYINT(1) NOT NULL, is_waiting TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_70E4FA78E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE user_registration');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE97139001');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE97139001 FOREIGN KEY (applicant_id) REFERENCES member (id)');
        $this->addSql('ALTER TABLE booking_bill DROP FOREIGN KEY FK_62B03BADCD53EDB6');
        $this->addSql('ALTER TABLE booking_bill ADD CONSTRAINT FK_62B03BADCD53EDB6 FOREIGN KEY (receiver_id) REFERENCES member (id)');
        $this->addSql('ALTER TABLE cooperator_pass DROP FOREIGN KEY FK_8D4E3F8B7E3C61F9');
        $this->addSql('ALTER TABLE cooperator_pass ADD CONSTRAINT FK_8D4E3F8B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES member (id)');
        $this->addSql('ALTER TABLE user ADD owner_id INT DEFAULT NULL, DROP name, DROP is_neighbor, DROP is_active');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6497E3C61F9 FOREIGN KEY (owner_id) REFERENCES member (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6497E3C61F9 ON user (owner_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE97139001');
        $this->addSql('ALTER TABLE booking_bill DROP FOREIGN KEY FK_62B03BADCD53EDB6');
        $this->addSql('ALTER TABLE cooperator_pass DROP FOREIGN KEY FK_8D4E3F8B7E3C61F9');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6497E3C61F9');
        $this->addSql('CREATE TABLE user_registration (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, email VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, address LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, is_neighbor TINYINT(1) NOT NULL, membership_end DATE DEFAULT NULL COMMENT \'(DC2Type:date_immutable)\', is_cooperator TINYINT(1) NOT NULL, membership_start DATE DEFAULT NULL COMMENT \'(DC2Type:date_immutable)\', UNIQUE INDEX UNIQ_E264DBA5E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE member');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE97139001');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE97139001 FOREIGN KEY (applicant_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE booking_bill DROP FOREIGN KEY FK_62B03BADCD53EDB6');
        $this->addSql('ALTER TABLE booking_bill ADD CONSTRAINT FK_62B03BADCD53EDB6 FOREIGN KEY (receiver_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE cooperator_pass DROP FOREIGN KEY FK_8D4E3F8B7E3C61F9');
        $this->addSql('ALTER TABLE cooperator_pass ADD CONSTRAINT FK_8D4E3F8B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('DROP INDEX UNIQ_8D93D6497E3C61F9 ON user');
        $this->addSql('ALTER TABLE user ADD name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD is_neighbor TINYINT(1) NOT NULL, ADD is_active TINYINT(1) NOT NULL, DROP owner_id');
    }
}
