<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Give my user admin and user impersonation powers.
 */
final class Version20221114100147 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Give my user admin and user impersonation powers.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'UPDATE user
            SET roles = "[\"ROLE_ADMIN\", \"ROLE_ALLOWED_TO_SWITCH\"]"
            WHERE email = "coucou@kayathommy.fr"'
        );

    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            'UPDATE user
            SET roles = "[]"
            WHERE email = "coucou@kayathommy.fr"'
        );

    }
}
