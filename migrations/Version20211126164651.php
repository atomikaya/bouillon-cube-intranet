<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211126164651 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE rental_price (id INT AUTO_INCREMENT NOT NULL, hourly_rate INT DEFAULT NULL, daily_rate INT DEFAULT NULL, monthly_rate INT DEFAULT NULL, overtime_rate INT DEFAULT NULL, hourly_cap INT NOT NULL, daily_cap INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room (id INT AUTO_INCREMENT NOT NULL, cooperator_rate_id INT DEFAULT NULL, neighbor_rate_id INT DEFAULT NULL, outsider_rate_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_729F519B1256B20D (cooperator_rate_id), INDEX IDX_729F519BA0664754 (neighbor_rate_id), INDEX IDX_729F519B8BA8DF14 (outsider_rate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B1256B20D FOREIGN KEY (cooperator_rate_id) REFERENCES rental_price (id)');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519BA0664754 FOREIGN KEY (neighbor_rate_id) REFERENCES rental_price (id)');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B8BA8DF14 FOREIGN KEY (outsider_rate_id) REFERENCES rental_price (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519B1256B20D');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519BA0664754');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519B8BA8DF14');
        $this->addSql('DROP TABLE rental_price');
        $this->addSql('DROP TABLE room');
    }
}
