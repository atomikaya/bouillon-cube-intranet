<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Teleconf room set to free with new free rate.
 */
final class Version20221109102803 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Teleconf room set to free with new free rate.';
    }

    public function up(Schema $schema): void
    {
        // add free rate
        $this->addSql(
            'INSERT INTO rental_price
            VALUES (61, 0, 0, 0, 0, 0, 0, "1 month", 0)'
        );
        // set teleconf room to free rate
        $this->addSql(
            'UPDATE room
            SET cooperator_rate_id = 61,
                neighbor_rate_id = 61,
                outsider_rate_id = 61
            WHERE slug = "teleconf"'
        );
    }

    public function down(Schema $schema): void
    {
        // set teleconf room back to office shared room rate
        $this->addSql(
            'UPDATE room
            SET cooperator_rate_id = 52,
                neighbor_rate_id = 53,
                outsider_rate_id = 54
            WHERE slug = "teleconf"'
        );
        // remove free rate
        $this->addSql(
            'DELETE FROM rental_price WHERE id = 61'
        );
    }
}
