<?php

namespace App\Tests\Service;

use App\Entity\Booking;
use App\Entity\RentalPrice;
use App\Entity\Room;
use App\Entity\Member;
use App\Entity\User;
use App\Repository\BookingBillRepository;
use App\Repository\BookingRepository;
use App\Repository\CooperatorPassRepository;
use App\Service\BillingCalculator;
use App\Service\BillingAccess;
use App\Util\AccurateDiff;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class BillingCalculatorTest extends TestCase
{
    // not a test
    public function initCalculator(): BillingCalculator
    {
        $access = $this->createStub(BillingAccess::class);
        $billingRepository = $this->createStub(BookingBillRepository::class);
        $bookingRepository = $this->createStub(BookingRepository::class);
        $passRepository = $this->createStub(CooperatorPassRepository::class);
        $manager = $this->createStub(EntityManagerInterface::class);

        $calculator = new BillingCalculator(
            $access,
            $billingRepository,
            $bookingRepository,
            $passRepository,
            $manager
        );

        $bookingRepository
            ->method('findYearlyByPlaceAndApplicant')
            ->willReturn([]);

        return $calculator;
    }
    
    // not a test
    public function createBookingStub(): array
    {
        $booking = $this->createStub(Booking::class);
        $place = $this->createStub(Room::class);
        $applicant = $this->createStub(Member::class);
        $rate = $this->createStub(RentalPrice::class);
        $access = $this->createStub(User::class);

        $booking->method('getApplicant')->willReturn($applicant);
        $applicant->method('getId')->willReturn(0);
        $applicant->method('getAccess')->willReturn($access);
        $access->method('getRoles')->willReturn([]);
        $booking->method('getPlace')->willReturn($place);
        $place->method('getRateForAccessLevel')->willReturn($rate);
        $rate->method('getHourSeeking')->willReturn('1 month');

        return [$booking, $rate];
    }

    // not a test
    public function createOfficeBookingStub(): Booking
    {
        [$booking, $rate] = $this->createBookingStub();

        $rate->method('getMonthlyRate')->willReturn(25000);
        $rate->method('getMaxHours')->willReturn(0);
        $rate->method('getMaxDays')->willReturn(0);

        return $booking;
    }

    // not a test
    public function createSharedOfficeBookingStub(): Booking
    {
        [$booking, $rate] = $this->createBookingStub();

        $rate->method('getHourlyRate')->willReturn(100);
        $rate->method('getDailyRate')->willReturn(1200);
        $rate->method('getMonthlyRate')->willReturn(10000);
        $rate->method('getMaxHours')->willReturn(0);
        $rate->method('getMaxDays')->willReturn(0);

        return $booking;
    }

    // not a test
    public function createTeleconfBookingStub(): Booking
    {
        [$booking, $rate] = $this->createBookingStub();

        $rate->method('getHourlyRate')->willReturn(0);
        $rate->method('getOvertimeRate')->willReturn(500);
        $rate->method('getMaxHours')->willReturn(10);
        $rate->method('getMaxDays')->willReturn(0);
        $rate->method('hasMaxHours')->willReturn(true);

        return $booking;
    }

    // not a test
    public function createTheaterBookingStub(): Booking
    {
        [$booking, $rate] = $this->createBookingStub();

        $rate->method('getHourlyRate')->willReturn(0);
        $rate->method('getDailyRate')->willReturn(0);
        $rate->method('getOvertimeRate')->willReturn(500);
        $rate->method('getMaxHours')->willReturn(0);
        $rate->method('getMaxDays')->willReturn(10);
        $rate->method('hasMaxDays')->willReturn(true);

        return $booking;
    }

    // not a test
    public function createKioskBookingStub(): Booking
    {
        [$booking, $rate] = $this->createBookingStub();

        $rate->method('getHourlyRate')->willReturn(0);
        $rate->method('getDailyRate')->willReturn(0);
        $rate->method('getOvertimeRate')->willReturn(500);
        $rate->method('getMaxHours')->willReturn(6);
        $rate->method('getMaxDays')->willReturn(9);
        $rate->method('hasMaxHours')->willReturn(true);
        $rate->method('hasMaxDays')->willReturn(true);

        return $booking;
    }

    // not a test
    public function initBooking(
        Booking $booking,
        string $start,
        string $end
    ): Booking {
        $startDate = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $start);
        $endDate = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $end);

        $booking->method('getStart')->willReturn($startDate);
        $booking->method('getEnd')->willReturn($endDate);
        $booking->method('getDuration')->willReturn(AccurateDiff::diff($startDate, $endDate));

        return $booking;
    }

    /**
     * @dataProvider officeBookingsProvider
     * @dataProvider sharedOfficeBookingsProvider
     * @dataProvider teleconfBookingsProvider
     * @dataProvider theaterBookingsProvider
     * @dataProvider kioskBookingsProvider
     */
    public function testEstimate(
        Booking $booking,
        string $unit,
        string $start,
        string $end,
        int $expected
    ) {
        $calculator = $this->initCalculator();
        $booking = $this->initBooking($booking, $start, $end);

        $booking->method('getUnit')->willReturn($unit);
        
        $bills = $calculator->estimate($booking);

        $this->assertEquals($expected, $calculator->sumPrices($bills));
    }

    public function officeBookingsProvider(): array
    {
        return [
            'office: first 6 months of 22' => [
                $this->createOfficeBookingStub(),
                'month',
                '01/01/2022 00:00',
                '01/07/2022 00:00',
                150000,
            ],
            'office: last month of 21 and first of 22' => [
                $this->createOfficeBookingStub(),
                'month',
                '01/12/2021 00:00',
                '01/02/2022 00:00',
                50000,
            ],
            'office: full 2022 year' => [
                $this->createOfficeBookingStub(),
                'month',
                '01/01/2022 00:00',
                '01/01/2023 00:00',
                300000,
            ],
            'office: 10 years and 3 months' => [
                $this->createOfficeBookingStub(),
                'month',
                '01/03/2022 00:00',
                '01/06/2032 00:00',
                3075000,
            ],
        ];
    }

    public function sharedOfficeBookingsProvider(): array
    {
        return [
            // hours
            'shared office: 9 hours' => [
                $this->createSharedOfficeBookingStub(),
                'hour',
                '15/12/2021 09:00',
                '15/12/2021 18:00',
                900,
            ],
            'shared office: full day in hours (23.5)' => [
                $this->createSharedOfficeBookingStub(),
                'hour',
                '20/12/2021 00:00',
                '20/12/2021 23:30',
                2350,
            ],
            'shared office: 3 hours starting on a half hour' => [
                $this->createSharedOfficeBookingStub(),
                'hour',
                '30/12/2021 05:30',
                '30/12/2021 08:30',
                300,
            ],
            'shared office: 6.5 hours starting on a half hour' => [
                $this->createSharedOfficeBookingStub(),
                'hour',
                '27/02/2022 09:30',
                '27/02/2022 16:00',
                650,
            ],
            'shared office: half hour' => [
                $this->createSharedOfficeBookingStub(),
                'hour',
                '17/02/2022 09:30',
                '17/02/2022 10:00',
                50,
            ],
            // days
            'shared office: 12 days in december' => [
                $this->createSharedOfficeBookingStub(),
                'day',
                '06/12/2021 00:00',
                '18/12/2021 00:00',
                14400,
            ],
            'shared office: full january 22 month' => [
                $this->createSharedOfficeBookingStub(),
                'day',
                '01/01/2022 00:00',
                '01/02/2022 00:00',
                37200,
            ],
            'shared office: 3 days between 2021 and 2022' => [
                $this->createSharedOfficeBookingStub(),
                'day',
                '30/12/2021 00:00',
                '02/01/2022 00:00',
                3600,
            ],
            'shared office: 5 days between non-bissextile february and march' => [
                $this->createSharedOfficeBookingStub(),
                'day',
                '27/02/2022 00:00',
                '04/03/2022 00:00',
                6000,
            ],
            // months
            'shared office: first 6 months of 22' => [
                $this->createSharedOfficeBookingStub(),
                'month',
                '01/01/2022 00:00',
                '01/07/2022 00:00',
                60000,
            ],
            'shared office: last month of 21 and first of 22' => [
                $this->createSharedOfficeBookingStub(),
                'month',
                '01/12/2021 00:00',
                '01/02/2022 00:00',
                20000,
            ],
            'shared office: full 2022 year' => [
                $this->createSharedOfficeBookingStub(),
                'month',
                '01/01/2022 00:00',
                '01/01/2023 00:00',
                120000,
            ],
            'shared office: 10 years and 3 months' => [
                $this->createSharedOfficeBookingStub(),
                'month',
                '01/03/2022 00:00',
                '01/06/2032 00:00',
                1230000,
            ],
        ];
    }

    public function teleconfBookingsProvider(): array
    {
        return [
            'teleconf: 10 regular hours' => [
                $this->createTeleconfBookingStub(),
                'hour',
                '01/01/2022 08:00',
                '01/01/2022 18:00',
                0,
            ],
            'teleconf: 2 hours overtime' => [
                $this->createTeleconfBookingStub(),
                'hour',
                '20/12/2021 09:00',
                '20/12/2021 21:00',
                1000,
            ],
            'teleconf: 4.5 hours overtime starting with a half hour' => [
                $this->createTeleconfBookingStub(),
                'hour',
                '05/01/2022 02:30',
                '05/01/2022 17:00',
                2250,
            ],
            'teleconf: 1 hour overtime on half hours' => [
                $this->createTeleconfBookingStub(),
                'hour',
                '13/03/2022 08:30',
                '13/03/2022 19:30',
                500,
            ],
        ];
    }

    public function theaterBookingsProvider(): array
    {
        return [
            // hours
            'theater: 3 hours' => [
                $this->createTheaterBookingStub(),
                'hour',
                '01/01/2022 09:00',
                '01/01/2022 12:00',
                0,
            ],
            'theater: full day in hours (23.5)' => [
                $this->createTheaterBookingStub(),
                'hour',
                '25/12/2021 00:00',
                '25/12/2021 23:30',
                0,
            ],
            'theater: half hour starting with half hour' => [
                $this->createTheaterBookingStub(),
                'hour',
                '12/12/2021 19:30',
                '12/12/2021 20:00',
                0,
            ],
            // days
            'theater: 10 regular days' => [
                $this->createTheaterBookingStub(),
                'day',
                '13/12/2021 00:00',
                '23/12/2021 00:00',
                0,
            ],
            'theater: 1 overtime day' => [
                $this->createTheaterBookingStub(),
                'day',
                '06/12/2021 00:00',
                '17/12/2021 00:00',
                12000,
            ],
            'theater: 11 days split 4 / 7 over 2 years' => [
                $this->createTheaterBookingStub(),
                'day',
                '27/12/2021 00:00',
                '07/01/2022 00:00',
                0,
            ],
            'theater: 5 overtime days split 3 / 2 over 2 months' => [
                $this->createTheaterBookingStub(),
                'day',
                '18/11/2021 00:00',
                '03/12/2021 00:00',
                60000,
            ],
        ];
    }

    public function kioskBookingsProvider(): array
    {
        return [
            // hours
            'kiosk: 6 regular hours' => [
                $this->createKioskBookingStub(),
                'hour',
                '01/01/2022 08:00',
                '01/01/2022 14:00',
                0,
            ],
            'kiosk: 2 overtime hours' => [
                $this->createKioskBookingStub(),
                'hour',
                '06/02/2022 09:00',
                '06/02/2022 17:00',
                1000,
            ],
            'kiosk: 1.5 hours starting with half hour' => [
                $this->createKioskBookingStub(),
                'hour',
                '15/12/2021 13:30',
                '15/12/2021 15:00',
                0,
            ],
            'kiosk: .5 overtime hour' => [
                $this->createKioskBookingStub(),
                'hour',
                '04/12/2021 06:00',
                '04/12/2021 12:30',
                250,
            ],
            // day
            'kiosk: 1 day (18 overtime hours)' => [
                $this->createKioskBookingStub(),
                'day',
                '27/12/2021 00:00',
                '28/12/2021 00:00',
                9000,
            ],
            'kiosk: 3 days (66 overtime hours)' => [
                $this->createKioskBookingStub(),
                'day',
                '13/12/2021 00:00',
                '16/12/2021 00:00',
                33000,
            ],
            'kiosk: 3 days split 1 / 2 between months' => [
                $this->createKioskBookingStub(),
                'day',
                '30/11/2021 00:00',
                '03/12/2021 00:00',
                30000,
            ],
            'kiosk: 10 days split 9 / 1 between months' => [
                $this->createKioskBookingStub(),
                'day',
                '22/11/2021 00:00',
                '02/12/2021 00:00',
                117000,
            ],
            'kiosk: 12 days split 10 / 2 between years' => [
                $this->createKioskBookingStub(),
                'day',
                '22/12/2021 00:00',
                '03/01/2022 00:00',
                138000,
            ],
        ];
    }
}
