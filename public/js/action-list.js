import {show} from './alert.js'
import {html} from './helpers.js'

const SPINNER_TPL = html`
    <span
        class="spinner-border spinner-border-sm" role="status"
        aria-hidden="true"
    ></span>
`
const TIMEOUT = 2000

let runningActions = 0
let reloadTimeout = null
let clickCallback = null
let reloadCallback = null

// lib

export const refresh = (target, onclick, onreload) => {
    target
        .querySelectorAll('.btn--remote')
        .forEach(btn => btn.addEventListener('click', sendAction))
    
    clickCallback = onclick
    reloadCallback = onreload
}

/**
 * @param {MouseEvent} e 
 */
const sendAction = e => {
    e.stopPropagation()
    e.preventDefault()

    console.log('clicked')

    let {target: {dataset: {action, method}}} = e
    
    disableLine(e.target)
    incrementActions()

    fetch(action, { method })
        .then(res => res.json())
        .then(resolveAction)
}

const incrementActions = () => {
    clearTimeout(reloadTimeout)

    runningActions++
}

const decrementActions = () => {
    if (runningActions > 0) {
        runningActions--
    }

    if (runningActions < 1) {
        reloadTimeout = setTimeout(() => reloadCallback ? reloadCallback() : location.reload(), TIMEOUT)
    }
}

const resolveAction = data => {
    for (const level in data.messages) {
        data.messages[level].forEach(msg => show(level, msg))

        if (clickCallback) {
            clickCallback()
        }

        decrementActions()
    }
}

const disableLine = target => {
    let cell = target.parentElement
    let btns = cell.querySelectorAll('.btn--remote')

    btns.forEach(btn => disableButton(btn === target, btn))
}

const disableButton = (isTarget, btn) => {
    btn.setAttribute('disabled', 'disabled')

    if (isTarget) {
        btn.prepend(SPINNER_TPL.cloneNode(true))
    }
}

// init

refresh(document)
