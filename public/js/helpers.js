/**
 * Turns a template into an HTMLElement
 * @param {String[]} strings 
 * @param  {String[]} vars 
 * @returns {HTMLElement|null}
 */
export const html = (strings, ...vars) => {
    let container = document.createElement('div')
    let content = strings
        .map((s, i) => i >= vars.length ? [s] : [s, vars[i]])
        .flat()
        .join('')

    container.innerHTML = content

    return container.firstElementChild
}
