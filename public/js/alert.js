import {html} from './helpers.js'

const ALERT_TPL = (level, msg) => html`
    <p
        class="alert alert-${level} alert-dismissible fade show"
        role="alert"
    >
        ${msg}
        <button
            type="button" class="btn-close" data-bs-dismiss="alert"
            aria-label="Effacer"
        ></button>
    </p>
`
const TIMEOUT = 5000

const alertBox = document.querySelector('.alert__box')

/**
 * Display an timed alert in the alert box
 * @param {String} level 
 * @param {String} msg 
 */
export const show = (level, msg) => {
    let alert = ALERT_TPL(level, msg)

    alertBox.appendChild(alert)

    let timeOut = setTimeout(() => alert.remove(), TIMEOUT)
}
