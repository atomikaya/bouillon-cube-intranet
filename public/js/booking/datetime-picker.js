export const dom = {
    startDate: document.getElementById('booking_start_date'),
    startTime: document.getElementById('booking_start_time_hour').parentElement,
    startHour: document.getElementById('booking_start_time_hour'),
    startMinute: document.getElementById('booking_start_time_minute'),
    endDate: document.getElementById('booking_end_date'),
    endTime: document.getElementById('booking_end_time_hour').parentElement,
    endHour: document.getElementById('booking_end_time_hour'),
    endMinute: document.getElementById('booking_end_time_minute'),
    repeatWeekly: document.getElementById('booking_repeat_weekly'),
    repeatWeeklyUntil: document.getElementById('booking_repeat_weekly_until'),
    inputs: [],
}

dom.inputs = [
    dom.startDate,
    dom.startHour,
    dom.startMinute,
    dom.endDate,
    dom.endHour,
    dom.endMinute,
]
