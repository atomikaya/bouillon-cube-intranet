export const init = () => {
    let list = document.getElementById('room_list')

    list.querySelectorAll('.nav-link').forEach(link => {
        link.addEventListener('click', toggleRoom(list))
    })
}

const toggleRoom = list => e => {
    e.preventDefault()

    let link = e.target
    let id = link.dataset.roomId

    if (link.classList.contains('active')) {
        link.classList.remove('active')
        document.dispatchEvent(new CustomEvent('filter-reset'))
    } else {
        let previous = list.querySelector('.active')

        if (previous) {
            previous.classList.remove('active')
        }
        
        link.classList.add('active')
        document.dispatchEvent(new CustomEvent('filter-room', {detail: id}))
    }
}
