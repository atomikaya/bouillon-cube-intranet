export const init = () => {
    buildUnitHelp()

    dom.place.addEventListener('change', watchRoom)

    watchRoom({target: dom.place})
}

// lib

const watchRoom = ({target}) => {
    Object.values(dom.priceTable).forEach(hidePriceRow)
    
    let { slug, ...rate } = target.options[target.selectedIndex].dataset

    toggleUnit(typeof rate.hourlyRate !== 'undefined', 'hour')
    toggleUnit(typeof rate.dailyRate !== 'undefined', 'day')
    toggleUnit(typeof rate.monthlyRate !== 'undefined', 'month')

    checkActiveUnit()
    showPreview(slug)

    if (Object.values(rate).every(r => r === '0')) {
        dom.priceFree.classList.remove('d-none')
    } else {
        dom.priceFree.classList.add('d-none')
        Object.entries(rate).forEach(([key, value]) => {
            if (key === 'hourSeeking') {
                dom.priceTable[key].innerHTML = hourSeekingTranslation[value]
            } else {
                dom.priceTable[key].innerHTML = value
            }
    
            showPriceRow(dom.priceTable[key])
        })
    }

}

const hourSeekingTranslation = {
    '1 month': '1 mois',
    '6 months': '6 mois',
}

const toggleUnit = (on, unitName) => {
    if (on) {
        dom.unit[unitName].removeAttribute('disabled')
        dom.unit[unitName + 'Help'].classList.remove('d-none')
    } else {
        dom.unit[unitName].setAttribute('disabled', 'disabled')
        dom.unit[unitName + 'Help'].classList.add('d-none')
    }
}

const checkActiveUnit = () => {
    let isMisplaced = Array
        .from(dom.unit.radio)
        .find(input => input.value === document.booking['booking[unit]'].value)
        .hasAttribute('disabled')
    
    if (!isMisplaced) return
    
    let unit = Array
        .from(dom.unit.radio)
        .find(input => !input.hasAttribute('disabled'))

    document.booking['booking[unit]'].value = unit.value
    unit.dispatchEvent(new Event('change'))
}

const showPreview = slug => {
    const items = document.querySelectorAll('.preview_item')
    const active = document.getElementById('preview_' + slug)

    Object.values(items).forEach(item => item.classList.add('d-none'))

    if (active) {
        active.classList.remove('d-none')
    }
}

const buildUnitHelp = () => {
    dom.unit.radio.forEach(input => {
        let p = document.createElement('p')

        p.classList.add('form-text')
        p.innerHTML = 'Option active'

        input.parentElement.append(p)
        dom.unit[input.value + 'Help'] = p
    })

    dom.unit.hourHelp.innerHTML = cutTemplate('booking_price_hourly_rate')
        // + cutTemplate('booking_price_max_hours')
    dom.unit.dayHelp.innerHTML = cutTemplate('booking_price_daily_rate')
        // + cutTemplate('booking_price_max_days')
    dom.unit.monthHelp.innerHTML = cutTemplate('booking_price_monthly_rate')

    dom.priceTable = {
        hourlyRate: document.getElementById('booking_price_hourly_rate'),
        dailyRate: document.getElementById('booking_price_daily_rate'),
        monthlyRate: document.getElementById('booking_price_monthly_rate'),
        overtimeRate: document.getElementById('booking_price_overtime_rate'),
        maxHours: document.getElementById('booking_price_max_hours'),
        hourSeeking: document.getElementById('booking_price_hour_seeking'),
        maxDays: document.getElementById('booking_price_max_days'),
    }
}

const cutTemplate = id => {
    let template = document.getElementById(id).parentElement.outerHTML

    document.getElementById(id).parentElement.outerHTML = ''

    return template
}

const hidePriceRow = el =>
    el.parentElement.classList.add('d-none')

const showPriceRow = el =>
    el.parentElement.classList.remove('d-none')

// state

export const dom = {
    // room selector
    place: document.getElementById('booking_place'),
    priceTable: {},
    priceFree: document.getElementById('booking_price_free'),
    unit: {
        radio: document.querySelectorAll('#booking_unit input'),
        hour: document.querySelector('#booking_unit [value=hour]'),
        day: document.querySelector('#booking_unit [value=day]'),
        month: document.querySelector('#booking_unit [value=month]'),
    },
}
