// concurrence alerts component

import {DateTime} from '../lib/luxon.js'
import * as DateTimePicker from './datetime-picker.js'
import * as PlacePicker from './place-picker.js'

export const init = () => {
    PlacePicker.dom.place.addEventListener('change', update)
    Object.values(DateTimePicker.dom.inputs).forEach(
        input => input.addEventListener('change', update)
    )
    document.booking.addEventListener('submit', e => {
        DateTimePicker.dom.endDate.value = getRealEnd()
    })
}

// lib

export const update = (e) => {
    if (!PlacePicker.dom.place.value) return

    // value correction
    if (
        e !== undefined &&
        e.target == DateTimePicker.dom.startDate &&
        document.booking['booking[unit]'].value == 'hour'
    ) {
        updateEndDate()
    }

    let url = `/reservation/info/${PlacePicker.dom.place.value}/` +
        `${DateTimePicker.dom.startDate.value}-` +
        `${DateTimePicker.dom.startHour.value.padStart(2, '0')}-` +
        `${DateTimePicker.dom.startMinute.value.padStart(2, '0')}/` +
        `${getRealEnd()}-` +
        `${DateTimePicker.dom.endHour.value.padStart(2, '0')}-` +
        `${DateTimePicker.dom.endMinute.value.padStart(2, '0')}/` +
        `${document.booking['booking[unit]'].value}/`

    fetch(url)
        .then(res => res.json())
        .then((data) => {
            clearErrors()

            if ('errors' in data) {
                data.errors.violations.forEach(addError)
                disableSubmission()
            } else {
                updateConcurrentAlerts(data.concurrents)
                updatePrice(data.price)
                enableSubmission()
            }
        })
}

const getRealEnd = () => {
    if (document.booking['booking[unit]'].value == 'hour') {
        return DateTimePicker.dom.endDate.value
    }

    return DateTime
        .fromJSDate(
            DateTimePicker.dom.endDate.valueAsDate,
            {zone: 'Europe/Paris'}
        )
        .plus({ days: 1 })
        .toFormat('yyyy-MM-dd')
}

export const updateEndDate = () => {
    DateTimePicker.dom.endDate.value = DateTimePicker.dom.startDate.value
}

const updateConcurrentAlerts = data => {
    if (!data.length > 0) {
        if (concurrentApproved) {
            dom.approvedAlert.classList.add('d-none')
            concurrentApproved = false
        }

        if (concurrentRequest) {
            dom.requestAlert.classList.add('d-none')
            concurrentRequest = false
        }

        return
    }

    let approved = data.filter(b => b.isApproved)
    let requests = data.filter(b => !b.isApproved)

    concurrentApproved = approved.length > 0
    concurrentRequest = requests.length > 0

    if (concurrentApproved) {
        let count = dom.approvedAlert.querySelector('.concurrent_count')
        let list = dom.approvedAlert.querySelector('.concurrent_list')

        count.innerHTML = approved.length
        list.innerHTML = ''

        approved.forEach(b => {
            let p = document.createElement('p')

            p.innerHTML = dom.approvedItem
                .replace('%user_id%', b.applicantId)
                .replace('%user_name%', b.applicantName)
                .replace('%start%', formatDate(b.start))
                .replace('%end%', formatDate(b.end))
            
            list.appendChild(p)
        })

        dom.approvedAlert.classList.remove('d-none')
    }

    if (concurrentRequest) {
        let count = dom.requestAlert.querySelector('.concurrent_count')
        let list = dom.requestAlert.querySelector('.concurrent_list')

        count.innerHTML = requests.length
        list.innerHTML = ''

        requests.forEach(b => {
            let p = document.createElement('p')

            p.innerHTML = dom.requestItem
                .replace('%user_id%', b.applicantId)
                .replace('%user_name%', b.applicantName)
                .replace('%start%', formatDate(b.start))
                .replace('%end%', formatDate(b.end))
            
            list.appendChild(p)
        })

        dom.requestAlert.classList.remove('d-none')
    }
}

const updatePrice = price => {
    let displayPrice = new Intl
        .NumberFormat('fr', { style: 'currency', currency: 'EUR' })
        .format(price / 100)
    dom.priceDisplay.innerHTML = displayPrice
}

const formatDate = dateString => {
    let date = new Date(dateString)

    return padInt(date.getDate()) + '/' +
        padInt(date.getMonth() + 1) + '/' +
        date.getFullYear() + ' ' +
        padInt(date.getUTCHours()) + ':' +
        padInt(date.getMinutes())
}

const padInt = (int, length) => int.toString().padStart(2, '0')

const addError = ({propertyPath, title}) => {
    let parent = document.getElementById(propertyPath === ''
        ? 'booking_data'
        : 'booking_' + propertyPath
    )
    let error = document.createElement('p')

    error.classList.add('invalid-feedback', 'd-block', 'async_error')
    error.innerHTML = title

    parent.append(error)
}

const clearErrors = () => {
    document.querySelectorAll('.invalid-feedback').forEach(error =>
        error.remove()
    )
}

const disableSubmission = () => {
    dom.submitBtns.forEach(btn => btn.setAttribute('disabled', 'disabled'))
}

const enableSubmission = () => {
    dom.submitBtns.forEach(btn => btn.removeAttribute('disabled'))
}

// state

const dom = {
    approvedAlert: document.getElementById('concurrent_approved_alert'),
    requestAlert: document.getElementById('concurrent_request_alert'),
    approvedItem: document.querySelector('#concurrent_approved_alert .concurrent_list').innerHTML,
    requestItem: document.querySelector('#concurrent_request_alert .concurrent_list').innerHTML,
    priceDisplay: document.getElementById('booking_price_display'),
    submitBtns: document.booking.querySelectorAll('[type=submit]'),
}
let concurrentApproved = false
let concurrentRequest = false
