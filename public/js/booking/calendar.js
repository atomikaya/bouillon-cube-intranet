
import {DateTime, Settings} from '../lib/luxon.js'
import * as ActionList from '../action-list.js'

export const init = localUrl => {
    Settings.defaultLocale = 'fr'
    
    eventCreationModal = document.getElementById('eventCreationModal')
    eventRequestModal = document.getElementById('eventRequestModal')
    eventDetailModal = document.getElementById('eventDetailModal')
    eventBlockingModal = document.getElementById('eventBlockingModal')
    
    if (eventRequestModal) {
        eventRequestTemplate = eventRequestModal.innerHTML
    }

    if (eventDetailModal) {
        eventDetailTemplate = eventDetailModal.innerHTML
    }
    
    let calendar = new FullCalendar.Calendar(
        document.getElementById('calendar-holder'),
        buildOptions(localUrl)
    )
    let localSource = calendar.getEventSourceById('local')

    document.addEventListener('filter-room', filterRoom(localSource))
    document.addEventListener('filter-reset', resetFilters(localSource))

    // get referer view and date

    let dateInfo = new URLSearchParams(window.location.search)

    if (dateInfo.has('date') && dateInfo.has('vue')) {
        calendar.changeView(dateInfo.get('vue'))
        calendar.gotoDate(dateInfo.get('date'))
    }

    //

    calendar.render()
}

const buildOptions = localUrl => {
    return {
        dayMaxEvents: true, // allow "more" link when too many events
        datesSet: updateUrl,
        editable: true,
        eventClick,
        eventOrder: ['-allDay', comparePlacePositions],
        eventOrderStrict: ['-allDay', comparePlacePositions],
        eventSources: [
            {
                id: 'local',
                url: localUrl,
                method: 'POST',
                extraParams,
                failure: () => {
                    alert("There was an error while fetching FullCalendar!");
                },
            },
        ],
        headerToolbar: {
            left: 'prev,next',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        initialView: 'timeGridWeek',
        locale: 'fr',
        navLinks: true, // can click day/week names to navigate views
        nowIndicator: true,
        select,
        selectable: true,
        selectOverlap: true,
        timeZone: 'Europe/Paris',
    }
}

const extraParams = () => ({
    filters: JSON.stringify(filters)
})

const eventClick = info => {
    let props = info.event.extendedProps
    let accessId = eventRequestModal ? parseInt(eventRequestModal.dataset.access) : -2
    let hasAccess = accessId === -1 || accessId === props.applicantId

    if (eventRequestTemplate && hasAccess || eventDetailTemplate) {
        let start = DateTime
            .fromJSDate(info.event.start, {zone: 'UTC'})
            .toLocaleString(DateTime.DATETIME_MED)
        let end = DateTime
            .fromJSDate(info.event.end, {zone: 'UTC'})
            //.minus({ minutes: 1 })
            .toLocaleString(DateTime.DATETIME_MED)
        let template = (eventRequestTemplate || eventDetailTemplate)
            .replace('%modalTitle%', props.modalTitle)
            .replace('%applicantName%', props.applicantName)
            .replace('%applicantUrl%', props.applicantUrl)
            .replace('%bookingsUrl%', props.bookingsUrl)
            .replace('%placeName%', props.placeName)
            .replace('%start%', start)
            .replace('%end%', end)
            .replace('%comment%', props.comment)
            .replace('%length%', props.length)
            .replace('%allowUrl%', props.allowUrl)
            .replace(/%denyUrl%/g, props.denyUrl)
        let modal = (eventRequestModal || eventDetailModal)
        
        modal.innerHTML = template

        let modalComponent = new bootstrap.Modal(modal)

        ActionList.refresh(
            modal,
            () => modalComponent.hide(),
            () => info.event.source.refetch(),
        )

        let commentBox = modal.querySelector('.booking-comment')

        if (!props.comment || props.comment === '') {
            commentBox.classList.add('d-none')
        } else {
            commentBox.classList.remove('d-none')
        }

        if (modal === eventDetailModal) {
            let requestActions = modal.querySelector('.request-actions')
            let approvedActions = modal.querySelector('.approved-actions')
    
            if (props.isApproved) {
                requestActions.classList.add('d-none')
                approvedActions.classList.remove('d-none')
            } else {
                requestActions.classList.remove('d-none')
                approvedActions.classList.add('d-none')
            }
        }

        modalComponent.show()
    }
}

const select = info => {
    let modal = eventCreationModal || eventBlockingModal

    if (modal) {
        let startInput = modal.querySelector('#calendar_booking_start')
        let endInput = modal.querySelector('#calendar_booking_end')
        let start = DateTime
            .fromJSDate(info.start, {zone: 'UTC'})
            .toFormat("yyyy-MM-dd'T'HH:mm")
        let end = DateTime
            .fromJSDate(info.end, {zone: 'UTC'})
            .toFormat("yyyy-MM-dd'T'HH:mm")

        startInput.value = start
        endInput.value = end

        new bootstrap.Modal(modal).show()
    }
}

const filterRoom = source => ({detail}) => {
    filters = {room: detail}

    source.refetch()
}

const resetFilters = source => () => {
    filters = {}

    source.refetch()
}

const updateUrl = dateInfo => {
    let [date, ...rest] = dateInfo.startStr.split('T')
    let url = '?date=' + date + '&vue=' + dateInfo.view.type

    history.pushState(null, null, url)
}

const comparePlacePositions = (a, b) =>
    a.extendedProps.placePosition - b.extendedProps.placePosition

let eventRequestModal = undefined
let eventRequestTemplate = undefined
let eventDetailModal = undefined
let eventDetailTemplate = undefined
let eventCreationModal = undefined
let eventBlockingModal = undefined
let filters = {}
