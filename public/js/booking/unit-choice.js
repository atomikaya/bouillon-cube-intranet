// unit choice component

import {DateTime} from '../lib/luxon.js'
import * as DateTimePicker from './datetime-picker.js'
import * as ConcurrenceAlert from './concurrence-alert.js'

export const init = () => {
    document
        .getElementsByName('booking[unit]')
        .forEach(input => {
            input.addEventListener('change', updateFields(true))
        })
    dom.repeatWeekly.addEventListener('change', updateEndWeekField)
    updateFields(false)({target: {value: document.booking['booking[unit]'].value}})
    updateEndWeekField({target: dom.repeatWeekly})
}
// lib

const updateFields = userInput => ({target: {value}}) => {
    switch (unit) {
        case 'hour':
            dom.endDate.classList.remove('d-none')
            break;
        case 'month':
        case 'day':
            dom.startTime.classList.remove('d-none')
            dom.endTime.classList.remove('d-none')
            break;
        default:
            break;
    }

    unit = value
    
    switch (unit) {
        case 'hour':
            dom.endDate.classList.add('d-none')
            dom.repeatWeekly.parentElement.classList.remove('d-none')
            
            if (dom.repeatWeekly.checked) {
                dom.repeatWeeklyUntil.parentElement.classList.remove('d-none')
            }
            
            initHoursChoice()
            break;
        case 'day':
            dom.startTime.classList.add('d-none')
            dom.endTime.classList.add('d-none')
            dom.repeatWeekly.parentElement.classList.add('d-none')
            dom.repeatWeeklyUntil.parentElement.classList.add('d-none')
            initDaysChoice(userInput)
            break;
        case 'month':
            dom.startTime.classList.add('d-none')
            dom.endTime.classList.add('d-none')
            dom.repeatWeekly.parentElement.classList.add('d-none')
            dom.repeatWeeklyUntil.parentElement.classList.add('d-none')
            initMonthsChoice(userInput)
            break;
        default:
            break;
    }

    ConcurrenceAlert.update()
}

const initHoursChoice = () => {
    let start = DateTime.fromJSDate(dom.startDate.valueAsDate, {zone: 'Europe/Paris'})
    let startTime = parseInt(dom.startHour.value) + parseInt(dom.startMinute.value) / 60
    let endTime = parseInt(dom.endHour.value) + parseInt(dom.endMinute.value) / 60

    // if (start < DateTime.now()) {
    //     dom.startDate.value = DateTime.now({zone: 'Europe/Paris'}).plus({hours: 1}).toFormat('yyyy-MM-dd')
    // }

    dom.endDate.value = dom.startDate.value

    if ([dom.startHour, dom.startMinute, dom.endHour, dom.endMinute].every(
        input => input.value === '0'
    )) {
        dom.startHour.value = '8'
        dom.endHour.value = '16'
    } else if (startTime >= endTime) {
        dom.endHour.value = parseInt(dom.startHour.value) + 1
    }
}

const initDaysChoice = userInput => {
    let today = DateTime.now().startOf('day')
    let start = DateTime
        .fromJSDate(dom.startDate.valueAsDate, {zone: 'Europe/Paris'})
        .startOf('day')
    let end = DateTime
        .fromJSDate(dom.endDate.valueAsDate, {zone: 'Europe/Paris'})
        .startOf('day')

    // if (start <= today) {
    //     start = today.plus({days: 1})
    // }

    if (userInput) {
        start = today.plus({days: 1})
        end = start.plus({days: 1})
    } else if (end <= start) {
        end = start.plus({days: 1})
    }

    dom.startDate.value = start.toFormat('yyyy-MM-dd')
    dom.endDate.value = end.minus({days: 1}).toFormat('yyyy-MM-dd')
    dom.startHour.value =
        dom.startMinute.value =
        dom.endHour.value =
        dom.endMinute.value = '0'
}

const initMonthsChoice = userInput => {
    let monthStart = DateTime.now().startOf('month').startOf('day')
    let start = DateTime
        .fromJSDate(dom.startDate.valueAsDate, {zone: 'Europe/Paris'})
        .startOf('month')
        .startOf('day')
    let end = DateTime
        .fromJSDate(dom.endDate.valueAsDate, {zone: 'Europe/Paris'})
        .startOf('month')
        .startOf('day')
    
    // if (start <= monthStart) {
    //     start = monthStart.plus({months: 1})
    // }
    
    if (userInput) {
        start = monthStart.plus({months: 1})
        end = start.plus({months: 1})
    } else if (end <= start) {
        end = start.plus({months: 1})
    }

    dom.startDate.value = start.toFormat('yyyy-MM-dd')
    dom.endDate.value = end.minus({days: 1}).toFormat('yyyy-MM-dd')
    dom.startHour.value =
        dom.startMinute.value =
        dom.endHour.value =
        dom.endMinute.value = '0'
}

// weekly repetition

const updateEndWeekField = e => {
    if (e.target.checked) {
        dom.repeatWeeklyUntil.parentElement.classList.remove('d-none')
    } else {
        dom.repeatWeeklyUntil.parentElement.classList.add('d-none')
    }
}

// state

const dom = DateTimePicker.dom
let unit = ''
