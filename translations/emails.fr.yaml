registration_created:
  subject: "%name%, votre demande d'inscription est prise en compte"
  text: |
    Bonjour %name%,

    Votre inscription est en cours. Nous allons la valider dans les plus brefs
    délais et vous renvoyer un mail quand ce sera fait.

    Belle journée 🌱

registration_approved:
  subject: "%name%, votre inscription a été validée"
  text: |
    Bonjour %name%,

    Votre inscription sur le site de l'intranet Bouillon Cube / La Grange a
    été validée. Vous pouvez vous connecter dès maintenant à l'adresse
    %url% avec les informations suivantes :
    
    Login : %email%
    Mot de passe : %password%

    Pour votre sécurité, pensez à changer le mot de passe à votre première
    connexion.

    Bienvenue 🌞
  
registration_denied:
  subject: "%name%, votre demande d'inscription a été rejetée"
  text: |
    Bonjour %name%,

    Votre demande d'inscription à l'intranet La Grange a été rejetée. Si vous
    n'êtes pas sûr⋅e de pourquoi vous pouvez nous demander en répondant à ce
    mail.

    Belle journée 🌱
  
member_disabled:
  subject: "%name%, votre compte a été désactivé"
  text: |
    Bonjour %name%,

    Votre compte a été désactivé (soit par vous, soit par nous). Vos
    informations ont été effacées à l'exception de votre email, afin de
    pouvoir vous contacter sur des réservations ou des factures passées.

    Merci et bonne route 🚲
  
pass_requested:
  subject: "%name%, votre demande d'abonnement coopérant⋅e est prise en compte"
  text: |
    Bonjour %name%,

    Nous avons bien noté votre demande d'abonnement coopérant⋅e. C'est en
    cours, nous allons valider ça dès que possible.

    L'abonnement sera valable pour 6 mois à compter de la date de validation,
    et renouvelable 1 mois avant la fin. On vous envoie un mail quand c'est
    confirmé !

    Belle journée 🌱
  
pass_denied:
  subject: "%name%, votre demande d'abonnement coopérant⋅e a été rejetée"
  text: |
    Bonjour %name%,

    Votre demande d'abonnement coopérant⋅e a été rejetée. Si vous n'êtes pas
    sûr⋅e de pourquoi vous pouvez nous demander en répondant à ce mail.

    Belle journée 🌱
  
pass_approved:
  subject: "%name%, votre demande d'abonnement coopérant⋅e a été acceptée"
  text: |
    Bonjour %name%,

    Votre demande d'abonnement coopérant⋅e a été acceptée. Il est valable à
    partir d'aujourd'hui et pour 6 mois (fin au %end%).

    Vous bénéficiez maintenant des tarifs coopérant⋅e sur la réservation des
    espaces, et des espaces disponibles aux coopérant⋅es comme les bureaux
    individuels. Les 30 € de l'abonnement sont ajoutés à la facture de fin de
    mois.

    Merci de votre soutien à La Grange 🌞
  
pass_interrupted:
  subject: "%name%, votre abonnement coopérant⋅e a été interrompu"
  text: |
    Bonjour %name%,

    Votre abonnement coopérant⋅e a été interrompu. Vous repassez à partir
    d'aujourd'hui au tarif '%level%'. Le coût de vos réservation futures a été
    recalculé, et les réservations correspondant à des espaces auxquels vous
    n'avez plus accès, supprimées.

    Si vous n'êtes pas sûr⋅e de pourquoi, vous pouvez nous demander en
    répondant à ce mail.

    Belle journée 🌱
  
pass_expired:
  subject: "%name%, votre abonnement coopérant⋅e a expiré"
  text: |
    Bonjour %name%,

    Votre abonnement coopérant⋅e est arrivé à expiration. Vous repassez à partir
    d'aujourd'hui au tarif '%level%'. Le coût de vos réservation futures a été
    recalculé, et les réservations correspondant à des espaces auxquels vous
    n'avez plus accès, supprimées.

    Belle journée 🌱

pass_expired_report:
  subject: "[Intranet] Abonnements expirés"
  text: |
    %count% utilisateur⋅ice(s) sont arrivé⋅es à terme de leur abonnement :

    %passes%

    Les réservations suivantes ont été ajustées :

    %bookings%

    Les utilisateur⋅ices en question ont été notifié⋅es.

    -- Le petit robot de l'intranet 🤖
  passes_item: |
    * %name% redevient %level%
  bookings_item: |
    * La réservation pour '%place%' du %start% au %end% %status%
  status_raised: passe à %price% €
  status_deleted: est supprimée
  
pass_expiring:
  subject: "%name%, votre abonnement coopérant⋅e expire bientôt"
  text: |
    Bonjour %name%,

    Votre abonnement coopérant⋅e expire dans un mois (le %end%). Vous pouvez
    demander le renouvellement sur votre compte : %url%. 

    Belle journée 🌱
  
renewal_requested:
  subject: >
    %name%, la demande de renouvellement de votre abonnement coopérant⋅e
    est prise en compte
  text: |
    Bonjour %name%,

    Nous avons bien noté la demande de renouvellement de votre abonnement
    coopérant⋅e. Nous allons valider ça au plus vite.

    L'abonnement sera valable à nouveau pour 6 mois de plus. On vous envoie un
    mail quand c'est confirmé !

    Belle journée 🌱
  
renewal_approved:
  subject: >
    %name%, la demande de renouvellement de votre abonnement coopérant⋅e a été
    acceptée
  text: |
    Bonjour %name%,

    La demande de renouvellement de votre abonnement coopérant⋅e a été
    acceptée. Il est valable à nouveau pour 6 mois (fin au %end%). Les 30 € de
    l'abonnement sont ajoutés à la facture de fin de mois.

    Merci de votre soutien à La Grange 🌞
  
renewal_denied:
  subject: >
    %name%, la demande de renouvellement de votre abonnement coopérant⋅e a été
    rejetée
  text: |
    Bonjour %name%,

    La demande de renouvellement de votre abonnement coopérant⋅e a été
    rejetée. Si vous n'êtes pas sûr⋅e de pourquoi vous pouvez nous demander en
    répondant à ce mail.

    Belle journée 🌱

booking_requested:
  subject: >
    %name%, votre demande pour la salle '%place%' du %start% au %end% est prise
    en compte
  text: |
    Bonjour %name%,

    Votre demande de réservation pour la salle '%place%' du %start% au %end% est
    prise en compte. Ou vous la confirme (ou non) dans les plus brefs délais !

    Un mail sera envoyé pour vous tenir au courant.

    Belle journée 🌱

booking_approved:
  subject: >
    %name%, votre demande pour la salle '%place%' du %start% au %end% a été
    approuvée
  text: |
    Bonjour %name%,

    La salle '%place%' pour la période du %start% au %end% est à vous ! Les
    %price% € de réservation sont ajoutés à vos facture.

    Belle journée 🌱

booking_denied:
  subject: >
    %name%, votre demande pour la salle '%place%' du %start% au %end% a été
    rejetée
  text: |
    Bonjour %name%,

    Votre demande de réservation pour la salle '%place%' du %start% au %end% a
    été rejetée : l'espace ne doit pas être disponible pour cette période. Vous
    pouvez demander une autre réservation sur %url%, nous y répondrons au plus
    vite.

    Belle journée 🌱

booking_cancelled:
  subject: >
    %name%, votre réservation pour la salle '%place%' du %start% au %end% a été
    annulée
  text: |
    Bonjour %name%,

    Votre réservation pour la salle '%place%' du %start% au %end% a été annulée.
    Si vous n'êtes pas sûr⋅e de pourquoi vous pouvez nous demander en répondant
    à ce mail.

    Belle journée 🌱

booking_cancelled_report:
  subject: >
    [Intranet] Réservation annulée pour la salle '%place%' du %start% au %end%
  text: |
    La réservation de l'utilisateurice '%name%' pour la salle '%place%' du
    %start% au %end% a été annulée.

    -- Le petit robot de l'intranet 🤖

billing_due:
  subject: "%name%, vous avez une facture pour %month%"
  text: |
    Bonjour %name%,

    Vous avez une facture de %price% € pour vos réservations et / ou votre
    abonnement en %month%. Vous pouvez nous contacter en répondant à ce mail si
    vous manquez d'informations pour le règlement.

    Belle journée 🌱

billing_due_report:
  subject: "[Intranet] Factures dues fin %month%"
  text: |
    %count% utilisateur⋅ice(s) ont reçu une facture pour %month%:

    %list%

    Vous pouvez les retrouver sur %url%.

    -- Le petit robot de l'intranet 🤖
  item: |
    * %name% pour un total de %price% €

password_reset_link:
  subject: "%name%, vous avez demandé un nouveau mot de passe ?"
  text: |
    Bonjour %name%,

    Nous avons reçu une demande de rénitialisation de mot de passe pour cette
    adresse. Si cette demande vient bien de vous, vous pouvez suivre ce lien :

    %url%

    Pour changer votre mot de passe. Attention, il expire dans 30 minutes (à
    %time%).

    Si cette démarche ne vient pas de vous, vous n'avez rien à faire, c'est sans
    doute un⋅e autre utilisateur⋅ice ou un petit robot de l'internet qui a perdu
    son chemin.

    Belle journée 🌱
